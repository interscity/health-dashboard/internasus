# Revision history for InternaSUS

The version numbers below try to follow the conventions at http://semver.org/.

## Unreleased

* Add current locale to the user session
* Refresh page when user changes locale
* Add unit for distance travelled in the procedure pop-ups
* Add locale for specialty 17
* Improve locales for service value and hospitalization rates
* Fix competences ordering in the dynamic section of the charts page
* Display the correct data sources in public instances
* Disable the census sector rates accordion in the general data page
* Update rails to version 6.1.6.1
* Change the footer background color to white in public instances
* Add controller variable with the privacy status of the instance
* Unzip public data before setup
* Fix deploy pipeline with public instance
* Update Ruby to version 2.7.6
* Deploy public instance with data from the Rio de Janeiro state
* Update Puma to version 5.6.4
* Update Rails to version 6.1.6
* Add configuration to set public instances
* Ignore formats directory in the flay code duplication test
* Add Rio de Janeiro data format for SeedsLoader
* Add Rio de Janeiro data
* Center the map in the median geolocation of health facilities
* Change facility type and administration to codes in the HealthFacility
* Split SeedsLoader into several modules
* Only build charts for used demographic divisions in general data
* Only create general data territory section if all demographic divisions are present
* Only display used demographic divisions in facilities and procedures popups
* Only create selectors and checkboxes for existing divisions
* Add state and city columns for PatientDatum and HealthFacility
* Add State to demographic division
* Make demographic divisions optional for PatientDatum and HealthFacility
* Split SeedsLoader into several modules
* Update rails to version 6.1.5
* Update puma to version 5.6.2
* Add apt-get update to the gitlab CI
* Move facility related helper methods in procedures_helper to health_facilities_helper
* Convert population to a database table
* Update Ruby to version 2.7.5
* Add SeedsLoader class for abstracting the seeding implementation
* Update ffi to version 1.15.5
* Update nokogiri to version 1.12.5
* Enhance the map page view for low resolution displays
* Update webpacker to version 5.4.2
* Update yarn packages
* Show alert on search query error
* Update brakeman to version 5.1.1
* Update Rails to version 6.1.4.1
* Render facilities on top of percentiles circles
* Fix warnings during rspec
* Fix alignment of li tags in the map options legend
* Move Diego, Rafael and Gustavo to past members list
* Setup canvas rendering for limits
* Change the color array order in general data
* Fix tooltip not showing for the 25% and 50% percentiles circles
* Fix the facilities' legend when there are less than 2 facilities
* Consider only queried facilities for calculating the procedures range
* Fix the facilities' legend labels failing to display actual values
* Update yarn packages
* Update ruby to 2.7.4
* Update addressable to 2.8.0
* Change symbology of health facilities to circle markers
* Update puma
* Update nokogiri
* Clear map procedures when clearing filters
* Update rails to 6.1.3.2
* Change procedures counts by demographic division data processing routine
* Change UBS Area to UBS Coverage Area
* Update rexml to 3.2.5
* Update yarn packages
* Update testle-auth to 0.4.2 
* Change procedures counts by demographic division data processing routine
* Adjust heatmap radius slider interval to 0.1-5 km
* Calculate health facilities percentiles only after the first search
* Add loading overlay when loading administrative limits

## v1.0.0 - 27/03/2021

* Fix paths on member imagens on about section
* Update mimemagic
* Update about page member list
* Add usage slides to the about page
* Fix about page layout
* Adjust health facilities icon scale on map
* Fix selected map options not corresponding to health facilities and clusters options
* Send id instead of url for health facilities
* Round percentile distances
* Merge distance percentiles data with health facilities on search response
* Fix homepage data overview layout
* Use hospitalization data competence to retrieve dataset year
* Round census sectors total rate
* Change strategy to pass health facility icon urls to the map
* Add years of datasets available to the homepage
* Add method to retrieve dataset years
* Round to 2 decimal places the sent hospitalization_rate values
* Remove override of health facilities data from CNES fields in the procedures files
* Change percentiles label on advanced search page
* Remove WebSockets related code
* Round numbers with excessive precision in data files
* Update activerecord-session_store
* Fix missing public assets on docker deploy
* Fit health facilities legend on map to one line
* Remove unused dataset_proportion search result
* Bind ajax success return listener to form element on procedures map
* Refactor procedures map ajax response handling js code
* Load health facilities when loading the map the first time
* Add method helper to prepare health facilities data
* Use patients data grouped by latitude and longitude to build the procedures map
* Remove administrative limits variables from the ProceduresController
* Load procedures map search data using AJAX
* Fix sliders max value not including all hospitalizations
* Serve static assets with nginx
* Update ts-node to 9.1.1
* Update webpacker to 5.2.1
* Update ts-loader
* Update typescript
* Update yarn packages
* Update rails to 6.1.3
* Remove LimitsFeatures class
* Send demographic divisions through AJAX calls
* Move geoJSON get request handling to the DemographicDivisionsController
* Create DemographicDivisionsController
* Fix ticks in selected options being on top of text
* Add english translations
* Fix seeds on Heath Facilites updates regarding administrative sectors
* Add button to clear selected filters in the procedures map
* Keep filters between queries in the procedures map
* Create AdministrationDataChannel and tests
* Setup ActionCable connection and authentication

## v0.5.0 - 02/02/2021

* Increase shared memony size for postgres container
* Fix general data page error on reloads
* Add option on search page for the user to choose between administrative and census sector for calculating the heatmap
* Generalize CensusSectorsProceduresCounterService to use diffferent demographic sectors
* Fix translation keys for patient race
* Refactor GeneralDataService
* Refactor demographic division models
* Use census sector data given by SMS instead of IBGE
* Create DemographicDivision model
* Remove association between administrative and census sector models
* Compress files before sending to the client
* Add age code filters to rate heatmap total population calculation
* Add task to generate census sectors population jsons
* Add 'population' jsonb field to CensusSector model
* Add 'about' and 'guide' static_pages
* Install fontawesome
* Make 2019 dataset the default for procedure searchs
* Fix procedures period to 2019 in the advanced search page
* Update script to clean CSV data from 2018 and 2019 datasets
* Add heatmap legend to procedures map PDF export
* Add icon for OUTROS health facility administration type
* Add heatmap legend to procedures map
* Change procedures map's heatmaps data initialization
* Add updated Health facilites file
* Make deployment process aware of multiple compressed procedures files
* Change seeds to read from multiple procedures files
* Deprecate 2015 procedure file
* Store compressed procedures files
* Add script to clean CSV data from 2018 and 2019 datasets
* Fix census sector weighted heatmap values on procedures map
* Adjust heatmap radius slider interval
* Uninstall Leaflet.heat
* Use heatmap.js for procedures' map heatmaps
* Install heatmap.js
* Add map options legend to the procedures map
* Resize health facilities icons in the map according to their hospitalization rate
* Use same colors for same data types across charts on General Data
* Add popup with information for each area on the map
* Paint FamilyHealthStrategy map layer to highlight it
* Install trestle-auth
* Add legend to procedures map filters selected options
* Create SelectedFiltersLegend javascript helper
* Open general data page in a new tab
* Translate procedures filters options
* Change header sections translations
* Add Colormap charts on GeneralData dynamic section
* Create Colormap charts on GeneralData territory section
* Create GeojsonProviderService
* Move dynamic charts' data formatting to the backend
* Change dynamic charts' hospitalization groups chart type to treemap
* Add CNAES to health facility dropdown filter on search page
* Change age codes so they can be ordered alphabetically more easily
* Add information icon on each general data section
* Print general data sections
* Change complexities chart from line to bar chart
* Order health facilities alphabetically on general data charts
* Show percentage of data bars on general data graphs
* Increase font size for general data charts
* Add health facilities bar charts on general data
* Add health facilities facility type chart on general data
* Display distances section in general data page
* Add scroll bar on rankings table
* Display census sectors data on general data page
* Fix characterization section on general data page
* Display health facilities specialty distances chart on general data page
* Adjust heatmap radius when map is zoomed
* Display dynamic charts' ICD diagnoses charts on general data page
* Display descriptive statistics table on general data page
* Display most of dynamic charts
* Display technical health supervision chart on general data page
* Display regional health coordinator chart on general data page
* Display administrative sector chart on general data page
* Upgrade to PostgreSQL 12 on production server
* Install fail2ban and configure on production server
* Leave only ports 20, 80 and 443 open on production environment using UFW
* Display hospitalizations per hospitalization days bar chart on general data page
* Display hospitalizations per age code bar chart on general data page
* Display hospitalizations per type pie chart on general data page
* Display hospitalizations per race pie chart on general data page
* Display hospitalizations per gender pie chart on general data page
* Display hospitalization percentage per specialty pie chart on general data page
* Display specialties bar chart on general data page
* Add time series chart to general data
* Fix loading percentiles on map when there are nil values
* Perform general node modules updates
* Perform general gem updates
* Update rspec-rails
* Update Rails to 6.0.3

## v0.4.0 - 15/05/2020

* Adjust min and max clustering radius values for map options sliders
* Fix heatmap radius units
* Fix procedures search page wording
* Fix GeneralData JS loading
* Adjust heatmap intensity accordingly to dataset proportion
* Create HealthFacility distances percentiles map tooltip
* Create a route for GET /procedures/search
* Increase NGINX max request size
* Add legends to map on Health Facilities map markers
* Group map export buttons into section
* Shorten weighted heatmap population filters section title
* Fix map pdf generation with Firefox

## v0.3.1 - 17/04/2020

* Fix lint error

## v0.3.0 - 17/04/2020

* Create FamilyHealthStrategy model
* Add UBS limits to procedures map
* Add UBS seeds
* Create Ubs model
* Add city limits to procedures map
* Add city seeds
* Create City model
* Add minimap to procedures map
* Install leaflet-minimap
* Implement General Data rankings section
* Create GeneralDataService
* Rename minimal layout to wicked_pdf
* Install wicked_pdf
* Display map for pdf generation
* Install html2canvas
* Create minimal layout
* Implement General Data characterization section
* Create General Data index page
* Plot HealthFacilities HospitalizationDatum distances percentiles
* Install descriptive_statistics
* Create HealthFacility map markers toggle
* Create HealthFacility map popup
* Create HealthFacility controller and its show action
* Add heatmap opacity option on procedures map
* Add heatmap high contrast option on procedures map
* Add heatmap radius option on procedures map
* Add patient cluster radius option on procedures map
* Create procedures search results printing page
* Perform general gem updates
* Update bootstrap
* Update JS dependencies
* Update rails
* Export filtered procedures to CSV
* Install activerecord-session_store
* Install ransack_memory
* Update browser-env
* Update @rails/webpacker
* Uncheck conflicting census sector population filters
* Create CensusSectorsPopulationFilters JS (ts) class
* Filter CensusSector total population for weighted heat map
* Create ProceduresSearchService
* Add census sector weighted heatmap layer to procedures map
* Add latitude and longitude to CensusSector
* Create CensusSectorsProceduresCounterService
* Update puma

## v0.2.0 - 27/02/2020

* Filter map data by PatientDatum attributes
* Update nokogiri
* Upgrade to Ruby 2.7
* Check for inconsistent naming after seeding
* Fix seed data inconsistent naming
* Add dependency license check to CI
* Display administrative sectors limits on map
* Display subprefecture limits on map
* Display technical health supervisions limits on map
* Display regional health coordinations limits on map
* Display census sectors limits on map
* Add heatmap layer to procedures map
* Install Leaflet.heat
* Implement PatientData cluster toggle
* Extract PatientData cluster addition to map as class method
* Extract Leaflet map size invalidation to class method
* Extract patient data map marker creation to class method
* Extract PatientData map popup creator to class method
* Extract HealthFacility map marker creation JS to class method
* Extract Leaflet map initialization to class
* Install tslint
* Install jasmine
* Install TypeScript support to webpacker
* Create map option accordion
* Filter map data by Procedure attributes

## v0.1.0 - 23/01/2020

* Set homepage counters
* Filter map data by HealthFacility attributes
* Use redis for caching
* Use ajax to load PatientData map popup
* Create Procedure show page
* Translate PatientDatum education level
* Create translation helper for PatientDatum race
* Create translation helper for PatientDatum gender
* Load PatientData and associated information to map popup
* Create search methods between PatientData and HospitalizationData
* Associate PatientData to CensusSectors
* Add gitleaks and brakeman security checks on CI
* Install ransack
* Deploy procedures seed data
* Store encrypted procedures seed data
* Load and cluster patient data to map
* Install Leaflet.markercluster
* Load health facilities to map
* Install leaflet
* Install bootstrap-select, bootstrap-slider and bootstrap-datepicker
* Set advanced search health facility, procedure and patient data fields box titles
* Set footer to page bottom if content does not fill entire page
* Create Procedures index (Advanced  Search)
* Start deployed webserver in production mode
* Seed Patient Data and Hospitalization Data
* Switch From Sqlite3 to Postgres
* Seed CensusSector data
* Add administration interface
* Add association between Hospitalization Data and Procedure
* Add association between Patient Data and Procedure
* Create Procedure entity
* Seed Health Facilities data from PMSP data
* Add association between ICD Category and Hospitalization Data
* Add association between ICD Subcategory and Hospitalization Data
* Add association between Health Facility and Hospitalization Data
* Create HospitalizationData entity
* Seed ICD 10 data
* Add association between Regional Health Coordination and Patient Data
* Add association between Technical Health Supervision and Patient Data
* Add association between Subprefecture and Patient Data
* Add association between Administrative Sector and Patient Data
* Create PatientData entity
* Install rails-i18n
* Add association between Administrative Sector and Census Sector
* Create Census Sector entity
* Add association between Regional Health Coordination and Health Facility
* Add association between Technical Health Supervision and Health Facility
* Add association between Subprefecture and Health Facility
* Add association between Administrative Sector and Health Facility
* Create HealthFacility entity
* Create RegionalHealthCoordination entity
* Create TechnicalHealthSupervision entity
* Create Subprefecture entity
* Create Administrative Sector entity
* Add association between ICD Chapter and Group
* Add association between ICD Chapter and Category
* Create ICD Chapter entity
* Add association between ICD Group and Category
* Create ICD Group entity
* Add association between ICD Category and Subcategory
* Create ICD Category entity
* Create ICD Subcategory entity
* Integrate user login and logout buttons to Devise
* Create User model
* Setup code duplication detection
* Create services section in homepage
* Create homepage counter
* Add homepage slider images
* Configure continuous delivery
* Automate NGINX proxy installation
* Automate webserver deployment
* Automate docker installation for deployment
* Automate deployment user creation
* Create deployment setup playbook
* Add responsive design to footer
* Create footer links
* Add partner logo to footer
* Add contact and address to footer
* Extract footer to layout partial
* Add partner logo to header
* Create header links
* Internationalize application title
* Extract navbar to layout partial
* Create Dockerfile
* Create CI script
* Create HomepageController with an index
* Install Bootstrap
* Set license to MPL
* Install Rubocop
* Install Bullet
* Install bundler-audit
* Install shoulda matchers
* Install FactoryBot
* Install Cucumber
* Install simplecov
* Install RSpec
* Create empty rails application
