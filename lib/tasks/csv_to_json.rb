# frozen_string_literal: true

require 'json'
require 'csv'

class CSVToJson
  def self.census_sectors_population(input_csv_name, output_cs_json_name, output_ads_json_name)
    census_json = {}
    admin_sectors_json = {}

    CSV.foreach(input_csv_name, col_sep: "\t").with_index do |data, index|
      next if index.zero?

      d = ->(data_index) { data[data_index].gsub(',', '.').to_i }

      # This is loadind the data from the file with the fields provided. For
      # better performance and usability, it might be useful do add fields with
      # keys as in:
      #   - gender_colour
      #   - age_colour
      #   - age
      # This way we can access any field by doing
      # [gender_filter, age_filter, colour_filter].join('_')
      # , even for blank values of any values - not when all are blank, though.
      # Then, add a migration to add a jsonb field 'population' to each census
      # sector, and then change the seeds to load them from this file
      census_json[data[0]] =
        {
          men:                 d[6],
          men_00a04:           d[7],
          men_05a09:           d[8],
          men_10a14:           d[9],
          men_15a19:           d[10],
          men_20a24:           d[11],
          men_25a29:           d[12],
          men_30a34:           d[13],
          men_35a39:           d[14],
          men_40a44:           d[15],
          men_45a49:           d[16],
          men_50a54:           d[17],
          men_55a59:           d[18],
          # men_60a65:           d[19],
          # men_65a69:           d[20],
          men_60a69:           d[19] + d[20],
          men_70:              d[21],
          women:               d[22],
          women_00a04:         d[23],
          women_05a09:         d[24],
          women_10a14:         d[25],
          women_15a19:         d[26],
          women_20a24:         d[27],
          women_25a29:         d[28],
          women_30a34:         d[29],
          women_35a39:         d[30],
          women_40a44:         d[31],
          women_45a49:         d[32],
          women_50a54:         d[33],
          women_55a59:         d[34],
          # women_60a65:         d[35],
          # women_65a69:         d[36],
          women_60a69:         d[35] + d[36],
          women_70:            d[37],
          total:               d[38],
          white:               d[39],
          black:               d[40],
          yellow:              d[41],
          mulatto:             d[42],
          native:              d[43],
          men_00a04_white:     d[44],
          men_00a04_black:     d[45],
          men_00a04_yellow:    d[46],
          men_00a04_mulatto:   d[47],
          men_00a04_native:    d[48],
          women_00a04_white:   d[49],
          women_00a04_black:   d[50],
          women_00a04_yellow:  d[51],
          women_00a04_mulatto: d[52],
          women_00a04_native:  d[53],
          men_05a09_white:     d[54],
          men_05a09_black:     d[55],
          men_05a09_yellow:    d[56],
          men_05a09_mulatto:   d[57],
          men_05a09_native:    d[58],
          women_05a09_white:   d[59],
          women_05a09_black:   d[60],
          women_05a09_yellow:  d[61],
          women_05a09_mulatto: d[62],
          women_05a09_native:  d[63],
          men_10a14_white:     d[64],
          men_10a14_black:     d[65],
          men_10a14_yellow:    d[66],
          men_10a14_mulatto:   d[67],
          men_10a14_native:    d[68],
          men_15a19_white:     d[69],
          men_15a19_black:     d[70],
          men_15a19_yellow:    d[71],
          men_15a19_mulatto:   d[72],
          men_15a19_native:    d[73],
          men_20a24_white:     d[74],
          men_20a24_black:     d[75],
          men_20a24_yellow:    d[76],
          men_20a24_mulatto:   d[77],
          men_20a24_native:    d[78],
          men_25a29_white:     d[79],
          men_25a29_black:     d[80],
          men_25a29_yellow:    d[81],
          men_25a29_mulatto:   d[82],
          men_25a29_native:    d[83],
          men_30a34_white:     d[84],
          men_30a34_black:     d[85],
          men_30a34_yellow:    d[86],
          men_30a34_mulatto:   d[87],
          men_30a34_native:    d[88],
          men_35a39_white:     d[89],
          men_35a39_black:     d[90],
          men_35a39_yellow:    d[91],
          men_35a39_mulatto:   d[92],
          men_35a39_native:    d[93],
          men_40a44_white:     d[94],
          men_40a44_black:     d[95],
          men_40a44_yellow:    d[96],
          men_40a44_mulatto:   d[97],
          men_40a44_native:    d[98],
          men_45a49_white:     d[99],
          men_45a49_black:     d[100],
          men_45a49_yellow:    d[101],
          men_45a49_mulatto:   d[102],
          men_45a49_native:    d[103],
          men_50a54_white:     d[104],
          men_50a54_black:     d[105],
          men_50a54_yellow:    d[106],
          men_50a54_mulatto:   d[107],
          men_50a54_native:    d[108],
          men_55a59_white:     d[109],
          men_55a59_black:     d[110],
          men_55a59_yellow:    d[111],
          men_55a59_mulatto:   d[112],
          men_55a59_native:    d[113],
          men_60a69_white:     d[114],
          men_60a69_black:     d[115],
          men_60a69_yellow:    d[116],
          men_60a69_mulatto:   d[117],
          men_60a69_native:    d[118],
          men_70_white:        d[119],
          men_70_black:        d[120],
          men_70_yellow:       d[121],
          men_70_mulatto:      d[122],
          men_70_native:       d[123],
          women_10a14_white:   d[124],
          women_10a14_black:   d[125],
          women_10a14_yellow:  d[126],
          women_10a14_mulatto: d[127],
          women_10a14_native:  d[128],
          women_15a19_white:   d[129],
          women_15a19_black:   d[130],
          women_15a19_yellow:  d[131],
          women_15a19_mulatto: d[132],
          women_15a19_native:  d[133],
          women_20a24_white:   d[134],
          women_20a24_black:   d[135],
          women_20a24_yellow:  d[136],
          women_20a24_mulatto: d[137],
          women_20a24_native:  d[138],
          women_25a29_white:   d[139],
          women_25a29_black:   d[140],
          women_25a29_yellow:  d[141],
          women_25a29_mulatto: d[142],
          women_25a29_native:  d[143],
          women_30a34_white:   d[144],
          women_30a34_black:   d[145],
          women_30a34_yellow:  d[146],
          women_30a34_mulatto: d[147],
          women_30a34_native:  d[148],
          women_35a39_white:   d[149],
          women_35a39_black:   d[150],
          women_35a39_yellow:  d[151],
          women_35a39_mulatto: d[152],
          women_35a39_native:  d[153],
          women_40a44_white:   d[154],
          women_40a44_black:   d[155],
          women_40a44_yellow:  d[156],
          women_40a44_mulatto: d[157],
          women_40a44_native:  d[158],
          women_45a49_white:   d[159],
          women_45a49_black:   d[160],
          women_45a49_yellow:  d[161],
          women_45a49_mulatto: d[162],
          women_45a49_native:  d[163],
          women_50a54_white:   d[164],
          women_50a54_black:   d[165],
          women_50a54_yellow:  d[166],
          women_50a54_mulatto: d[167],
          women_50a54_native:  d[168],
          women_55a59_white:   d[169],
          women_55a59_black:   d[170],
          women_55a59_yellow:  d[171],
          women_55a59_mulatto: d[172],
          women_55a59_native:  d[173],
          women_60a69_white:   d[174],
          women_60a69_black:   d[175],
          women_60a69_yellow:  d[176],
          women_60a69_mulatto: d[177],
          women_60a69_native:  d[178],
          women_70_white:      d[179],
          women_70_black:      d[180],
          women_70_yellow:     d[181],
          women_70_mulatto:    d[182],
          women_70_native:     d[183]
        }

      admin_sector_name = data[4].parameterize(separator: ' ').titleize
      admin_sectors_json[admin_sector_name] ||= []
      admin_sectors_json[admin_sector_name].append(data[0])
    end

    gender_names = %w[men women]
    age_names = %w[00a04 05a09 10a14 15a19 20a24 25a29 30a34 35a39 40a44 45a49 50a54 55a59 60a69 70]
    race_names = %w[white black yellow mulatto native]

    census_json.each_key do |census_sector_id|
      # gender_race filters
      gender_names.product(race_names).each do |pair|
        value = 0
        gender = pair.first
        race = pair.second
        age_names.each { |age| value += census_json[census_sector_id][[gender, age, race].join('_').to_sym] }

        census_json[census_sector_id][pair.join('_').to_sym] = value
      end

      # age_race filters
      age_names.product(race_names).each do |pair|
        value = 0
        age = pair.first
        race = pair.second
        gender_names.each { |gender| value += census_json[census_sector_id][[gender, age, race].join('_').to_sym] }

        census_json[census_sector_id][pair.join('_').to_sym] = value
      end

      # age only filters
      age_names.each do |age|
        value = 0
        gender_names.each { |gender| value += census_json[census_sector_id]["#{gender}_#{age}".to_sym] }

        census_json[census_sector_id][age.to_sym] = value
      end
    end

    admin_sectors_json.transform_values! do |contained_sectors|
      contained_sectors.reduce({}) do |admin_sector, census_sector_id|
        admin_sector.merge(census_json[census_sector_id]) { |_, v1, v2| v1 + v2 }
      end
    end

    File.write(output_ads_json_name, JSON.pretty_generate(admin_sectors_json))
    File.write(output_cs_json_name, JSON.pretty_generate(census_json))
  end
end
