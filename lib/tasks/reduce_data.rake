# frozen_string_literal: true

require_relative 'data_reducer'

namespace :reduce_data do
  desc 'Reduce file sizes by rounding decimal places; the files are in original_data folder,
        which can be unziped from original_data.zip'

  task 'reduce' => :environment do
    GEOJSON_FILES = %w[
      cities.geojson
      sp_administrative_sectors.geojson
      sp_fhs.geojson
      sp_regional_health_coordination.geojson
      sp_subprefectures.geojson
      sp_technical_health_supervision.geojson
      sp_ubs.geojson
    ].freeze

    # Census Secturs
    Dir.mkdir('vendor/census_sectors') unless File.exist?('vendor/census_sectors')

    Dir.glob(Rails.root.join('vendor/original_data/census_sectors/*.json')).each do |sector_page|
      DataReducer.clean_census_sectors(sector_page)
    end

    # Centroids
    DataReducer.clean_centroids_csv('vendor/original_data/census_sectors/centroids.csv')
    DataReducer.clean_centroids_csv('vendor/original_data/admin_centroids.csv')

    # Administrative limits
    GEOJSON_FILES.each do |filename|
      DataReducer.clean_geojson_data("vendor/original_data/#{filename}")
    end
  end
end
