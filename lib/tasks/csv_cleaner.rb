# frozen_string_literal: true

require 'net/http'

class CsvCleaner
  @@distances = {}

  SUBPREFECTURES = {
    1 => 'PERUS',
    2 => 'PIRITUBA / JARAGUA',
    3 => 'FREGUESIA-BRASILANDIA',
    4 => 'CASA VERDE-CACHOEIRINHA',
    5 => 'SANTANA-TUCURUVI',
    6 => 'JACANA-TREMEMBE',
    7 => 'VILA MARIA-VILA GUILHERME',
    8 => 'LAPA',
    9 => 'SE',
    10 => 'BUTANTA',
    11 => 'PINHEIROS',
    12 => 'VILA MARIANA',
    13 => 'IPIRANGA',
    14 => 'SANTO AMARO',
    15 => 'JABAQUARA',
    16 => 'CIDADE ADEMAR',
    17 => 'CAMPO LIMPO',
    18 => 'MBOI MIRIM',
    19 => 'CAPELA DO SOCORRO',
    20 => 'PARELHEIROS',
    21 => 'PENHA',
    22 => 'ERMELINO MATARAZZO',
    23 => 'SAO MIGUEL PAULISTA',
    24 => 'ITAIM PAULISTA',
    25 => 'MOOCA',
    26 => 'ARICANDUVA / FORMOSA / CARRÃO',
    27 => 'ITAQUERA',
    28 => 'GUAIANASES',
    29 => 'VILA PRUDENTE',
    30 => 'SAO MATEUS',
    31 => 'CIDADE TIRADENTES',
    32 => 'SAPOPEMBA'
  }.freeze

  def self.clean_2018_or_2019_data(input_csv_name, output_csv_name)
    CSV.open(Rails.root.join("vendor/#{output_csv_name}"), 'w', quote_char: '"', col_sep: ',') do |csv|
      CSV.foreach(Rails.root.join("vendor/#{input_csv_name}"), quote_char: '"', col_sep: ',').with_index do |data, index|
        # This is a header
        if index == 0
          data.map! do |header|
            formatted_header = header.split(',').first
            case formatted_header
            when 'YSCWGS84'
              'LAT_SC'
            when 'XSCWGS84'
              'LONG_SC'
            when 'COD_GISA'
              'CODGISA'
            when 'NOME_DISTR'
              'DA'
            when 'SUBPREF'
              'PR'
            when 'NOME_FANTA'
              'CNES_DENO'
            when 'LONWGS84'
              'LONG_CNES'
            when 'LATWGS84'
              'LAT_CNES'
            when 'EsfAdm'
              'GESTAO'
            else
              formatted_header
            end
          end
          data.push 'DISTANCIA'
        else
          # Solve date format differences
          data[3].insert(4, '-').insert(7, '-')
          data[4].insert(4, '-').insert(7, '-')
          data[5].insert(4, '-').insert(7, '-')

          lat_sc_index, long_sc_index = output_csv_name.match?(/2018/) ? [36, 35] : [34, 33]
          lat_cnes_index, long_cnes_index = output_csv_name.match?(/2018/) ? [50, 51] : [47, 48]

          subprefecture_index      = output_csv_name.match?(/2018/) ? 25 : 27
          subprefecture_name_index = output_csv_name.match?(/2018/) ? 29 : 28
          da_index                 = output_csv_name.match?(/2018/) ? 26 : 25

          # Remove abbreviations
          data[da_index].gsub!(/CID\b/, 'CIDADE')
          data[da_index].gsub!(/JD\b/, 'JARDIM')

          # Use subprefecture code instead of name, as those not always match
          # with actual subprefecture names
          data[subprefecture_name_index] = SUBPREFECTURES[(data[subprefecture_index]).to_i]

          data.push(calculate_osrm_route(data[lat_sc_index], data[long_sc_index], data[lat_cnes_index], data[long_cnes_index]))

          # Round patient latitude and longitude
          [lat_sc_index, long_sc_index].each do |index|
            data[index] = data[index].to_f.round(4)
          end
          
          # Round health facility latitude and longitude
          [lat_cnes_index, long_cnes_index].each do |index|
            data[index] = data[index].to_f.round(5)
          end
        end

        csv << data
      end
    end
  end

  def self.calculate_osrm_route(lat_sc, long_sc, lat_cnes, long_cnes)
    route_params = "#{long_sc},#{lat_sc};#{long_cnes},#{lat_cnes}"
    return @@distances[route_params] if @@distances[route_params]

    uri = URI("http://127.0.0.1:5000/route/v1/driving/#{route_params}?overview=false")
    response = Net::HTTP.get(uri)
    parsed_response = JSON.parse response
    @@distances[route_params] = (parsed_response['routes'][0]['distance'].to_f / 1000).round(1)
    @@distances[route_params]
  end
end
