# frozen_string_literal: true

require_relative 'csv_to_json'

namespace :csv_to_json do
  desc 'Extract census sectors population data to json'
  task 'census_sectors_population' => :environment do
    CSVToJson.census_sectors_population('vendor/malha_populacao.tsv',
                                        'vendor/census_sectors_population.json',
                                        'vendor/administrative_sectors_population.json')
  end
end
