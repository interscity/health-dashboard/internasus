# frozen_string_literal: true

class DataReducer
  CENSUS_SECTOR_POPULATION = %w[
    POPULACAO_TOTAL
    POPULACAO_HOMEM
    POPULACAO_MULHER
    POPULACAO_BRANCA
    POPULACAO_PRETA
    POPULACAO_AMARELA
    POPULACAO_PARDA
    POPULACAO_INDIGENA
  ].freeze

  def self.new_filename(filename)
    filename.sub('vendor/original_data/', 'vendor/')
  end

  def self.clean_polygon(polygon)
    polygon.each do |ring|
      ring.each do |coordinates|
        coordinates[0] = coordinates[0].round(5)
        coordinates[1] = coordinates[1].round(5)
      end
    end
  end

  def self.clean_multi_polygon(multipolygon)
    multipolygon.each do |polygon|
      clean_polygon(polygon)
    end
  end

  def self.clean_feature(feature)
    case feature['geometry']['type']
    when 'Polygon'
      clean_polygon(feature['geometry']['coordinates'])
    when 'MultiPolygon'
      clean_multi_polygon(feature['geometry']['coordinates'])
    else
      message = 'Invalid GeoJSON Geometry object'
      Rails.logger.warn(message)
    end
  end

  def self.clean_geojson_data(filename)
    data = JSON.parse(File.read(filename))

    data['features'].each do |feature|
      clean_feature(feature)
    end

    File.write(new_filename(filename), JSON.dump(data))
  end

  def self.clean_census_sectors(filename)
    data = JSON.parse(File.read(filename))

    data['features'].each do |feature|
      clean_feature(feature)
      CENSUS_SECTOR_POPULATION.each do |category|
        feature['properties'][category] = feature['properties'][category].to_i
      end
    end

    File.write(new_filename(filename), JSON.dump(data))
  end

  def self.clean_centroids_csv(filename)
    data = CSV.parse(File.read(filename))

    CSV.open(new_filename(filename), 'w') do |row|
      row << data[0]

      data.drop(1).each do |data_row|
        [1, 2].each do |i|
          data_row[i] = data_row[i].to_f.round(5)
        end
        row << data_row
      end
    end
  end
end
