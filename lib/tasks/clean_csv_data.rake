# frozen_string_literal: true

require_relative 'csv_cleaner'

namespace :clean_csv_data do
  desc 'Clean 2018 CSV'
  task '2018' => :environment do
    CsvCleaner.clean_2018_or_2019_data('SIH18_SC_AP_IBGE_WGS84_ANONIMIZADA.csv', 'procedures_2018.csv')
  end

  desc 'Clean 2019 CSV'
  task '2019' => :environment do
    CsvCleaner.clean_2018_or_2019_data('SIH19_SC_AP_IBGE_WGS84_ANONIMIZADA.csv', 'procedures_2019.csv')
  end
end
