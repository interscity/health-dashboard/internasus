# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  PERMITTED_AREAS = /(state|city|census_sector|regional_health_coordination|technical_health_supervision|subprefecture|administrative_sector|ubs|family_health_strategy)/.freeze

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    devise_for :users
    root 'homepage#index'

    resources :procedures, only: %i[index show] do
      post 'search/', to: 'procedures#search', on: :collection
      get 'search/', to: 'procedures#index', on: :collection
      get 'export/', to: 'procedures#export', on: :collection
      post 'print/', to: 'procedures#print', on: :collection
      get 'map_popup_on/:area/for/:id', to: 'procedures#map_popup_on_area', on: :collection, as: :map_popup_on_area, area: PERMITTED_AREAS
    end

    resources :health_facilities, only: [:show]

    resources :general_data, only: [:index] do
      post 'print/', to: 'general_data#print', on: :collection
    end

    resources :demographic_divisions do
      get 'geojson/:area', to: 'demographic_divisions#geojson', on: :collection, area: PERMITTED_AREAS
    end

    get 'about', to: 'static_pages#about'
    get 'guide', to: 'static_pages#guide'
  end
end
