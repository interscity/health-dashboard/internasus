# InternaSUS

An interactive visual dashboard for large-scale data analysis based on the Brazilian National Health System (SUS) hospitalization data. Its software architecture enables integration with the Hospital Information System (SIH-SUS) datasets from any region of Brazil so that health professionals can use it in hundreds of different cities. It processes SIH-SUS data and stores it into a geolocated relational database. Expert users can then perform advanced queries on the data with composite filters. Results are then displayed via multiple map visualizations, graphs, and tables. The goal is that this open-source platform will become a useful tool for science-based public health policymaking, influencing Brazilian public managers in the future to adopt an evidence-based, data-driven approach to healthcare management.

## Ruby version

The version is defined by the `.ruby-version` file.

## System dependencies

* Ruby
  - Our preferred way of installation is using [RVM](https://rvm.io/)

* [Yarn](https://yarnpkg.com/lang/en/)
  - You can install it using npm by running `npm install -g yarn`

* [wkhtmltopdf](https://wkhtmltopdf.org/)
  - You can install it from your OS package manager
  - Or run `gem install wkhtmltopdf-binary`
  - Or [download](https://wkhtmltopdf.org/downloads.html) it

## Database

This platform needs the insertion of the following data files to work:
* `/vendor/*_health_centres.csv`, to load health facilities data
* `/vendor/procedures.csv`, to load the hospitalizations and patients data
* `/vendor/cid10/*.csv`, to load all diagnosis ICD codes
* `/vendor/census_sectors/*.json`, to load census sectors shapes
* `/vendor/*.geojson`, to load the sector shapes are shown on the map
  - This application uses Sao Paulo city sectors: Administrative Sector, Subprefecture, Regional Health Coordination, and Technical Health Supervision

## Configuration

Just run `./bin/setup` to get an development environment working.

## How to run the test suite

* Linter:
  - for code style run `rubocop`
  - for code duplications run `flay -s **/*.rb`
  - for TypeScript run `yarn lint`
* All tests: `rake`
* Unit tests: `rails spec`
* JS unit tests: `yarn test`
* Acceptance tests: `rails cucumber`

## Deployment instructions

Check [deploy/README.md](deploy/README.md).

## Building the docker image

If you wish to build your image from source instead of using [our registry ](https://gitlab.com/interscity/health-dashboard/internasus/container_registry), run:

* `docker build . -t internasus`

## License

[Mozilla Public License v2](COPYING)
