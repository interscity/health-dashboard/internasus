Feature: Procedures map

  In order to visualize procedure related georeferenced data
  As a user
  I should be able to see it in a map

  Background:
    Given I have a logged in user
    And there are sample organizational entities
    And there are sample ICD entities
    And there is a sample HealthFacility
    And there are other distant sample PatientDatum and HospitalizationDatum with id 0
    And there are other distant sample PatientDatum and HospitalizationDatum with id 1
    And there are sample PatientDatum and HospitalizationDatum with id 2
    And there are sample PatientDatum and HospitalizationDatum with id 3

  @javascript
  Scenario: Loading with data
    When I am at "/procedures"
    Then I should have no console errors
    And I should see health facilities markers
    And I should not see the patient data markers
    And I should not see the patient data cluster marker

  @javascript
  Scenario: Loading 2019 data by default
    When I am at "/procedures"
    And I disable collapse transitions
    And I click the "Procedimento" button
    Then I should see 2019 limits on the period filter

  @javascript
  Scenario: Seeing the minimap
    When I am at "/procedures"
    Then I should have no console errors
    And I should see the minimap
    When I minimize the minimap
    Then I should see the minimized minimap

  Scenario: Retrieving the procedures by demographic division data
    When I initialize DemographicDivisionsProceduresCounterService
    And I call the procedures by division method
    Then I should get an array with the expected information

  @javascript
  Scenario: Searching
    When I am at "/procedures"
    And I click the "Buscar" button
    And I wait for the overlay to disappear
    And I click the health facility marker
    And I click the patient data cluster marker
    And I click the patient data cluster marker
    And I click the patient data marker
    Then I should have no console errors
    And I should see the patient data markers
    And I should see a popup
    And I should have no console errors
    And I should see health facilities markers
    And I should see the patient data cluster marker
    When I disable collapse transitions
    And I click the "Mapa de calor por taxa de internações" button
    When I check the "csw_heat_map_black" checkbox
    Then the "csw_heat_map_black" checkbox should be checked
    When I click the "Agrupamento" button
    And I uncheck the "clustering_enabled" checkbox
    Then I should have no console errors
    And I should not see the patient data cluster marker
    When I check the "clustering_enabled" checkbox
    Then I should have no console errors
    And I should see the patient data cluster marker
    And I should see 2 patient data cluster markers
    When I drag the clustering radius slider to a higher value
    And I should see 1 patient data cluster markers
    When I click the "Mapa de calor" button
    And I check the "heat_map_enabled" checkbox
    And I set the slider with id "heat_map_radius" value to "30"
    Then I should have no console errors
    And I set the slider with id "heat_map_opacity" value to "0.5"
    Then I should have no console errors
    When I check the "heat_map_high_contrast" checkbox
    Then I should have no console errors
    When I uncheck the "heat_map_high_contrast" checkbox
    Then I should have no console errors
    When I uncheck the "heat_map_enabled" checkbox
    Then I should have no console errors
    When I check the "csw_heat_map_enabled" checkbox
    Then I should have no console errors
    When I uncheck the "csw_heat_map_enabled" checkbox
    Then I should have no console errors
    When I click the "Limites administrativos" button
    Then I should be able to toggle "show_administrative_sector_limits"
    And I should be able to toggle "show_subprefecture_limits"
    And I should be able to toggle "show_technical_health_supervision_limits"
    And I should be able to toggle "show_regional_health_coordination_limits"
    And I should be able to toggle "show_census_sector_limits"
    When I click the "Estabelecimentos" button
    And I uncheck the "health_facilities_enabled" checkbox
    Then I should have no console errors
    And I should not see health facilities markers
    When I check the "health_facilities_enabled" checkbox
    Then I should have no console errors
    And I should see health facilities markers
    When I check the "health_facilities_percentiles_enabled" checkbox
    Then I should have no console errors
    When I uncheck the "health_facilities_percentiles_enabled" checkbox
    Then I should have no console errors
    When I set the slider with id "q_health_facility_beds_between" values to "0" and "0"
    And I click the "Buscar" button
    Then I should have no console errors
    And I should not see the patient data markers
    And I should not see the patient data cluster marker
    And I should not see health facilities markers

  @javascript
  Scenario: Filter legends
    When I am at "/procedures"
    And I disable collapse transitions
    And I click the "Estabelecimento" button
    Then I should not see "Estabelecimentos selecionados"
    When I select "1" from "q_health_facility_id_in" filter
    Then I should see "Estabelecimentos selecionados"

  @javascript
  Scenario: Change sector type used for heatmap rates
    When I am at "/procedures"
    And I disable collapse transitions
    And I click the "Mapa de calor por taxa de internações" button
    Then I should see "Tipo de setor usado para taxa"
    When I select "Distrito Administrativo" from "heatmap_weight_model" filter
    And I click the "Buscar" button
    Then I should have no console errors

  @javascript
  Scenario: Seeing the heatmap legend
    When I am at "/procedures"
    Then I should have no console errors
    And I should not see the "procedures" heatmap
    And I should not see the "csw" heatmap
    When I disable collapse transitions
    And I click the "Legendas" button
    And I click the "Mapa de calor" button
    And I check the "heat_map_enabled" checkbox
    Then I should see the "procedures" heatmap
    And I should not see the "csw" heatmap
    When I check the "csw_heat_map_enabled" checkbox
    And I uncheck the "heat_map_enabled" checkbox
    Then I should see the "csw" heatmap
    And I should not see the "procedures" heatmap
    And I should have no console errors

  @javascript
  Scenario: Printing
    When I am at "/procedures"
    And I click the "PDF" link
    Then I should have no console errors

  @javascript
  Scenario: CSV download
    When I am at "/procedures"
    And I click the "Buscar" button
    And I wait for the overlay to disappear
    And I click the "CSV" link
