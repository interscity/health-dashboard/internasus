Feature: Admin access

  In order to protect sensitive data
  As a user
  I should not be able to access the admin panel

  Scenario: Non logged user
    Given I am at "/admin"
    Then I should be at "/users/sign_in"
    And I should see "Para continuar, faça login"

  Scenario: Logged but non admin user
    Given I have a logged in user
    And I am at "/admin"
    Then I should be at "/"
    And I should see "Apenas administradores podem acessar essa página"
