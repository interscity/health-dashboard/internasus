Feature: Users Admin

  In order to control who has access to sensitive data
  As an admin
  I should be able to manage users

  Scenario: Creation
    Given I am logged in as an admin user
    And I am at "/admin/users/new"
    When I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Salvar Usuário" button
    Then I should see "Usuário foi criado com sucesso."
