# frozen_string_literal: true

Given('I have an account with email {string} and password {string}') do |email, password|
  @user = User.create email: email, password: password
end

Given('I am logged in as an admin user') do
  email = 'admin@admin.com'
  password = 'admin123'

  @user = User.create email: email, password: password, admin: true

  step 'I am at "/users/sign_in"'
  step "I fill the \"E-mail\" input with \"#{email}\""
  step "I fill the \"Senha\" input with \"#{password}\""
  step 'I click the "Login" button'
end

Given('I have a logged in user') do
  email = 'user@user.com'
  password = 'user123'

  @user = User.create email: email, password: password, admin: false

  step 'I am at "/users/sign_in"'
  step "I fill the \"E-mail\" input with \"#{email}\""
  step "I fill the \"Senha\" input with \"#{password}\""
  step 'I click the "Login" button'
end
