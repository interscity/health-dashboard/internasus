# frozen_string_literal: true

When('I minimize the minimap') do
  find('a.leaflet-control-minimap-toggle-display').click
end

When('I click the patient data cluster marker') do
  find('div.marker-cluster div span', match: :first).click
end

When('I click the patient data marker') do
  find('img.leaflet-marker-icon:not(.health_facility_icon)', match: :first).click
end

When('I click the health facility marker') do
  find('#map div div g path.leaflet-interactive', match: :first).click
end

Then('I should see health facilities markers') do
  expect(page).to have_selector('#map div div g path.leaflet-interactive')
end

Then('I should see the patient data cluster marker') do
  expect(page).to have_selector('div.marker-cluster div span')
end

Then('I should see {int} patient data cluster markers') do |markers|
  expect(find_all('div.marker-cluster div span').count).to eq(markers)
end

When('I drag the clustering radius slider to a higher value') do
  el = page.driver.browser.find_element(css: 'div#clustering_radius_container .min-slider-handle')
  page.driver.browser.action.drag_and_drop_by(el, 100, 0).perform
end

When('I select {string} from {string} filter') do |option, select_id|
  select option, from: select_id, visible: false
end

When('I initialize DemographicDivisionsProceduresCounterService') do
  @counter_service = DemographicDivisionsProceduresCounterService.new(PatientDatum.ids)
end

When('I call the procedures by division method') do
  @procedures_by_division_id = @counter_service.procedures_by_division_id
end

Then('I should get an array with the expected information') do
  procs = PatientDatum.from(@procedures_by_division_id, :procs_by_div_id)
                      .pluck(:div_id, :procs_count)
  expect(procs).to eq([[5, 3]])
end

When('I wait for the overlay to disappear') do
  sleep(1)
end

Then('I should see the patient data markers') do
  expect(page).to have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should see a popup') do
  expect(page).to have_selector('div.leaflet-popup-content')
end

Then('I should not see health facilities markers') do
  expect(page).to_not have_selector('img.leaflet-marker-icon.health_facility_icon')
end

Then('I should not see the patient data cluster marker') do
  expect(page).to_not have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should not see the patient data markers') do
  expect(page).to_not have_selector('img.leaflet-marker-icon:not(.health_facility_icon)')
end

Then('I should see the minimap') do
  expect(page).to have_selector('div.leaflet-control-minimap')
end

Then('I should see the minimized minimap') do
  expect(page).to have_selector('a.minimized-bottomright')
end

Then('I should see the {string} heatmap') do |heatmap_type|
  expect(page).to have_selector("div##{heatmap_type}-heatmap-legend")
end

Then('I should not see the {string} heatmap') do |heatmap_type|
  expect(page).not_to have_selector("div##{heatmap_type}-heatmap-legend")
end

Then('I should see 2019 limits on the period filter') do
  expect(page).to have_selector("input[value='2019-01-01']")
  expect(page).to have_selector("input[value='2019-12-31']")
end

Then('I should be able to toggle {string}') do |field|
  step "I check the \"#{field}\" checkbox"
  step 'I should have no console errors'
  step "I uncheck the \"#{field}\" checkbox"
  step 'I should have no console errors'
end
