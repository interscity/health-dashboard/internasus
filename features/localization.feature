Feature: Localization

  In order to see the platform in a different language
  As a user
  I should be able to change locales

  Scenario: Changing to English while in homepage without locale in URL
    When I am at "/"
    And I click the "Idioma" link
    And I click the "English" link
    Then I should be at "/en"

  Scenario: Changing from English to Portuguese
    When I am at "/en"
    And I click the "Language" link
    And I click the "Português" link
    Then I should be at "/pt-BR"

  Scenario: Clicking a link keeps the locale in the URL
    When I am at "/pt-BR"
    And I click the "InternaSUS" link
    Then I should be at "/pt-BR"