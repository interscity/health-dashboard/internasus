Feature: Authentication

  In order to have access to sensitive data
  As a user
  I should be able to login

  Scenario: Valid credentials
    Given I have an account with email "user@user.com" and password "user1234"
    And I am at "/users/sign_in"
    When I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
    Then I should see "Login efetuado com sucesso."
