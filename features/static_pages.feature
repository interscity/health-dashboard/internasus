Feature: Static pages

  In order to know more about the platform
  As a user
  I should be able to see the About and User Guide pages

  @javascript
  Scenario: Access the About page
    Given I am at "/pt-BR"
    When I click the "Sobre" link
    Then I should be at "/pt-BR/about"
    And I should have no console errors

  @javascript
  Scenario: Access the User Guide page
    Given I am at "/pt-BR"
    When I click the "Guia do Usuário" link
    Then I should be at "/pt-BR/guide"
    And I should have no console errors
