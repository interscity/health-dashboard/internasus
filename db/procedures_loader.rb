module ProceduresLoader
  def load_procedures_files(file_paths, procedures_format)
    @procedures_count = 1
    csv_info = procedures_format[:csv]

    file_paths.each do |file_path|
      patients = []
      hospitalizations = []

      CSV.foreach(file_path, quote_char: csv_info[:quote_char], col_sep: csv_info[:col_sep], headers: true) do |data|
        patient_data = create_patient_data(data, procedures_format[:column_mappings])
        patients << patient_data.merge(create_divisions_data(data, procedures_format[:demographic_divisions]))
        hospitalizations << create_hospitalization_data(data, procedures_format[:column_mappings])

        @procedures_count += 1

        if patients.length > 999
          PatientDatum.insert_all(patients)
          HospitalizationDatum.insert_all(hospitalizations)
          patients = []
          hospitalizations = []
        end
      rescue MissingDivision
        next
      end

      PatientDatum.insert_all(patients) unless patients.empty?
      HospitalizationDatum.insert_all(hospitalizations) unless hospitalizations.empty?
    end
  end

  def validate_procedures_format(procedures_format)
    procedures_format.assert_valid_keys(:csv, :demographic_divisions, :column_mappings)
    procedures_format[:csv].assert_valid_keys(:quote_char, :col_sep)
    procedures_format[:column_mappings].assert_valid_keys(
      :age_year, :education_level, :gender, :latitude, :longitude, :race, :facility, :diag_pr, :diag_se1, :diag_se2,
      :admission_date, :competence, :complexity, :distance, :financing, :gestor_id, :hospitalization_days,
      :hospitalization_group, :hospitalization_rates, :hospitalization_type, :issue_date, :leave_date, :service_value,
      :specialty, :ui_rates, :uti_rates
    )
    procedures_format[:demographic_divisions].each do |div|
      div.assert_valid_keys(:klass, :on, :column)
    end
  end

  def create_patient_data(data, mapping)
    {
      age_code: get_age_code(data[mapping[:age_year]].to_i),
      age_year: data[mapping[:age_year]].to_i,
      educational_level: data[mapping[:educational_level]].to_i,
      gender: data[mapping[:gender]],
      latitude: data[mapping[:latitude]].to_f,
      longitude: data[mapping[:longitude]].to_f,
      race: data[mapping[:race]],
      procedure_id: @procedures_count,
      created_at: Time.now,
      updated_at: Time.now
    }
  end

  def create_hospitalization_data(data, mapping)
    health_facility = HealthFacility.find_by(cnes: data[mapping[:facility]])
    if health_facility.nil?
      warn 'Procedure skipped because of missing facility.' \
           " Couldn't find health facility with CNES of #{data[mapping[:facility]]}."
      raise SkippedProcedure.new 'Procedure skipped because of missing facility.'
    end

    icd_subcategory = IcdSubcategory.find_by(code: data[mapping[:diag_pr]])
    icd_category = icd_subcategory ? IcdCategory.find(icd_subcategory.icd_category_id) : nil
    secondary_icd_subcategory1 = IcdSubcategory.find_by(code: data[mapping[:diag_se1]])
    secondary_icd_subcategory2 = IcdSubcategory.find_by(code: data[mapping[:diag_se2]])

    {
      health_facility_id: health_facility.id,
      icd_subcategory_id: icd_subcategory&.id,
      icd_category_id: icd_category&.id,
      secondary_icd1_id: secondary_icd_subcategory1 ? secondary_icd_subcategory1.id : nil,
      secondary_icd2_id: secondary_icd_subcategory2 ? secondary_icd_subcategory2.id : nil,
      admission_date: data[mapping[:admission_date]],
      competence: data[mapping[:competence]].to_i,
      complexity: data[mapping[:complexity]].to_i,
      distance: data[mapping[:distance]].to_f,
      financing: data[mapping[:financing]].to_i,
      gestor_id: data[mapping[:gestor_id]].to_i,
      hospitalization_days: data[mapping[:hospitalization_days]].to_i,
      hospitalization_group: data[mapping[:hospitalization_group]].to_i,
      hospitalization_rates: data[mapping[:hospitalization_rates]].to_i,
      hospitalization_type: data[mapping[:hospitalization_type]].to_i,
      issue_date: data[mapping[:issue_date]],
      leave_date: data[mapping[:leave_date]],
      service_value: data[mapping[:service_value]].to_f,
      specialty: data[mapping[:specialty]].to_i,
      ui_rates: data[mapping[:ui_rates]].to_i,
      uti_rates: data[mapping[:uti_rates]].to_i,
      procedure_id: @procedures_count,
      created_at: Time.now,
      updated_at: Time.now
    }
  end
end
