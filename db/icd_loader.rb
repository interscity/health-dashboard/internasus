module ICDLoader
  def load_icd_categories
    icd_categories = []
    file = Rails.root.join('vendor', 'cid10', 'CID-10-CATEGORIAS.CSV')
    CSV.foreach(file, quote_char: '|', col_sep: ';', headers: true) do |icd10_row|
      icd_categories << { code: icd10_row['CAT'], description: icd10_row['DESCRICAO'],
                          created_at: Time.now, updated_at: Time.now }
    end
    IcdCategory.insert_all(icd_categories)
  end

  def load_icd_subcategories
    category = nil
    icd_subcategories = []
    file = Rails.root.join('vendor', 'cid10', 'CID-10-SUBCATEGORIAS.CSV')
    CSV.foreach(file, quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
      category_code = icd10_row['SUBCAT'][0..2]
      category = IcdCategory.find_by(code: category_code) if category&.code != category_code

      icd_subcategories << { code: icd10_row['SUBCAT'], description: icd10_row['DESCRICAO'],
                             icd_category_id: category.id, created_at: Time.now, updated_at: Time.now }
    end
    IcdSubcategory.insert_all(icd_subcategories)
  end

  def load_icd_groups
    file = Rails.root.join('vendor', 'cid10', 'CID-10-GRUPOS.CSV')
    CSV.foreach(file, quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
      group = IcdGroup.new(description: icd10_row['DESCRICAO'])
      group.icd_categories = IcdCategory.where(
        "code >= :first_category AND code <= :last_category",
        { first_category: icd10_row['CATINIC'], last_category: ['CATFIM'] }
      )
      group.save!
    end
  end

  def load_icd_chapters
    file = Rails.root.join('vendor', 'cid10', 'CID-10-CAPITULOS.CSV')
    CSV.foreach(file, quote_char: '$', col_sep: ';', headers: true) do |icd10_row|
      chapter = IcdChapter.new(code: icd10_row['NUMCAP'], description: icd10_row['DESCRICAO'])
      chapter.icd_categories = IcdCategory.where(
        "code >= :first_category AND code <= :last_category",
        { first_category: icd10_row['CATINIC'], last_category: ['CATFIM'] }
      )
      chapter.save!
    end
  end
end
