module HealthFacilitiesLoader
  def load_health_facilities_file(file_path, facilities_format)
    csv_info = facilities_format[:csv]

    CSV.foreach(file_path, quote_char: csv_info[:quote_char], col_sep: csv_info[:col_sep], headers: true) do |data|
      facility_data = create_health_facility(data, facilities_format[:column_mappings])
      division_data = create_divisions_data(data, facilities_format[:demographic_divisions])
      HealthFacility.create!(facility_data.merge(division_data))
    end
  end

  def validate_health_facilities_format(facilities_format)
    facilities_format.assert_valid_keys(:data_file, :csv, :column_mappings, :demographic_divisions, :after)
    facilities_format[:csv].assert_valid_keys(:quote_char, :col_sep)
    facilities_format[:column_mappings].assert_valid_keys(:cnes, :name, :latitude, :longitude,
                                                          :facility_type, :phone, :beds, :administration)
    facilities_format[:demographic_divisions].each do |div|
      div.assert_valid_keys(:klass, :on, :column)
    end
  end

  def create_health_facility(data, mapping)
    {
      cnes: data[mapping[:cnes]],
      name: data[mapping[:name]],
      latitude: data[mapping[:latitude]],
      longitude: data[mapping[:longitude]],
      facility_type: data[mapping[:facility_type]],
      phone: data[mapping[:phone]],
      beds: data[mapping[:beds]],
      administration: data[mapping[:administration]]
    }
  end
end
