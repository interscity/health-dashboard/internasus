# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'
require_relative 'seeds_loader.rb'

AGE_CODE = %w[TP_000A004 TP_000A004 TP_000A004 TP_000A004 TP_000A004 TP_005A009 TP_005A009 TP_005A009
              TP_005A009 TP_005A009 TP_010A014 TP_010A014 TP_010A014 TP_010A014 TP_010A014 TP_015A019
              TP_015A019 TP_015A019 TP_015A019 TP_015A019 TP_020A024 TP_020A024 TP_020A024 TP_020A024
              TP_020A024 TP_025A029 TP_025A029 TP_025A029 TP_025A029 TP_025A029 TP_030A034 TP_030A034
              TP_030A034 TP_030A034 TP_030A034 TP_035A039 TP_035A039 TP_035A039 TP_035A039 TP_035A039
              TP_040A044 TP_040A044 TP_040A044 TP_040A044 TP_040A044 TP_045A049 TP_045A049 TP_045A049
              TP_045A049 TP_045A049 TP_050A054 TP_050A054 TP_050A054 TP_050A054 TP_050A054 TP_055A059
              TP_055A059 TP_055A059 TP_055A059 TP_055A059 TP_060A064 TP_060A064 TP_060A064 TP_060A064
              TP_060A064 TP_065A069 TP_065A069 TP_065A069 TP_065A069 TP_065A069 TP_070A074 TP_070A074
              TP_070A074 TP_070A074 TP_070A074 TP_075A079 TP_075A079 TP_075A079 TP_075A079 TP_075A079
              TP_080A084 TP_080A084 TP_080A084 TP_080A084 TP_080A084 TP_085A089 TP_085A089 TP_085A089
              TP_085A089 TP_085A089 TP_090A094 TP_090A094 TP_090A094 TP_090A094 TP_090A094 TP_095A099
              TP_095A099 TP_095A099 TP_095A099 TP_095A099 TP_100OUMA]

def get_age_code(age)
  if age >= 0 && age <= 99
    return AGE_CODE[age]
  elsif age > 99
    return AGE_CODE[100]
  else
    return nil
  end
end

seeds_loader = SeedsLoader.new

seeds_loader.load_icd_data
seeds_loader.load_demographic_divisions
seeds_loader.assert_divisions_shapes
seeds_loader.load_health_facilities
seeds_loader.load_procedures

puts "data loaded successfully" + (" " * 20) + "\n"
