module DemographicDivisionLoader
  def create_with_shape(klass, paths, field, type)
    divisions = []

    paths.each do |path|
      JSON.parse(File.read(path))['features'].each do |geojson|
        case type
        when :code
          name = geojson['properties'][field]
        when :name
          name = geojson['properties'][field].parameterize(separator: ' ').titleize
        end

        divisions << { name: name,
                       shape: geojson['geometry'].to_json,
                       type: klass.to_s,
                       created_at: Time.now,
                       updated_at: Time.now }
      end
    end

    klass.insert_all(divisions)
  end

  def update_population(klass, path)
    json_data = JSON.parse(File.read(path))
    json_data.each do |sector, population_data|
      pop = population_data.transform_keys { |k| "p_#{k}" }
      klass.find_by!(name: sector)
           .create_population!(pop.merge({ created_at: Time.now, updated_at: Time.now }))
    end
  end

  def update_centroid(klass, path, column, csv)
    CSV.foreach(path, quote_char: csv[:quote_char], col_sep: csv[:col_sep], headers: true) do |centroid|
      klass.find_by!(name: centroid[column])
           .update!(latitude: centroid['lat'], longitude: centroid['lng'])
    end
  end

  def create_divisions_data(data, demographic_divisions)
    hf_field = ->(index) { data[index].parameterize(separator: ' ').titleize }

    divisions = {}

    demographic_divisions.each do |div_info|
      case div_info[:on]
      when :code
        name = data[div_info[:column]]
      when :name
        name = data[div_info[:column]].parameterize(separator: ' ').titleize.gsub(' / ', '-')
      end

      division = div_info[:klass].find_by(name: name)

      if division.nil?
        warn 'Entry skipped because of missing demographic division.' \
             " Couldn't find demographic division with name '#{name}'."
        raise MissingDivision.new 'entry skipped because of missing division'
      end

      divisions["#{div_info[:klass].to_s.underscore}_id".to_sym] = division.id
    end

    divisions
  end
end

class MissingDivision < StandardError
  def initialize(msg="Skipped procedure", exception_type="custom")
    @exception_type = exception_type
    super(msg)
  end
end
