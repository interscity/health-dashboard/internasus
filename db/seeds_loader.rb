Dir[Rails.root.join('db/data_formats/*.rb')].each { |file| require file }
require_relative 'demographic_divisions_loader'
require_relative 'health_facilities_loader'
require_relative 'icd_loader'
require_relative 'procedures_loader'

class SeedsLoader
  include DemographicDivisionLoader
  include HealthFacilitiesLoader
  include ICDLoader
  include ProceduresLoader

  def initialize
    loader_table = {
      "sms-sp" => SMSSPFormat,
      "public-rj" => RioDeJaneiroFormat
    }
    key = ENV["HEALTHDASHBOARD_DATA_FORMAT"]
    key = "sms-sp" unless loader_table.key?(key) # fallback
    @format = loader_table[key].new
    print "data format: #{key}\n"
  end

  def load_icd_data
    info "loading ICD categories..."
    load_icd_categories

    info "loading ICD subcategories..."
    load_icd_subcategories

    info "loading ICD groups..."
    load_icd_groups

    info "loading ICD chapters..."
    load_icd_chapters
  end

  def load_demographic_divisions
    info "loading administrative divisions..."

    @format.demographic_divisions.each do |division|
      root = division[:root].nil? ? @format.root : division[:root]

      unless division[:shape].nil?
        shape = division[:shape]

        paths = [Rails.root.join(*root, shape[:file])] unless shape[:file].nil?
        paths = Dir.glob(Rails.root.join(*root, 'Setor_with_pop*.json')) unless shape[:glob].nil?

        create_with_shape(division[:klass], paths, shape[:field], shape[:type])
      end

      unless division[:population_file].nil?
        path = Rails.root.join(*root, division[:population_file])
        update_population(division[:klass], path)
      end

      unless division[:centroids].nil?
        centroids = division[:centroids]
        update_centroid(division[:klass], Rails.root.join(*root, centroids[:file]), centroids[:column], centroids[:csv])
      end
    end
  end

  def load_health_facilities
    info "loading health facilities..."

    facilities_format = @format.health_facilities
    validate_health_facilities_format(facilities_format)

    facilities_file = Rails.root.join(*@format.root, facilities_format[:data_file])

    if FileTest.exist?(facilities_file)
      load_health_facilities_file(facilities_file, facilities_format)
    else
      warn 'Did not find the health facilities CSV file for seeds in vendor directory.'
    end

    facilities_format[:after].call unless facilities_format[:after].nil?
  end

  def load_procedures
    info "loading procedures..."

    procedures_format = @format.procedures
    validate_procedures_format(procedures_format)

    root_path = Rails.root.join(*@format.root)
    file_names = Dir['procedures*.csv', base: root_path]
    file_paths = file_names.map { |file_name| "#{root_path}/#{file_name}" }

    if file_paths.all? { |file_path| FileTest.exist?(file_path) }
      load_procedures_files(file_paths, procedures_format)
    else
      warn 'Did not find the file procedures CSVs for seeds on vendor directory.'
    end
  end

  def assert_divisions_shapes
    demographic_divisions = @format.demographic_divisions.map { |division| division[:klass] }
    demographic_divisions.each do |klass|
      if klass.where(shape: nil).exists?
        warn "There is a #{klass} instance with nil shape."
      end
    end
  end

  private

  def warn(message)
    Rails.logger.warn(message)
    puts "WARNING: #{message}\n"
  end

  def info(str)
    print "\r" + str + (" " * 40) + "\r"
  end
end
