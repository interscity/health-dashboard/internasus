require 'csv'

class RioDeJaneiroFormat
  def root
    %w[vendor rj].freeze
  end

  def demographic_divisions
    [
      {
        klass: State,
        shape: { file: 'rj_state.geojson', field: 'NM_UF', type: :name }
      },
      {
        klass: City,
        shape: { file: 'rj_cities.geojson', field: 'NM_MUN', type: :name }
      },
      {
        klass: CensusSector,
        shape: { file: 'rj_census_sectors.geojson', field: 'CD_GEOCODI', type: :code },
        centroids: { file: 'rj_census_sectors_centroids.csv', column: 'CD_GEOCODI',
                     csv: { quote_char: '"', col_sep: ',' } },
        population_file: 'rj_census_sectors_population.json'
      }
    ]
  end

  def health_facilities
    {
      data_file: 'rj_health_facilities.csv',
      csv: { quote_char: '"', col_sep: ',' },
      column_mappings: {
        cnes: 'CNES',
        name: 'NOME',
        phone: 'FONE',
        facility_type: 'TIPO',
        administration: 'GESTAO',
        beds: 'LEITOS',
        latitude: 'LAT',
        longitude: 'LONG'
      },
      demographic_divisions: [
        { klass: State, on: :name, column: 'UF'  },
        { klass: City,  on: :name, column: 'MUN' }
      ]
    }
  end

  def procedures
    {
      csv: { quote_char: '$', col_sep: ',' },
      column_mappings: {
        gender: 'SEXO',
        race: 'RACA_COR',
        age_year: 'IDADE',
        education_level: 'INSTRU',
        latitude: 'LAT',
        longitude: 'LONG',
        competence: 'CMPT',
        admission_date: 'DT_INTER',
        leave_date: 'DT_SAIDA',
        issue_date: '',
        facility: 'CNES',
        gestor_id: 'GESTOR_TP',
        hospitalization_group: 'PROC_REA',
        hospitalization_type: 'CAR_INT',
        diag_pr: 'DIAG_PR',
        diag_se1: 'DIAG_SE1',
        diag_se2: 'DIAG_SE2',
        specialty: 'ESPECIA',
        complexity: 'COMPLEX',
        hospitalization_rates: 'DIARIAS',
        ui_rates: 'DIARIAS_UI',
        uti_rates: 'DIARIAS_UTI',
        hospitalization_days: 'DIAS_PERM',
        financing: 'FINANC',
        service_value: 'VAL_TOT',
        distance: 'DISTANCIA'
      },
      demographic_divisions: [
        { klass: State,        on: :name, column: 'UF'       },
        { klass: City,         on: :name, column: 'MUN'      },
        { klass: CensusSector, on: :code, column: 'CD_SETOR' }
      ]
    }
  end
end
