require 'csv'

class SMSSPFormat
  def root
    %w[vendor sp].freeze
  end

  def demographic_divisions
    [
      {
        klass: City,
        shape: { file: 'cities.geojson', field: 'Name', type: :name }
      },
      {
        klass: FamilyHealthStrategy,
        shape: { file: 'sp_fhs.geojson', field: 'Name', type: :name }
      },
      {
        klass: RegionalHealthCoordination,
        shape: { file: 'sp_regional_health_coordination.geojson', field: 'Name', type: :name }
      },
      {
        klass: Subprefecture,
        shape: { file: 'sp_subprefectures.geojson', field: 'Name', type: :name }
      },
      {
        klass: TechnicalHealthSupervision,
        shape: { file: 'sp_technical_health_supervision.geojson', field: 'Name', type: :name }
      },
      {
        klass: Ubs,
        shape: { file: 'sp_ubs.geojson', field: 'Name', type: :name }
      },
      {
        klass: AdministrativeSector,
        shape: { file: 'sp_administrative_sectors.geojson', field: 'Name', type: :name },
        centroids: { file: 'admin_centroids.csv', column: 'name', csv: { quote_char: '$', col_sep: ',' } },
        population_file: 'administrative_sectors_population.json'
      },
      {
        root: root.dup << 'census_sectors',
        klass: CensusSector,
        shape: { glob: 'Setor_with_pop*.json', field: 'CD_GEOCODI', type: :code },
        centroids: { file: 'centroids.csv', column: 'code', csv: { quote_char: '$', col_sep: ',' } },
        population_file: 'census_sectors_population.json'
      }
    ]
  end

  def health_facilities
    {
      data_file: 'pmsp_health_centres.csv',
      csv: { quote_char: '$', col_sep: ',' },
      column_mappings: {
        cnes: 'CNES',
        name: 'CNES_DENO',
        latitude: 'LAT',
        longitude: 'LONG',
        facility_type: 'TIPO',
        phone: 'FONE',
        beds: 'LEITOS',
        administration: 'GESTAO'
      },
      demographic_divisions: [
        { klass: AdministrativeSector,       on: :name, column: 'DA'  },
        { klass: RegionalHealthCoordination, on: :name, column: 'CRS' },
        { klass: Subprefecture,              on: :name, column: 'PR'  },
        { klass: TechnicalHealthSupervision, on: :name, column: 'STS' }
      ],
      after: method(:load_beds)
    }
  end

  def procedures
    {
      csv: { quote_char: '$', col_sep: ',' },
      column_mappings: {
        age_year: 'P_IDADE',
        education_level: 'LV_INSTRU',
        gender: 'P_SEXO',
        latitude: 'LAT_SC',
        longitude: 'LONG_SC',
        race: 'P_RACA',
        facility: 'CNES',
        diag_pr: 'DIAG_PR',
        diag_se1: 'DIAG_SE1',
        diag_se2: 'DIAG_SE2',
        admission_date: 'DT_INTERNA',
        competence: 'CMPT',
        complexity: 'COMPLEXIDA',
        distance: 'DISTANCIA',
        financing: 'FINANCIAME',
        gestor_id: 'GESTOR_IDE',
        hospitalization_days: 'DIAS_PERM',
        hospitalization_group: 'PROC_RE',
        hospitalization_rates: 'DIARIAS',
        hospitalization_type: 'CAR_INTEN',
        issue_date: 'DT_EMISSAO',
        leave_date: 'DT_SAIDA',
        service_value: 'VAL_TOT',
        specialty: 'ESPECIALID',
        ui_rates: 'DIARIAS_UI',
        uti_rates: 'DIARIAS_UT'
      },
      demographic_divisions: [
        { klass: CensusSector,               on: :code, column: 'CD_GEOCODI' },
        { klass: AdministrativeSector,       on: :name, column: 'DA'         },
        { klass: RegionalHealthCoordination, on: :name, column: 'CRS'        },
        { klass: Subprefecture,              on: :name, column: 'PR'         },
        { klass: TechnicalHealthSupervision, on: :name, column: 'STS'        }
      ]
    }
  end

  private

  def load_beds
    beds_file = Rails.root.join(root.join('/'), 'beds_2018_2019.csv')
    CSV.foreach(beds_file, quote_char: '"', col_sep: ';', headers: true) do |data|
      health_facility = HealthFacility.find_by(cnes: data['CNES'])
      next if health_facility.nil?

      health_facility.beds = data['TOTAL_2019']
      health_facility.phone = data['TELEFONES']
      health_facility.facility_type = data['DescrTpServicoCnes']
      health_facility.save if health_facility.changed?
    end
  end
end
