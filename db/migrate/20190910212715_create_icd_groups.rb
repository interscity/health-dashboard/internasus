class CreateIcdGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :icd_groups do |t|
      t.string :first_category
      t.string :last_category
      t.text :description

      t.timestamps
      t.index [:first_category, :last_category], unique: true
    end
  end
end
