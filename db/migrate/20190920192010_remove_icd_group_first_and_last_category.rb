class RemoveIcdGroupFirstAndLastCategory < ActiveRecord::Migration[6.0]
  def change
    remove_index :icd_groups, [:first_category, :last_category]
    remove_column :icd_groups, :first_category
    remove_column :icd_groups, :last_category
  end
end
