class ChangeProcedureIdToString < ActiveRecord::Migration[6.0]
  def up
    change_column :patient_data, :procedure_id, :string
    change_column :hospitalization_data, :procedure_id, :string
  end

  def down
    change_column :patient_data, :procedure_id, :integer
    change_column :hospitalization_data, :procedure_id, :integer
  end
end
