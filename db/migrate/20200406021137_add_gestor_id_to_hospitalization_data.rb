class AddGestorIdToHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_column :hospitalization_data, :gestor_id, :integer
  end
end
