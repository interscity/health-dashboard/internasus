class RemovePopulationFromDemographicDivision < ActiveRecord::Migration[6.1]
  def change
    remove_column :demographic_divisions, :population, :jsonb
  end
end
