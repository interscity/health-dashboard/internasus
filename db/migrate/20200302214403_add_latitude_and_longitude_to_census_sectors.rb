class AddLatitudeAndLongitudeToCensusSectors < ActiveRecord::Migration[6.0]
  def change
    add_column :census_sectors, :latitude, :float
    add_column :census_sectors, :longitude, :float
  end
end
