class AddCityAndStateToHealthFacility < ActiveRecord::Migration[6.1]
  def change
    add_reference :health_facilities, :city
    add_reference :health_facilities, :state
  end
end
