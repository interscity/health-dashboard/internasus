class CreateUbs < ActiveRecord::Migration[6.0]
  def change
    create_table :ubs do |t|
      t.string :name
      t.string :shape

      t.timestamps
    end
  end
end
