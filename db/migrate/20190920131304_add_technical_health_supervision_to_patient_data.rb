class AddTechnicalHealthSupervisionToPatientData < ActiveRecord::Migration[6.0]
  def change
    add_reference :patient_data, :technical_health_supervision
  end
end
