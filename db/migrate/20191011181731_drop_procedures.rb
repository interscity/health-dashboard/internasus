class DropProcedures < ActiveRecord::Migration[6.0]
  def change
    drop_table :procedures
  end
end
