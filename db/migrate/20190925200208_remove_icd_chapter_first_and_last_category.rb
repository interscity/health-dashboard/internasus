class RemoveIcdChapterFirstAndLastCategory < ActiveRecord::Migration[6.0]
  def change
    remove_column :icd_chapters, :first_category
    remove_column :icd_chapters, :last_category
  end
end
