class AddTechnicalHealthSupervisionToHealthFacility < ActiveRecord::Migration[6.0]
  def change
    add_reference :health_facilities, :technical_health_supervision
  end
end
