class AddSubprefectureToPatientData < ActiveRecord::Migration[6.0]
  def change
    add_reference :patient_data, :subprefecture
  end
end
