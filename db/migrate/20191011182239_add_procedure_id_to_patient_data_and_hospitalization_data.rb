class AddProcedureIdToPatientDataAndHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_column :patient_data, :procedure_id, :integer
    add_column :hospitalization_data, :procedure_id, :integer
  end
end
