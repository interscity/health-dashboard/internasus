class AddIcdChapterToIcdGroup < ActiveRecord::Migration[6.0]
  def change
    add_reference :icd_groups, :icd_chapter
  end
end
