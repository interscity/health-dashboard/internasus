# frozen_string_literal: true

class RemoveAdministrativeAndCensusSectorAssociation < ActiveRecord::Migration[6.0]
  def change
    remove_column :census_sectors, :administrative_sector_id, :index
  end
end
