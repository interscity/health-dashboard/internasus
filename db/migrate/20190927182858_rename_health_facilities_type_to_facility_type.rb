class RenameHealthFacilitiesTypeToFacilityType < ActiveRecord::Migration[6.0]
  def change
    rename_column :health_facilities, :type, :facility_type
  end
end
