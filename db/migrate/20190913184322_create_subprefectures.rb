class CreateSubprefectures < ActiveRecord::Migration[6.0]
  def change
    create_table :subprefectures do |t|
      t.string :name
      t.string :shape

      t.timestamps
      t.index :name, unique: true
    end
  end
end
