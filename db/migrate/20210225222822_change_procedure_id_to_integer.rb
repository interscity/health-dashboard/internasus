class ChangeProcedureIdToInteger < ActiveRecord::Migration[6.0]
  def change
    remove_column :patient_data, :procedure_id, :string
    add_column :patient_data, :procedure_id, :integer
    add_index :patient_data, :procedure_id, unique: true
    remove_column :hospitalization_data, :procedure_id, :string
    add_column :hospitalization_data, :procedure_id, :integer
    add_index :hospitalization_data, :procedure_id, unique: true
  end
end
