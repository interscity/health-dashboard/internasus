class CreateHealthFacilities < ActiveRecord::Migration[6.0]
  def change
    create_table :health_facilities do |t|
      t.integer :cnes
      t.string :name
      t.float :latitude
      t.float :longitude
      t.string :type
      t.string :phone
      t.integer :beds
      t.string :administration

      t.timestamps
      t.index :cnes, unique: true
    end
  end
end
