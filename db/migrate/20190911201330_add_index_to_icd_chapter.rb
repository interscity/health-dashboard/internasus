class AddIndexToIcdChapter < ActiveRecord::Migration[6.0]
  def change
    change_table :icd_chapters do |t|
      t.index :code, unique: true
    end
  end
end
