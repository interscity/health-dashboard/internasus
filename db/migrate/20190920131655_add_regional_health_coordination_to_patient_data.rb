class AddRegionalHealthCoordinationToPatientData < ActiveRecord::Migration[6.0]
  def change
    add_reference :patient_data, :regional_health_coordination
  end
end
