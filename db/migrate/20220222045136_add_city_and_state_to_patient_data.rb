class AddCityAndStateToPatientData < ActiveRecord::Migration[6.1]
  def change
    add_reference :patient_data, :city
    add_reference :patient_data, :state
  end
end
