class AddPopulationJsonFieldToCensusSector < ActiveRecord::Migration[6.0]
  def change
    add_column :census_sectors, :population, :jsonb
  end
end
