class AddIcdChapterToIcdCategory < ActiveRecord::Migration[6.0]
  def change
    add_reference :icd_categories, :icd_chapter
  end
end
