class AddSecondaryIcdsToHospitalizationData < ActiveRecord::Migration[6.0]
  def change
    add_reference :hospitalization_data, :secondary_icd1, foreign_key: {to_table: :icd_subcategories}
    add_reference :hospitalization_data, :secondary_icd2, foreign_key: {to_table: :icd_subcategories}
  end
end
