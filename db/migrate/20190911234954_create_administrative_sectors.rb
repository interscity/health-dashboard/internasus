class CreateAdministrativeSectors < ActiveRecord::Migration[6.0]
  def change
    create_table :administrative_sectors do |t|
      t.string :name
      t.string :shape

      t.timestamps
      t.index :name, unique: true
    end
  end
end
