class AddRegionalHealthCoordinationToHealthFacility < ActiveRecord::Migration[6.0]
  def change
    add_reference :health_facilities, :regional_health_coordination
  end
end
