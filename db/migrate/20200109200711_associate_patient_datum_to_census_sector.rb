class AssociatePatientDatumToCensusSector < ActiveRecord::Migration[6.0]
  def change
    add_reference :patient_data, :census_sector
  end
end
