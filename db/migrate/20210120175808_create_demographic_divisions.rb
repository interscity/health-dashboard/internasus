# frozen_string_literal: true

class CreateDemographicDivisions < ActiveRecord::Migration[6.0]
  def change
    create_table :demographic_divisions do |t|
      t.string :name, null: false
      t.string :shape
      t.float :latitude
      t.float :longitude

      t.jsonb :population

      t.string :type

      t.timestamps
    end
  end
end
