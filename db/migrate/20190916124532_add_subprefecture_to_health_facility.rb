class AddSubprefectureToHealthFacility < ActiveRecord::Migration[6.0]
  def change
    add_reference :health_facilities, :subprefecture
  end
end
