# frozen_string_literal: true

class DropReplicateDemographicDivisionTables < ActiveRecord::Migration[6.0]
  def change
    drop_table(:administrative_sectors) {}
    drop_table(:census_sectors) {}
    drop_table(:cities) {}
    drop_table(:family_health_strategies) {}
    drop_table(:regional_health_coordinations) {}
    drop_table(:subprefectures) {}
    drop_table(:technical_health_supervisions) {}
    drop_table(:ubs) {}
  end
end
