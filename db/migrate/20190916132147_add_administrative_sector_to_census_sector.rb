class AddAdministrativeSectorToCensusSector < ActiveRecord::Migration[6.0]
  def change
    add_reference :census_sectors, :administrative_sector
  end
end
