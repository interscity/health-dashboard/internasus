export class CensusSectorsPopulationFilters {
  constructor(
    private genderFilters: string[],
    private raceFilters: string[],
    private jQuery: any
  ) {}

  checkbox(filter: string) {
    return this.jQuery(`input[name='csw_heat_map[${filter}]'][type=checkbox]`);
  }

  uncheckAll(filters: string[]) {
    for(const filter of filters) {
      this.checkbox(filter).prop('checked', false);
    }
  }

  changing(filter: string) {
    const checked = this.checkbox(filter).prop('checked');

    if(checked) {
      if(this.genderFilters.includes(filter)) {
        this.uncheckAll(this.raceFilters);
      } else {
        this.uncheckAll(this.genderFilters);
      }
    }
  }
}

window.CensusSectorsPopulationFilters = CensusSectorsPopulationFilters;
