export class FilterClearer {
  private jQuery: any;
  private proceduresMap: any;

  constructor(jQuery: any, proceduresMap: any) {
    this.jQuery = jQuery;
    this.proceduresMap = proceduresMap;
    jQuery('#clear-filters-button').click(() => this.clearFilters());
  }

  clearFilters() {
    const filters = this.jQuery('#filters-accordion');
    filters.find('select[multiple = multiple]').val([]).change();
    filters.find('input.datepicker').each((i, e) => {
      if (i === 0) {
        this.jQuery(e).val('2019-01-01');
      } else {
        this.jQuery(e).val('2019-12-31');
      }
    });
    filters.find('input[data-provide = slider]').each((i, e) => {
      const max = this.jQuery(e).data('slider').options.max;
      this.jQuery(e).slider('setValue', [0, max]).change();
    });

    this.proceduresMap.clearProcedures();
  }
}

window.FilterClearer = FilterClearer;
