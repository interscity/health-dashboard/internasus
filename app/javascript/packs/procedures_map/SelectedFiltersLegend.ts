export class SelectedFiltersLegend {
  private jQuery: any;
  private destinationId: string;
  private title: string;

  constructor(jQuery: any, destinationId: string, title: string) {
    this.jQuery = jQuery;
    this.destinationId = destinationId;
    this.title = title;
  }

  update(selectElement: any) {
    const options = selectElement.options;
    const destination = this.jQuery(`#${this.destinationId}`)
    const data = this.jQuery(document.createElement("ul"))
    let anySelected = false;

    destination.children().remove()

    for (const option of options) {
      if (option.selected) {
        data.append(`<li>${option.text}</li>`)
        anySelected = true;
      }
    }

    if (anySelected) {
      destination.append(`<h6 class="pt-2">${this.title}</h6>`, data)
    }
  }
}

window.SelectedFiltersLegend = SelectedFiltersLegend;
