export class LoadingOverlay {
  private jQuery: any;

  constructor(jQuery: any) {
    this.jQuery = jQuery;
  }

  showOverlay(): void {
    this.jQuery('#loading-overlay').modal('show');
  }

  showOverlayWithCallback(callback : () => void) : void {
    this.showOverlay();
    window.setTimeout(callback, 100);
  }

  hideOverlay(): void {
    this.jQuery('#loading-overlay').modal('hide');
  }
}

window.LoadingOverlay = LoadingOverlay;
