type patientDatum = {lat: number, lng: number, value: number};
type heatmapDataStruct = {max: number, data: patientDatum[]};
type searchResponseData = {
    health_facilities: any,
    patients_data: any,
    divisions_rates: any
};

export class ProceduresMap {
  private map: any;
  private patientMarkers: any[];
  private patientsDataCluster: any;
  private Leaflet: any;
  private jQuery: any;
  private loadingOverlay: any;
  private heatmapLayer: any;
  public heatmapPreData: {};
  public heatmapData: heatmapDataStruct;
  public cSWHeatmapData: heatmapDataStruct;
  private minimap: any;
  private limitsLayers: {};
  private cSWHeatmapLayer: any;
  public heatmapGradients = {
    default: {
      0.25: "#2bffd3",
      0.62: "#fffd57",
       1.0: "#f93434"
    },
    highContrast: {
      0.25: "#D3C9F8",
      0.55: "#7B5CEB",
      0.85: "#4E25E4",
       1.0: "#3816B3"
    }
  };
  public healthFacilitiesLayer: any;
  private healthFacilitiesPercentilesLayer: any;
  private percentileColors = {
    75: '#FF4444',
    50: '#44FF44',
    25: '#4444FF',
  };
  private errorMessage: string;
  private percentileTranslation: string;
  private radiusTranslation: string;
  public healthFacilityColors = {
    'E': '#1B9E77',
    'M': '#7570B3',
    'O': '#D95F02',
  };
  public minCircleMarkerRadius: number = 5;
  private hospitalizationRateRadiusMultiplier: number = 15000;

  constructor(divId: string, Leaflet: any, jQuery: any, loadingOverlay : any, heatmap: any, mapCenter: number[],
    errorMessage: string, percentileTranslation: string, radiusTranslation: string) {
    this.Leaflet = Leaflet;
    this.jQuery = jQuery;
    this.loadingOverlay = loadingOverlay;

    this.map = this.Leaflet.map(divId, {
      center: mapCenter,
      zoom: 11,
      maxZoom: 18
    });

    const osmUrl = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';
    const osmAttribution =
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'

    this.Leaflet.tileLayer(osmUrl,
                           { attribution: osmAttribution }).addTo(this.map);

    const minimapLayer = this.Leaflet.tileLayer(osmUrl, { attribution: osmAttribution });
    this.minimap = this.Leaflet.control.minimap(
      minimapLayer, { toggleDisplay: true }
    ).addTo(this.map);

    this.patientMarkers = [];
    this.patientsDataCluster = this.Leaflet.markerClusterGroup();
    this.heatmapLayer = new heatmap({
      "scaleRadius": true,
      "radius": 0.02,
      "maxOpacity": 0.8,
      "useLocalExtrema": true,
      gradient: this.heatmapGradients.default,
      valueField: 'value',
      onExtremaChange: this.heatmapListenerFor('procedures-heatmap')
    });

    this.limitsLayers = {};

    // Census Sector Weighted Heatmap Layer
    this.cSWHeatmapLayer = new heatmap({
      "scaleRadius": true,
      "radius": 0.02,
      "maxOpacity": 0.8,
      "useLocalExtrema": true,
      gradient: this.heatmapGradients.default,
      valueField: 'value',
      onExtremaChange: this.heatmapListenerFor('csw-heatmap')
    });

    this.heatmapPreData = {};
    this.heatmapData = {max: -1, data: []};
    this.cSWHeatmapData = {max: -1, data: []};

    this.healthFacilitiesLayer = this.Leaflet.layerGroup([]);
    this.healthFacilitiesPercentilesLayer = this.Leaflet.layerGroup([]);

    // Translations
    this.errorMessage = errorMessage;
    this.percentileTranslation = percentileTranslation;
    this.radiusTranslation = radiusTranslation;
  }

  heatmapListenerFor(idPrefix: string) {
    const jQuery = this.jQuery;

    return (event) => { jQuery(`#${idPrefix}-max`).text(event.max.toFixed(2)); }
  }

  clear(): void {
    this.clearProcedures();
    this.healthFacilitiesLayer.clearLayers();
  }

  clearProcedures(): void {
    this.patientsDataCluster.removeLayers(this.patientsDataCluster.getLayers());
    this.patientMarkers = [];

    this.heatmapData = {max: -1, data: []};
    this.cSWHeatmapData = {max: -1, data: []};
    this.heatmapLayer.setData({data: []});
    this.cSWHeatmapLayer.setData({data: []});

    this.healthFacilitiesPercentilesLayer.clearLayers();
  }

  currentHeatmapLayer(): string {
    let result = '';

    if (this.map.hasLayer(this.heatmapLayer)) {
      result = `procedures:${this.heatmapLayer._heatmap._store._max}`
    } else if (this.map.hasLayer(this.cSWHeatmapLayer)) {
      result = `csw:${this.cSWHeatmapLayer._heatmap._store._max.toFixed(2)}`
    }

    return result
  }

  initializeHeatmapLayersBackend() {
    this.map.addLayer(this.heatmapLayer)
    this.map.addLayer(this.cSWHeatmapLayer)
    this.map.removeLayer(this.heatmapLayer)
    this.map.removeLayer(this.cSWHeatmapLayer)
  }

  moveFacilitiesToFront(): void {
    this.healthFacilitiesLayer.eachLayer((layer) => {
      layer.bringToFront();
    });
  }

  populateHealthFacilities(hfData: any): void {
    // HfData format:
    // {
    //   facilities_data_array: [[administration, lat, long, hospRates, id, [[percentile, radius]...], ...],
    //   facilities_procedures_range: [min_procedures, max_procedures]
    // }
    hfData.facilities_data_array.forEach((hf) => {
      this.addHealthFacility(hf[0], hf[1], hf[2], hf[3], hf[4]);
      const percentiles = hf[5];
      percentiles.forEach((percentile) => {
        this.addHealthFacilityPercentile(hf[1], hf[2], percentile[0], percentile[1]);
      });
    });

    this.moveFacilitiesToFront();

    this.updateFacilitiesSizesLegend(hfData);
  }

  populateDivisionsRates(divisionsRatesData: any): void {
    // Division rates data is formatted as [[lat, long, rate], ...]
    divisionsRatesData.forEach((division) => {
      this.createCSWHMarker(division[0], division[1], division[2]);
    });
  }

  getHospitalizationRateFromMarkerRadius(radius: number): number {
    return radius * radius / this.hospitalizationRateRadiusMultiplier;
  }

  calculateFacilityRadius(hospitalizationRate: number): number {
    const circleRadius = Math.sqrt(hospitalizationRate * this.hospitalizationRateRadiusMultiplier);
    return Math.max(circleRadius, this.minCircleMarkerRadius);
  }

  calculateFacilityDiameter(hospitalizationRate: number): number {
    return this.calculateFacilityRadius(hospitalizationRate) * 2;
  }

  addHealthFacility(administration: string, latitude: number, longitude: number, hospitalizationRate: number,
    facilityId: number) {

    const marker = this.Leaflet.circleMarker(
      [
        latitude,
        longitude,
      ],
      {
        radius: this.calculateFacilityRadius(hospitalizationRate),
        stroke: false,
        color: this.healthFacilityColors[administration],
        fillOpacity: 0.6,
      }
    );

    marker.bindPopup((popup) => {
      return this.getPopupContent(`/health_facilities/${facilityId}`, popup, this.errorMessage);
    });

    marker.addTo(this.healthFacilitiesLayer);
  }

  setPopupContent(element: any, mapElement: any, content: string) {
    element.html(content);
    if (typeof mapElement[`update`] === 'function') mapElement.update();
  }

  getPopupContent(url: string, popup: any, errorMessage: string) {
    const el = this.jQuery('<div/>');
    el.css('width', '300px');

    this.jQuery.ajax({
      url,
      dataType: 'html',
      data: ''
    })
    .done((data) => { this.setPopupContent(el, popup, data); })
    .fail((error) => { this.setPopupContent(el, popup, errorMessage); });

    return el[0];
  }

  // patientsData should be formatted as [[lat, long, procedures_ids], ...]
  createPatientsDataMarkers(patientsData: any[]) {
    for (const value of patientsData) {
      const latitude = value[0];
      const longitude = value[1];
      const proceduresIds = value[2];

      for (const id of proceduresIds) {
        this.addPointToHeatmapPreData(latitude, longitude);

        const marker = this.Leaflet.marker([latitude, longitude]);

        marker.bindPopup((popup) => {
          return this.getPopupContent(`/procedures/${id}`, popup, this.errorMessage);
        });

        this.patientMarkers.push(marker);
      }
    }
  }

  addPointToHeatmapPreData(latitude: number, longitude: number) {
    if (!this.heatmapPreData[latitude]) {
      this.heatmapPreData[latitude] = {};
      this.heatmapPreData[latitude][longitude] = 1;
    } else {
      if (!this.heatmapPreData[latitude][longitude])
        this.heatmapPreData[latitude][longitude] = 0

      this.heatmapPreData[latitude][longitude]++;
    }
  }

  populateHeatmapData(): void {
    for (const latitude in this.heatmapPreData) {
      if (!this.heatmapPreData.hasOwnProperty(latitude))
        continue

      for (const longitude in this.heatmapPreData[latitude]) {
        if (!this.heatmapPreData[latitude].hasOwnProperty(longitude))
          continue

        this.heatmapData.data.push({
          lat: Number(latitude),
          lng: Number(longitude),
          value: this.heatmapPreData[latitude][longitude]
        });
      }
    }
  }

  populateSearchData(data: searchResponseData): void {
    this.populateHealthFacilities(data.health_facilities);
    this.createPatientsDataMarkers(data.patients_data);
    this.populateDivisionsRates(data.divisions_rates);
  }

  reloadWithData(data: searchResponseData): void {
    this.clear();

    this.populateSearchData(data);

    this.initializeHeatmapLayers();
    this.applyPatientCluster();
  }

  initializeHeatmapLayers(): void {
    this.populateHeatmapData();

    this.heatmapLayer.setData(this.heatmapData);
    this.cSWHeatmapLayer.setData(this.cSWHeatmapData);
  }

  invalidateSize() {
    this.map.invalidateSize();
  }

  generateClusterLayer(markers: any[], radius: number): any {
    const clusterGroup = this.Leaflet.markerClusterGroup({
      maxClusterRadius: radius
    });

    markers.forEach((marker) => { clusterGroup.addLayer(marker) });

    return clusterGroup;
  }

  toggleCluster() {
    this.toggleLegendDiv('cluster');

    if(this.map.hasLayer(this.patientsDataCluster)) {
      this.map.removeLayer(this.patientsDataCluster);
    } else {
      this.map.addLayer(this.patientsDataCluster);
    }
  }

  kilometresToPixels(kilometres:number): number {
    const latitude = -23.557296000000001;
    const c = 40075016.686;
    const toRad = Math.PI / 180;
    const zoom = this.map.getZoom();
    const metres = kilometres * 1e3;

    const scale = c * Math.abs(Math.cos(latitude * toRad)) / Math.pow(2, zoom + 8);

    return Math.ceil(metres / scale);
  }

  applyPatientCluster(radius: number = 80): void {
    const patientsCluster = this.generateClusterLayer(this.patientMarkers, radius);

    if(this.map.hasLayer(this.patientsDataCluster)) {
      this.toggleCluster()
      this.patientsDataCluster = patientsCluster;
      this.toggleCluster()
    } else {
      this.patientsDataCluster = patientsCluster;
    }
  }

  clusterRadiusEvent(slider: any): void {
    this.setLegendValue('cluster-radius', slider.value);

    const radiusInKilometers = slider.value;
    const radiusInPixels = this.kilometresToPixels(radiusInKilometers);

    this.applyPatientCluster(radiusInPixels);
  }

  toggleHeatmap() {
    this.toggleLegendDiv('heatmap-by-count');
    this.toggleDiv('procedures-heatmap-legend');

    if(this.map.hasLayer(this.heatmapLayer)) {
      this.map.removeLayer(this.heatmapLayer);
    } else {
      this.map.addLayer(this.heatmapLayer);
    }
  }

  setDataSearchCallbacks(id) {
    this.jQuery(id).on("ajax:success", (event) => this.dataSearchSuccess(event));
    this.jQuery(id).on("ajax:error", (event) => this.dataSearchError());
  }

  dataSearchSuccess(event: any): void {
    const [_data, _status, xhr] = event.detail;
    const data = JSON.parse(xhr.responseText);

    this.reloadWithData(data);
    this.loadingOverlay.hideOverlay();
  }

  dataSearchError(): void {
    alert(this.errorMessage);
    this.loadingOverlay.hideOverlay();
  }

  heatmapRadiusEvent(slider: any): void {
    this.setLegendValue('heatmap-radius', slider.value);

    const radiusPixels = slider.value * 0.01;

    this.heatmapLayer.cfg.radius = radiusPixels;
    this.cSWHeatmapLayer.cfg.radius = radiusPixels;

    if (this.map.hasLayer(this.heatmapLayer)) {
      this.heatmapLayer._update()
    }

    if (this.map.hasLayer(this.cSWHeatmapLayer)) {
      this.cSWHeatmapLayer._update()
    }
  }

  toggleHeatmapHighContrast(): void {
    this.toggleLegendDiv('heatmap-high-contrast');

    const highContrastEnabled =
      this.heatmapLayer._heatmap._config.gradient[1] === this.heatmapGradients.default[1];

    const index = highContrastEnabled? "highContrast" : "default"

    const agradient = this.heatmapGradients[index];

    this.heatmapLayer._heatmap.configure({gradient: agradient})
    this.cSWHeatmapLayer._heatmap.configure({gradient: agradient})

    this.jQuery('.heatmap-legend')
        .css('background-image' , this.heatmapGradientCSS())
  }

  heatmapGradientCSS(): string {
    const gradient = this.heatmapLayer._heatmap._config.gradient;
    return `linear-gradient(to right, ${this.gradientToString(gradient)})`
  }

  gradientToString(gradient: {}): string {
    return Object.keys(gradient)
                 .sort()
                 .map((x) => `${gradient[x]} ${Number(x) * 100}%`)
                 .join(', ')
  }


  heatmapOpacityEvent(slider: any): void {
    this.setLegendValue('heatmap-opacity', slider.value);

    this.jQuery('.heatmap-canvas').css('opacity', slider.value);
  }

  buildLimitsLayer(identifier: string, geoJSON: any): void {
    const style: {[key: string]: any} = {
        'color': '#444444',
        'opacity': 0.6,
        'stroke': true,
        'fill': true,
        'fillOpacity': 0.05,
    };
    if (identifier === 'FamilyHealthStrategy') {
      style[`fillOpacity`] = 0.2;
      style[`fillColor`] = '#9B1D03';
    }

    this.limitsLayers[identifier] = this.Leaflet.geoJSON(
      geoJSON,
      {
        style,
        renderer: this.Leaflet.canvas(),
        onEachFeature: (feature, layer) => { this.addPopupToLayer(layer); }
      }
    );
  }

  addPopupToLayer(layer: any) {
    layer.bindPopup((popup) => {
      return this.getPopupContent(layer.feature.properties.dataUrl, popup, layer.feature.properties.errorMessage);
    });
  }

  toggleLimits(identifier: string): void {
    const layer = this.limitsLayers[identifier];
    if (layer !== undefined) {
      this.toggleLimitsLayer(identifier, layer);
    } else {
      this.getGeoJsonData(identifier);
    }
  }

  toggleLimitsWithOverlay(identifier: string): void {
    this.loadingOverlay.showOverlayWithCallback(() => this.toggleLimits(identifier));
  }

  getGeoJsonData(identifier: string): void {
    this.jQuery.get('/demographic_divisions/geojson/' + identifier)
      .done((geoJSON) => {
        this.buildLimitsLayer(identifier, geoJSON);
        const layer = this.limitsLayers[identifier];
        this.toggleLimitsLayer(identifier, layer);
      });
  }

  toggleLimitsLayer(identifier: string, layer: any): void {
    this.toggleLegendDiv(`limits-${identifier}`);

    if(this.map.hasLayer(layer)) {
      this.map.removeLayer(layer);
    } else {
      this.map.addLayer(layer);
    }

    this.loadingOverlay.hideOverlay();
  }

  createCSWHMarker(latitude: number, longitude: number, rate: number) { // Create Census Sector Weighted Heatmap Marker
    this.cSWHeatmapData.data.push({lat: latitude, lng: longitude, value: rate});
  }

  toggleCSWHeatmap() { // Toggle Census Sector Weighted Heatmap
    this.toggleLegendDiv('heatmap-by-csw');
    this.toggleDiv('csw-heatmap-legend');

    if(this.map.hasLayer(this.cSWHeatmapLayer)) {
      this.map.removeLayer(this.cSWHeatmapLayer);
    } else {
      this.map.addLayer(this.cSWHeatmapLayer);
    }
  }

  toggleHealthFacilities() {
    this.toggleLegendDiv('facilities-icons');

    if(this.map.hasLayer(this.healthFacilitiesLayer)) {
      this.map.removeLayer(this.healthFacilitiesLayer);
    } else {
      this.map.addLayer(this.healthFacilitiesLayer);
    }
  }

  addHealthFacilityPercentile(latitude: number, longitude: number, percentile: number, radius: number) {
    const circle = this.Leaflet.circle(
      [
        latitude,
        longitude,
      ],
      {
        color: this.percentileColors[percentile],
        fillColor: this.percentileColors[percentile],
        fillOpacity: 0.2,
        radius: radius * 1000
      }
    );

    circle.bindTooltip(
      `<b>${this.percentileTranslation}</b>: ${percentile}%<br><b>${this.radiusTranslation}</b>: ${radius} Km`,
      {direction:'top'}
    );
    circle.addTo(this.healthFacilitiesPercentilesLayer);
  }

  toggleHealthFacilitiesPercentiles() {
    this.toggleLegendDiv('facilities-percentiles');

    if(this.map.hasLayer(this.healthFacilitiesPercentilesLayer)) {
      this.map.removeLayer(this.healthFacilitiesPercentilesLayer);
    } else {
      this.map.addLayer(this.healthFacilitiesPercentilesLayer);
      this.moveFacilitiesToFront();
    }
  }

  createInfoBox(content: string) {
    const box = this.Leaflet.control({ position: 'bottomleft' });
    box.onAdd = () => {
      const div = this.Leaflet.DomUtil.create('div', 'leaflet-infobox');
      div.innerHTML = content;
      return div;
    };
    box.addTo(this.map);
  }

  countLengendListVisibleItems(id: string) {
    return this.jQuery(`#map-options-legend-${id} li`).filter(function() {
      return this.style.display !== 'none';
    }).length;
  }

  getFacilitiesLegendDiameters(hfDataArray: any): number[] {
    const minDiameter = this.minCircleMarkerRadius * 2;

    switch (hfDataArray.length) {
      case 0: {
        return [minDiameter, minDiameter, minDiameter];
      }
      case 1: {
        // Makes the sizing more consistent than simply dividing by 2
        const hospRateFromMinRadius = this.getHospitalizationRateFromMarkerRadius(this.minCircleMarkerRadius);
        const maxHospRate = hfDataArray[0][3];
        return [
          this.calculateFacilityDiameter(maxHospRate),
          this.calculateFacilityDiameter((hospRateFromMinRadius + maxHospRate) / 2),
          minDiameter
        ];
      }
      default: {
        // Assuming hfData is ordered by hospitalizationRate we can get the smallest and biggest facilities by index
        const maxHospRate = hfDataArray[0][3];
        const minHospRate = hfDataArray[hfDataArray.length - 1][3];
        return [
          this.calculateFacilityDiameter(maxHospRate),
          this.calculateFacilityDiameter((maxHospRate + minHospRate) / 2),
          this.calculateFacilityDiameter(minHospRate)
        ];
      }
    }
  }

  updateFacilitiesLegendMarkersSizes(hfDataArray: any) {
    const facilitiesDiameters = this.getFacilitiesLegendDiameters(hfDataArray);

    // 50% bigger then the biggest children circle, this gives more then enough space for the circles
    this.jQuery('.circle-marker-legend').css('width', facilitiesDiameters[0] * 1.5);

    this.jQuery('#facilities-rates-legend div.rounded-circle').each((idx, element) => {
      const el = this.jQuery(element);
      el.width(facilitiesDiameters[idx]);
      el.height(facilitiesDiameters[idx]);
    });
  }

  getFacilitiesLegendProcedures(hfData: any): number[] {
    const hfProcRange = hfData.facilities_procedures_range;

    switch (hfData.facilities_data_array.length) {
      case 0: {
        return [0, 0, 0];
      }
      case 1: {
        const maxProcedures = hfProcRange[1];
        return [maxProcedures, maxProcedures / 2, 0];
      }
      default: {
        const maxProcedures = hfProcRange[1];
        const minProcedures = hfProcRange[0];
        return [maxProcedures, Math.floor((maxProcedures + minProcedures) / 2), minProcedures];
      }
    }
  }

  updateFacilitiesLegendMarkersLabels(hfData: any) {
    const facilitiesProcedures = this.getFacilitiesLegendProcedures(hfData);
    this.jQuery('#facilities-rates-legend span').each((idx, item) => this.jQuery(item).text(facilitiesProcedures[idx]));
  }

  updateFacilitiesSizesLegend(hfData) {
    this.updateFacilitiesLegendMarkersSizes(hfData.facilities_data_array);
    this.updateFacilitiesLegendMarkersLabels(hfData);
  }

  updateLegend() {
    if (this.countLengendListVisibleItems('heatmap-by') === 0) {
      this.jQuery('#map-options-legend-heatmap').hide();
    } else {
      this.jQuery('#map-options-legend-heatmap').show();
    }

    if (this.countLengendListVisibleItems('limits-list') === 0) {
      this.jQuery('#map-options-legend-limits').hide();
    } else {
      this.jQuery('#map-options-legend-limits').show();
    }

    if (this.countLengendListVisibleItems('facilities-list') === 0) {
      this.jQuery('#map-options-legend-facilities').hide();
    } else {
      this.jQuery('#map-options-legend-facilities').show();
    }
  }

  toggleLegendDiv(id: string) {
    this.jQuery(`#map-options-legend-${id}`).toggle();
    this.updateLegend();
  }

  toggleDiv(id: string): void {
    this.jQuery(`#${id}`).toggle();
  }

  setLegendValue(id: string, value: any) {
    this.jQuery(`#map-options-legend-${id}`).html(value);
  }
}

window.ProceduresMap = ProceduresMap;
