type formInput = {id: string, val: any};

export class ImageGenerator {
  private html2canvas: any;
  private jQuery: any;
  private form: any;
  private inputs: formInput[];

  constructor(html2canvas: any, jQuery: any, formId: string, inputs: formInput[]) {
    this.html2canvas = html2canvas;
    this.jQuery = jQuery;

    this.inputs = inputs;

    this.form = this.jQuery(`#${formId}`);
  }

  html2canvasWrapper(node: any, callback: (canvas: any) => void): void {
    const child = node.querySelector("[style*='display: block']")
    if (child != null) child.style.removeProperty('height');

    this.html2canvas(node, {
      allowTaint: true,
      useCORS: true,
      scrollX: 0,
      scrollY: -window.scrollY
    }).then(callback);
  }

  populateInput(input: formInput, canvas: any): void {
    let val = input.val;
    if (typeof val === 'function') {
      val = val(canvas);
    }

    this.jQuery(`#${input.id}`).val(val);
  }

  populateInputs(canvas): void {
    this.inputs.forEach((input) => this.populateInput(input, canvas));
  }

  generate(targetId: string): void {
    const target = this.jQuery(`#${targetId}`)[0];

    this.html2canvasWrapper(target, (canvas) => this.generationCallback(canvas));
  }

  generationCallback(canvas: any) {
    const img = canvas.toDataURL('image/png');

    this.populateInputs(canvas);
    this.form.submit();
  }
}

window.ImageGenerator = ImageGenerator;
