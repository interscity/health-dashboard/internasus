# frozen_string_literal: true

class Population < ApplicationRecord
  belongs_to :demographic_division, inverse_of: :population
end
