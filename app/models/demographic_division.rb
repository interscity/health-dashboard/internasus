# frozen_string_literal: true

class DemographicDivision < ApplicationRecord
  has_one :population, inverse_of: :demographic_division, dependent: :destroy
end
