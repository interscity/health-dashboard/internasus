# frozen_string_literal: true

class IcdGroup < ApplicationRecord
  has_many :icd_categories, dependent: :nullify
  belongs_to :icd_chapter, optional: true
end
