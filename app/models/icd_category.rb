# frozen_string_literal: true

class IcdCategory < ApplicationRecord
  has_many :icd_subcategories, dependent: :destroy
  belongs_to :icd_group, optional: true
  belongs_to :icd_chapter, optional: true
  has_many :hospitalization_data, dependent: :nullify
end
