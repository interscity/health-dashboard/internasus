# frozen_string_literal: true

class CensusSector < DemographicDivision
  has_many :patient_data, dependent: :nullify

  alias_attribute :code, :name
end
