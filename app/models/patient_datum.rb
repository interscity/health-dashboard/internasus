# frozen_string_literal: true

class PatientDatum < ApplicationRecord
  belongs_to :city, optional: true
  belongs_to :state, optional: true
  belongs_to :administrative_sector, optional: true
  belongs_to :subprefecture, optional: true
  belongs_to :technical_health_supervision, optional: true
  belongs_to :regional_health_coordination, optional: true
  belongs_to :census_sector, optional: true
  belongs_to :hospitalization_datum, inverse_of: :patient_datum,
                                     foreign_key: 'procedure_id',
                                     primary_key: 'procedure_id'
end
