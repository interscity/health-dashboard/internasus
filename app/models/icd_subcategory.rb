# frozen_string_literal: true

class IcdSubcategory < ApplicationRecord
  belongs_to :icd_category
  has_many :secondary_disgnosis_1, as: :secondary_icd1, dependent: :nullify
  has_many :secondary_disgnosis_2, as: :secondary_icd2, dependent: :nullify
  has_many :hospitalization_data, dependent: :nullify
end
