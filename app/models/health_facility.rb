# frozen_string_literal: true

class HealthFacility < ApplicationRecord
  belongs_to :state, optional: true
  belongs_to :city, optional: true
  belongs_to :administrative_sector, optional: true
  belongs_to :subprefecture, optional: true
  belongs_to :technical_health_supervision, optional: true
  belongs_to :regional_health_coordination, optional: true
  has_many :hospitalization_data, dependent: :nullify

  def hospitalization_rate
    hospitalization_data.count / HospitalizationDatum.count.to_f
  end

  def self.administrations
    select(:administration).distinct.pluck(:administration)
  end
end
