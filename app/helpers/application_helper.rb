# frozen_string_literal: true

module ApplicationHelper
  def flash_class(level)
    {
      'notice' => 'alert alert-info',
      'success' => 'alert alert-success',
      'error' => 'alert alert-danger',
      'alert' => 'alert alert-warning'
    }[level]
  end

  def human_attribute_value(model_instance, attribute_name)
    value = model_instance.send(attribute_name)
    model_key = model_instance.class.to_s.underscore

    I18n.t("#{model_key}.#{attribute_name}_#{value}")
  end
end
