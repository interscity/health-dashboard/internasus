# frozen_string_literal: true

module HealthFacilityHelper
  def health_facilities_options
    HealthFacility.distinct.where.not(name: nil)
                  .pluck(:cnes, :name, :id).map do |option|
      ["#{option[0]} - #{option[1].titleize}", option[2]]
    end
  end

  def administration_options
    HealthFacility.distinct.where.not(administration: nil)
                  .pluck(:administration).map(&:titleize)
  end

  def max_beds
    HealthFacility.maximum(:beds)
  end

  def model_search_options(model_class)
    model_class
      .distinct
      .where
      .not(name: nil)
      .order(:name)
      .pluck(:name, :id)
      .map do |option|
        option[0] = option[0].titleize
        option
      end
  end

  def health_facility_icons
    HealthFacility.administrations.index_with { |adm| image_path("#{adm}_hf_icon.png") }.to_json
  end

  def prepare_health_facilities_procedures_range(health_facilities_data)
    procs_per_facility = HospitalizationDatum.select('COUNT(*) as procs')
                                             .where(health_facility_id: health_facilities_data.ids)
                                             .group('health_facility_id')

    HospitalizationDatum.from(procs_per_facility).pick(Arel.sql('MIN(procs)'), Arel.sql('MAX(procs)'))
  end

  def health_facilities_data_source(health_facilities, calculate_percentiles)
    health_facilities.select(*facilities_select_args(calculate_percentiles))
                     .joins(:hospitalization_data)
                     .group('health_facilities.id')
  end

  def facilities_select_args(calculate_percentiles)
    args = ['health_facilities.*', 'COUNT(*)::real / (SELECT COUNT(*) FROM hospitalization_data) AS hosp_rate']
    return args unless calculate_percentiles

    args + [Arel.sql('PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY distance) AS percentile_75'),
            Arel.sql('PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY distance) AS percentile_50'),
            Arel.sql('PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY distance) AS percentile_25')]
  end

  def prepare_health_facilities_data_array(health_facilities, calculate_percentiles)
    percentile_cols = calculate_percentiles ? %i[percentile_75 percentile_50 percentile_25] : []

    HealthFacility.from(health_facilities_data_source(health_facilities, calculate_percentiles))
                  .order(hosp_rate: :desc)
                  .pluck(:administration, :latitude, :longitude, :hosp_rate, :id, *percentile_cols)
                  .map { |v| v[0..4] << (calculate_percentiles ? [['75', v[5]], ['50', v[6]], ['25', v[7]]] : []) }
  end

  def prepare_health_facilities_data(health_facilities, calculate_percentiles)
    {
      facilities_data_array: prepare_health_facilities_data_array(health_facilities, calculate_percentiles),
      facilities_procedures_range: prepare_health_facilities_procedures_range(health_facilities)
    }
  end
end
