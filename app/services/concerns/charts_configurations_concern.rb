# frozen_string_literal: true

def divisions
  keys = %i[administrative_sector subprefecture technical_health_supervision regional_health_coordination].map! do |v|
    "#{v}s_dynamic".to_sym if PatientDatum.where.not("#{v}_id".to_sym => nil).any?
  end
  keys.compact!
  keys || []
end

# rubocop:disable Metrics/ModuleLength

module ChartsConfigurationsConcern
  extend ActiveSupport::Concern

  DYNAMIC_KEYS = (%i[health_facilities_dynamic competences_dynamic hospitalization_groups_dynamic specialties_dynamic
                     hospitalization_types_dynamic icd_categories_dynamic icd_secondary_categories_dynamic
                     icd_secondary_2_categories_dynamic complexities_dynamic financing_types_dynamic
                     ages_ordered_by_code_dynamic races_dynamic educational_levels_dynamic
                     administrations_dynamic hospitalization_days_dynamic uti_rates_dynamic ui_rates_dynamic
                     hospitalization_rates_dynamic service_values_dynamic distances_dynamic] + divisions).freeze
  RANKING_KEYS = if PatientDatum.where.not(administrative_sector_id: nil).any?
                   %i[health_facilities_ranking administrative_sectors age_codes genders icd_categories]
                 else
                   %i[health_facilities_ranking age_codes genders icd_categories].freeze
                 end.freeze
  COLORS = ['#d48265', '#61a0a8', '#2f4554', '#c23531', '#91c7ae', '#749f83',
            '#ca8622', '#bda29a', '#6e7074', '#546570', '#c4ccd3'].freeze
  CATEGORY_Y = { xAxis: { type: 'value' }, yAxis: { type: 'category' } }.freeze
  CATEGORY_X = { xAxis: { type: 'category' }, yAxis: { type: 'value' } }.freeze
  SLIDER_X = { id: 'dataZoomX', type: 'slider', xAxisIndex: [0], filterMode: 'filter' }.freeze
  SLIDER_Y = { id: 'dataZoomY', type: 'slider', yAxisIndex: [0], filterMode: 'empty'  }.freeze
  TERRITORY_BAR_CHART_INFO = {
    chart_type: 'bar', chart_options: { percentile: true, dataZoom: [SLIDER_X, SLIDER_Y] }.merge!(CATEGORY_Y)
  }.freeze
  DYNAMIC_BAR_CHART_INFO = {
    chart_title: '', chart_type: 'bar', chart_options: { dataZoom: [SLIDER_X, SLIDER_Y] }.merge!(CATEGORY_Y)
  }.freeze
  CHART_INFO = {
    historic_series: {
      chart_title: '', chart_type: 'line', data: :historic_series,
      chart_options: { percentage: true }.merge!(CATEGORY_X)
    },
    specialties_bar: {
      chart_title: I18n.t('general_data.specialties.specialties_bar_chart_title'), chart_type: 'bar',
      data: :specialties, chart_options: { colors: :specialties, percentage: true }.merge!(CATEGORY_Y)
    },
    specialties_pie: {
      chart_title: I18n.t('general_data.specialties.specialties_pie_chart_title'), chart_type: 'pie',
      data: :specialties, chart_options: { colors: :specialties }
    },
    administrations_bar: {
      chart_title: I18n.t('general_data.health_facilities.administration_pie_chart_title'), chart_type: 'bar',
      data: :administrations, chart_options: { colors: :administrations, percentage: true }.merge!(CATEGORY_Y)
    },
    administrations_pie: {
      chart_title: I18n.t('general_data.health_facilities.administration_pie_chart_title'), chart_type: 'pie',
      data: :administrations, chart_options: { colors: :administrations }
    },
    facility_types_bar: {
      chart_title: I18n.t('general_data.health_facilities.facility_types_pie_chart_title'), chart_type: 'bar',
      data: :facility_types, chart_options: { colors: :facility_types, percentage: true }.merge!(CATEGORY_Y)
    },
    facility_types_pie: {
      chart_title: I18n.t('general_data.health_facilities.facility_types_pie_chart_title'), chart_type: 'pie',
      data: :facility_types, chart_options: { colors: :facility_types }
    },
    beds: {
      chart_title: I18n.t('general_data.health_facilities.number_of_beds_bar_chart_title'), chart_type: 'bar',
      data: :beds, chart_options: { percentage: true }.merge!(CATEGORY_Y)
    },
    distances_buckets: {
      chart_title: I18n.t('general_data.distance.distance_bucket_chart_title'), chart_type: 'pie',
      data: :distances_buckets, chart_options: { colors: :distances_buckets }
    },
    specialties_means: {
      chart_title: I18n.t('general_data.distance.mean_distance_by_specialty_chart_title'), chart_type: 'bar',
      data: :specialties_means, chart_options: { colors: :specialties }.merge!(CATEGORY_Y)
    },
    genders: {
      chart_title: I18n.t('general_data.hospitalizations.hospitalizations_per_gender_title'), chart_type: 'pie',
      data: :genders, chart_options: { colors: :genders }
    },
    ages_ordered_by_code: {
      chart_title: I18n.t('general_data.hospitalizations.hospitalizations_per_age_code_title'), chart_type: 'bar',
      data: :ages_ordered_by_code, chart_options: { percentage: true }.merge!(CATEGORY_Y)
    },
    hospitalization_days: {
      chart_title: I18n.t('general_data.hospitalizations.hospitalizations_per_days_title'), chart_type: 'bar',
      data: :hospitalization_days, chart_options: { percentage: true, dataZoom: [SLIDER_X] }.merge!(CATEGORY_Y)
    },
    hospitalization_types: {
      chart_title: I18n.t('general_data.hospitalizations.hospitalizations_per_type_title'), chart_type: 'pie',
      data: :hospitalization_types, chart_options: { colors: :hospitalization_types }
    },
    races: {
      chart_title: I18n.t('general_data.hospitalizations.hospitalizations_per_race_title'), chart_type: 'pie',
      data: :races, chart_options: { colors: :races }
    },
    administrative_sectors_by_name: {
      chart_title: I18n.t('general_data.territory.procedures_by_administrative_sector'),
      data: :administrative_sectors_by_name
    }.merge!(TERRITORY_BAR_CHART_INFO),
    regional_health_coordinations: {
      chart_title: I18n.t('general_data.territory.procedures_by_regional_health_coordinator'),
      data: :regional_health_coordinations
    }.merge!(TERRITORY_BAR_CHART_INFO),
    technical_health_supervisions: {
      chart_title: I18n.t('general_data.territory.procedures_by_technical_health_supervision'),
      data: :technical_health_supervisions
    }.merge!(TERRITORY_BAR_CHART_INFO),
    administrative_sectors_by_name_map: {
      chart_title: I18n.t('general_data.territory.administrative_sectors_map'), chart_type: 'map',
      data: :administrative_sectors_by_name, chart_area: 'administrative_sector'
    },
    regional_health_coordinations_map: {
      chart_title: I18n.t('general_data.territory.regional_health_coordinations_map'), chart_type: 'map',
      data: :regional_health_coordinations, chart_area: 'regional_health_coordination'
    },
    technical_health_supervisions_map: {
      chart_title: I18n.t('general_data.territory.technical_health_supervisions_map'), chart_type: 'map',
      data: :technical_health_supervisions, chart_area: 'technical_health_supervision'
    },
    subprefectures_map: {
      chart_title: I18n.t('general_data.territory.subprefectures_map'), chart_type: 'map',
      data: :subprefectures, chart_area: 'subprefecture'
    },
    health_facilities_dynamic: { data: :health_facilities }.merge!(DYNAMIC_BAR_CHART_INFO),
    competences_dynamic: {
      chart_title: '', chart_type: 'line', data: :competences,
      chart_options: { dataZoom: [SLIDER_X, SLIDER_Y] }.merge!(CATEGORY_X)
    },
    hospitalization_groups_dynamic: {
      chart_title: '', chart_type: 'treemap', treemap_type: 'HOSP_GROUP', data: :hospitalization_groups,
      chart_options: { rootName: I18n.t('general_data.dynamic.hospitalization_groups_dynamic') }
    },
    specialties_dynamic: {
      chart_title: '', chart_type: 'pie', data: :specialties, chart_options: { colors: :specialties }
    },
    hospitalization_types_dynamic: {
      chart_title: '', chart_type: 'pie', data: :hospitalization_types,
      chart_options: { colors: :hospitalization_types }
    },
    icd_categories_dynamic: {
      chart_title: '', chart_type: 'treemap', treemap_type: 'ICD', data: :icd_categories,
      chart_options: { rootName: I18n.t('general_data.dynamic.procedures') }
    },
    icd_secondary_categories_dynamic: {
      chart_title: '', chart_type: 'treemap', treemap_type: 'ICD', data: :icd_secondary_categories,
      chart_options: { rootName: I18n.t('general_data.dynamic.procedures') }
    },
    icd_secondary_2_categories_dynamic: {
      chart_title: '', chart_type: 'treemap', treemap_type: 'ICD', data: :icd_secondary_2_categories,
      chart_options: { rootName: I18n.t('general_data.dynamic.procedures') }
    },
    complexities_dynamic: {
      chart_title: '', chart_type: 'bar', data: :complexities,
      chart_options: { colors: :complexities }.merge!(CATEGORY_X)
    },
    financing_types_dynamic: {
      chart_title: '', chart_type: 'pie', data: :financing_types, chart_options: { colors: :financing_types }
    },
    ages_ordered_by_code_dynamic: { data: :ages_ordered_by_code }.merge!(DYNAMIC_BAR_CHART_INFO),
    races_dynamic: {
      chart_title: '', chart_type: 'pie', data: :races, chart_options: { colors: :races }
    },
    educational_levels_dynamic: {
      chart_title: '', chart_type: 'pie', data: :educational_levels, chart_options: { colors: :educational_levels }
    },
    administrative_sectors_dynamic: {
      chart_title: '', chart_type: 'map', data: :administrative_sectors, chart_area: 'administrative_sector'
    },
    subprefectures_dynamic: {
      chart_title: '', chart_type: 'map', data: :subprefectures, chart_area: 'subprefecture'
    },
    technical_health_supervisions_dynamic: {
      chart_title: '', chart_type: 'map', data: :technical_health_supervisions,
      chart_area: 'technical_health_supervision'
    },
    regional_health_coordinations_dynamic: {
      chart_title: '', chart_type: 'map', data: :regional_health_coordinations,
      chart_area: 'regional_health_coordination'
    },
    administrations_dynamic: {
      chart_title: '', chart_type: 'pie', data: :administrations, chart_options: { colors: :administrations }
    },
    hospitalization_days_dynamic: { data: :hospitalization_days }.merge!(DYNAMIC_BAR_CHART_INFO),
    uti_rates_dynamic: { data: :uti_rates }.merge!(DYNAMIC_BAR_CHART_INFO),
    ui_rates_dynamic: { data: :ui_rates }.merge!(DYNAMIC_BAR_CHART_INFO),
    hospitalization_rates_dynamic: { data: :hospitalization_rates }.merge!(DYNAMIC_BAR_CHART_INFO),
    service_values_dynamic: { data: :service_values }.merge!(DYNAMIC_BAR_CHART_INFO),
    distances_dynamic: { data: :distances }.merge!(DYNAMIC_BAR_CHART_INFO),
    health_facilities_specialty_distances: {
      chart_title: I18n.t('general_data.health_facilities_specialty_distances.chart_title'), chart_type: 'stacked_bar',
      data: :health_facilities_specialty_distances,
      chart_options: {
        colors: :distances_buckets, xAxis: { type: 'value', max: 100, axisLabel: { formatter: '{value}%' } },
        yAxis: { type: 'category', axisLabel: { interval: 0 } }, legend: { right: 20, bottom: 10 }, numberOfBars: 4
      }
    }
  }.freeze
  CHART_INFO.select { |_k, v| v.dig :chart_options, :colors }.each_value { |v| v[:chart_options].freeze }
end

# rubocop:enable Metrics/ModuleLength
