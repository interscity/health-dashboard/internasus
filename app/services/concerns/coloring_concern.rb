# frozen_string_literal: true

module ColoringConcern
  extend ActiveSupport::Concern

  def color_charts(data_hash, color_hashes)
    data_hash.each_value do |value|
      if value[:chart_options] && value[:chart_options][:colors]
        color_hash = color_hashes[value[:chart_options][:colors]]
        value[:chart_options][:colors] = send("#{value[:chart_type]}_coloring", value, color_hash)
      end
    rescue NoMethodError
      value[:chart_options][:colors] = fallback_coloring(value, color_hash)
    end
  end

  private

  def fallback_coloring(data_hash, color_hash)
    data_hash[:data].map { |x| color_hash[x[:name]] }
  end

  def bar_coloring(data_hash, color_hash)
    data_hash[:data].each { |x| x[:itemStyle] = { color: color_hash[x[:name]] } }
    data_hash[:data].pluck(:name)
  end

  def stacked_bar_coloring(data_hash, color_hash)
    series = data_hash[:data].first.second.first
    series[1..].map { |x| color_hash[x] }
  end
end
