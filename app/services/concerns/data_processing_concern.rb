# frozen_string_literal: true

# rubocop:disable Metrics/ModuleLength

module DataProcessingConcern
  extend ActiveSupport::Concern

  def process_chart_data(data_hash)
    data_hash.each_value do |value|
      value[:data] = send("#{value[:chart_type]}_format", value)
    rescue NoMethodError
      value[:data] = fallback_format(value)
    end
  end

  def fallback_format(data_hash)
    if data_hash.dig(:chart_options, :percentage)
      create_data_array_with_percentages(data_hash[:data])
    else
      data_hash[:data].map { |key, value| { name: key, value: value } }
    end
  end

  def pie_format(data_hash)
    create_data_array_with_percentages(data_hash[:data])
  end

  def map_format(data_hash)
    {
      max_value: calculate_max_value(data_hash[:data]).second,
      data: create_data_array_with_percentages(data_hash[:data])
    }
  end

  def stacked_bar_format(data_hash)
    data_hash[:data].transform_values { |value| convert_data_to_stacked_chart_format(value) }
  end

  def treemap_format(data_hash)
    send("treemap_#{data_hash[:treemap_type].downcase}_format", data_hash)
  rescue NoMethodError
    []
  end

  def treemap_icd_format(data_hash)
    chart_data = create_icd_treemap_scaffold
    populate_icd_data_treemap(chart_data, data_hash[:data])
    chart_data
  end

  def treemap_hosp_group_format(data_hash)
    total_sum = calculate_total_sum(data_hash[:data])
    chart_data = []
    convert_data_to_hash_tree(data_hash[:data], 3).each do |key, value|
      chart_data.push(convert_hash_tree_to_treemap(value, key, total_sum))
    end
    chart_data
  end

  private

  CAPITAL_LETTER_OFFSET = 65
  SERIES = ['Especialidade', I18n.t('general_data.distance.group_0'), I18n.t('general_data.distance.group_1'),
            I18n.t('general_data.distance.group_2'), I18n.t('general_data.distance.group_3')].freeze

  def create_data_array_with_percentages(data)
    total_sum = calculate_total_sum(data)
    data.map do |key, value|
      { name: key, value: value, percentage: format('%<percentage>.2f%%', percentage: 100.0 * value / total_sum) }
    end
  end

  def create_node(name)
    { name: name, children: [] }
  end

  def create_leaf(fullname, name, value, total_sum)
    {
      fullname: fullname,
      name: name,
      value: value,
      percentage: format('%<percentage>.2f%%', percentage: 100.0 * value / total_sum)
    }
  end

  def calculate_total_sum(data)
    data.inject(0) { |sum, element| element.first == '' ? sum : sum + element.second }
  end

  def calculate_max_value(data)
    data.max_by { |_key, value| value }
  end

  def create_icd_treemap_scaffold
    chart_data = []
    (0...26).each do |letter_index|
      chart_data.push(create_node(letter_index + CAPITAL_LETTER_OFFSET))
      (0...10).each do |number_index|
        chart_data[letter_index][:children].push(
          create_node((letter_index + CAPITAL_LETTER_OFFSET).chr + number_index.to_s)
        )
      end
    end
    chart_data
  end

  def populate_icd_data_treemap(treemap, data)
    total_sum = calculate_total_sum(data)
    data.each do |icd_fullname, value|
      next if icd_fullname == ''

      push_value_to_icd_treemap(
        treemap, icd_fullname[0].ord - CAPITAL_LETTER_OFFSET, icd_fullname[1].to_i,
        create_leaf(icd_fullname, icd_fullname[0...3], value, total_sum)
      )
    end
  end

  def push_value_to_icd_treemap(treemap, letter_index, number_index, value)
    return if letter_index >= treemap.length || letter_index.negative? ||
              number_index >= treemap[letter_index][:children].length || number_index.negative?

    treemap[letter_index][:children][number_index][:children].push(value)
  end

  def convert_data_to_hash_tree(data, split_size)
    root = {}
    data.each do |name, value|
      length = name.to_s.length
      next if length.zero? || !(length % split_size).zero?

      add_to_hash_tree(root, name.to_s, value, split_size, split_size)
    end
    root
  end

  def add_to_hash_tree(node, key, value, cur_size, split_size)
    if key.length <= cur_size
      node[key] = value
      return
    end
    cur_key = key[0..(cur_size - 1)]
    node[cur_key] = {} unless node[cur_key]
    add_to_hash_tree(node[cur_key], key, value, cur_size + split_size, split_size)
  end

  def convert_hash_tree_to_treemap(tree, node_name, total_sum)
    return create_leaf(node_name, node_name, tree, total_sum) if tree.is_a? Numeric

    node = create_node(node_name)
    tree.each { |key, value| node[:children].push(convert_hash_tree_to_treemap(value, key, total_sum)) }
    node
  end

  def convert_data_to_stacked_chart_format(data)
    formated_data = [SERIES, [], [SERIES], []]
    data.first.each_key { |key| add_stacked_chart_entry(formated_data, key, data) }
    formated_data
  end

  def add_stacked_chart_entry(formated_data, key, data)
    counts = data[0][key]
    percentages = data[1][key]
    formated_data[1].push([key].concat(counts))
    formated_data[2].push([key].concat(percentages))
    formated_data[3].push(counts.sum)
  end
end

# rubocop:enable Metrics/ModuleLength
