# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength

class GeneralDataService
  include ChartsConfigurationsConcern
  include DescriptiveStatisticsConcern
  include DistancesConcern
  include TranslationsConcern
  include DataProcessingConcern
  include ColoringConcern
  attr_accessor :health_facilities, :procedures_ids

  def initialize(health_facilities, procedures_ids)
    self.health_facilities = health_facilities
    self.procedures_ids = procedures_ids
  end

  def stats
    chart_stats = CHART_INFO.deep_dup.transform_values { |config| config.merge(data: data[config[:data]]) }
    @stats ||= color_charts(process_chart_data(chart_stats), color_hashes)
  end

  def data
    @data ||= translations_for(patient_data.merge(hospitalization_data, health_facilities_data, distances_data))
  end

  def patient_data
    {
      administrative_sectors: administrative_sector_count_ordered_by(count_all: :desc), age_year: '',
      age_codes: ages_ordered_by(count_all: :desc), ages_ordered_by_code: ages_ordered_by(:age_code),
      genders: order_and_count(PatientDatum, :gender, count_all: :desc), subprefectures: subprefecture_count,
      educational_levels: count(PatientDatum, :educational_level), races: count(PatientDatum, :race),
      administrative_sectors_by_name: administrative_sector_count_ordered_by(name: :desc),
      technical_health_supervisions: technical_health_supervision_count,
      regional_health_coordinations: regional_health_coordination_count
    }
  end

  def hospitalization_data
    {
      historic_series: historic_series, competences: competences,
      hospitalization_groups: order_and_count(HospitalizationDatum, :hospitalization_group, :hospitalization_group),
      specialties: count(HospitalizationDatum, :specialty),
      hospitalization_types: order_and_count(HospitalizationDatum, :hospitalization_type, count_all: :desc),
      complexities: count(HospitalizationDatum, :complexity), financing_types: count(HospitalizationDatum, :financing),
      hospitalization_days: order_and_count(HospitalizationDatum, :hospitalization_days, :hospitalization_days),
      service_values: order_and_count(HospitalizationDatum, :service_value, :service_value),
      distances: order_and_count(HospitalizationDatum, :distance, :distance)
    }.merge(hospitalization_rates_data, icd_categories_data)
  end

  def health_facilities_data
    {
      health_facilities: health_facilities_procedures,
      health_facilities_ranking: health_facilities_ranking,
      health_facilities_specialty_distances: health_facilities_specialty_distances,
      facility_types: health_facility_types_count,
      beds: health_facilities_beds,
      administrations: administration_count
    }
  end

  def descriptive_statistics
    descriptive_statistics_for(data)
  end

  def rankings
    data.select { |k, _v| RANKING_KEYS.include?(k) }
  end

  def dynamic
    stats.select { |k, _v| DYNAMIC_KEYS.include?(k) }
  end

  def color_hashes
    translations_for(data_type_items.transform_values { |items| create_color_hash(items) })
  end

  def data_type_items
    {
      genders: distinct_order_pluck(PatientDatum, :gender), races: distinct_order_pluck(PatientDatum, :race),
      educational_levels: distinct_order_pluck(PatientDatum, :educational_level),
      facility_types: distinct_order_pluck(HealthFacility, :facility_type),
      administrations: distinct_order_pluck(HealthFacility, :administration),
      specialties: distinct_order_pluck(HospitalizationDatum, :specialty),
      hospitalization_types: distinct_order_pluck(HospitalizationDatum, :hospitalization_type),
      complexities: distinct_order_pluck(HospitalizationDatum, :complexity),
      financing_types: distinct_order_pluck(HospitalizationDatum, :financing), distances_buckets: [0, 1, 2, 3]
    }
  end

  private

  def historic_series
    order_and_count(HospitalizationDatum,
                    Arel.sql("DATE_TRUNC('month', admission_date)"),
                    Arel.sql("DATE_TRUNC('month', admission_date)"))
  end

  def competences
    count(HospitalizationDatum, :competence).sort_by do |key, _|
      key = key.to_s
      key = "#{key[0...4]}0#{key[4]}" if key.length == 5
      key
    end.to_h
  end

  def administration_count
    join_filter_order_and_count(
      health_facilities, :hospitalization_data, :administration, :administration,
      hospitalization_data: { procedure_id: procedures_ids }
    )
  end

  def ages_ordered_by(column)
    order_and_count(PatientDatum, :age_code, column)
  end

  def administrative_sector_count_ordered_by(column)
    join_filter_order_and_count(PatientDatum, :administrative_sector, :name, column, procedure_id: procedures_ids)
  end

  def regional_health_coordination_count
    join_filter_order_and_count(PatientDatum, :regional_health_coordination, :name,
                                { name: :desc }, procedure_id: procedures_ids)
  end

  def technical_health_supervision_count
    join_filter_order_and_count(PatientDatum, :technical_health_supervision, :name,
                                { name: :desc }, procedure_id: procedures_ids)
  end

  def subprefecture_count
    join_filter_order_and_count(PatientDatum, :subprefecture, :name, :name, procedure_id: procedures_ids)
  end

  def hospitalization_rates_data
    {
      uti_rates: order_and_count(HospitalizationDatum, :uti_rates, :uti_rates),
      ui_rates: order_and_count(HospitalizationDatum, :ui_rates, :ui_rates),
      hospitalization_rates: order_and_count(HospitalizationDatum, :hospitalization_rates, :hospitalization_rates)
    }
  end

  def icd_categories_data
    {
      icd_categories: order_and_count(HospitalizationDatum, :icd_category_id, count_all: :desc),
      icd_secondary_categories: order_and_count(HospitalizationDatum, :secondary_icd1_id, count_all: :desc),
      icd_secondary_2_categories: order_and_count(HospitalizationDatum, :secondary_icd2_id, count_all: :desc)
    }
  end

  def count(data, group)
    data.where(procedure_id: procedures_ids).group(group).count
  end

  def order_and_count(data, group, order)
    data.where(procedure_id: procedures_ids).group(group).order(order).count
  end

  def health_facilities_ranking
    join_filter_order_and_count(
      health_facilities, :hospitalization_data, :name, { count_all: :desc },
      hospitalization_data: { procedure_id: procedures_ids }
    )
  end

  def health_facilities_procedures
    join_filter_order_and_count(
      health_facilities, :hospitalization_data, :name, { name: :desc },
      hospitalization_data: { procedure_id: procedures_ids }
    )
  end

  def health_facilities_specialty_distances
    health_facilities_specialty_distances_absolute.transform_values! do |series|
      new_series = []
      new_series.push(series)
      new_series.push(
        series.transform_values do |distances|
          sum = distances.sum
          distances.map { |x| 100.0 * x / sum }
        end
      )
    end
  end

  def health_facilities_specialty_distances_absolute
    health_facilities_distance_categories.each_with_object({}) do |((name, specialty, distance_group), count), hash|
      hash[name] ||= { total: [0, 0, 0, 0] }
      hash[name][specialty] ||= [0, 0, 0, 0]
      hash[name][:total][distance_group] += count
      hash[name][specialty][distance_group] = count
    end
  end

  def health_facilities_distance_categories
    join_filter_order_and_count(
      HospitalizationDatum, :health_facility,
      [
        :name, :specialty,
        'CASE WHEN distance <= 1 THEN 0'\
             'WHEN distance <= 5 THEN 1'\
             'WHEN distance <= 10 THEN 2 ELSE 3 END'
      ],
      :name, procedure_id: procedures_ids
    )
  end

  def health_facility_types_count
    join_filter_order_and_count(health_facilities, :hospitalization_data,
                                :facility_type,
                                'facility_type DESC',
                                'hospitalization_data.procedure_id' => procedures_ids)
  end

  def health_facilities_beds
    health_facilities.select(:name, :beds)
                     .joins(:hospitalization_data)
                     .where('hospitalization_data.procedure_id' => procedures_ids)
                     .order(name: :desc)
                     .distinct
                     .pluck(:name, :beds)
  end

  def join_filter_order_and_count(data, table, group, order, filter)
    data.joins(table).where(filter).group(group).order(order).count
  end

  def distinct_order_pluck(data, data_type)
    data.distinct.order(data_type).pluck(data_type)
  end

  def create_color_hash(categories)
    hash = {}
    categories.map do |x|
      hash[x] = COLORS[hash.size % COLORS.size]
    end
    hash
  end
end

# rubocop:enable Metrics/ClassLength
