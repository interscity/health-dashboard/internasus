# frozen_string_literal: true

class GeojsonProviderService
  class << self
    include Rails.application.routes.url_helpers
  end

  def self.geojson_for(klass)
    feature_collection = { 'type' => 'FeatureCollection', 'features' => [] }
    klass.camelize.constantize.find_each { |record| feature_collection['features'] << feature_hash(record) }
    feature_collection
  end

  def self.feature_hash(record)
    {
      'type' => 'Feature', 'geometry' => JSON.parse(record.shape),
      'properties' => {
        'name' => record.name,
        'dataUrl' => map_popup_on_area_procedures_path(area: record.class.to_s.underscore, id: record.id),
        'errorMessage' => I18n.t('error', scope: 'procedures.map.load')
      }
    }
  end
end
