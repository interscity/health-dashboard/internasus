# frozen_string_literal: true

class ProceduresMapAreaContentPopupService
  attr_reader :area_class, :id, :content

  def initialize(area_class, id)
    @area_class = area_class
    @id = id
    @content = {}
  end

  def build_popup_content
    content[:name] = area.name

    content[:patients_count] = if valid_division(area_class)
                                 PatientDatum.where(area_class => area).count
                               else
                                 I18n.t('missing_information', scope: 'procedures.map_popup_on_area')
                               end
    content
  end

  private

  def valid_division(klass)
    PatientDatum.column_names.include?("#{klass}_id") and PatientDatum.where.not("#{klass}_id".to_sym => nil).any?
  end

  def area
    @area ||= area_class.camelize.constantize.find(id)
  end
end
