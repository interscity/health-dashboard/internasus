# frozen_string_literal: true

class DemographicDivisionsProceduresCounterService
  GENDER_FILTERS = %w[women men].freeze
  RACE_FILTERS = %w[black mulatto native white yellow].freeze
  AGE_FILTERS = %w[00a04 05a09 10a14 15a19 20a24 25a29 30a34 35a39 40a44 45a49 50a54 55a59 60a69 70].freeze
  ALL_FILTERS = GENDER_FILTERS + RACE_FILTERS + AGE_FILTERS

  def initialize(procedures_ids, demographic_division = :census_sector)
    raise 'Unknown demographic division' unless DemographicDivision.distinct(:type).pluck(:type)
                                                                   .include? demographic_division.to_s.camelize

    self.demographic_division = demographic_division
    self.filtered_procedures = PatientDatum.where(procedure_id: procedures_ids)
    self.filters = []
  end

  # This transforms a list of separate type filters into a list of aggregate
  # filters, like: [men 10a14 native white] -> [men_10a14_native men_10a14_white],
  # which is the format the population field of census sector stores those
  # informations.
  def filters_from_params(params)
    selected_filters = [GENDER_FILTERS, AGE_FILTERS, RACE_FILTERS].map do |filter_set|
      filter_set.select { |filter| ActiveModel::Type::Boolean.new.cast(params[filter]) }
    end

    selected_filters.map { |group| group.empty? ? [''] : group }
                    .reduce(:product)
                    .map { |filter| filter.flatten.join('_').gsub('__', '_').gsub(/(^_|_$)/, '') }
  end

  def build_filters(params)
    self.filters = filters_from_params(params)
    self.filters = ['total'] if filters.include? ''
  end

  def procedures_by_division_id
    filtered_procedures.select("#{demographic_division}_id AS div_id", 'count(*) AS procs_count')
                       .group(ActiveRecord::Base.sanitize_sql("#{demographic_division}_id"))
  end

  def sum_of_population_columns
    filters.map { |filter| "pop.p_#{filter}" }
           .join(' + ')
  end

  def demographic_divisions_rates
    hospitalization_rates = Arel.sql(
      ActiveRecord::Base.sanitize_sql("procs_count::decimal / (#{sum_of_population_columns})")
    )

    PatientDatum.from(procedures_by_division_id, :procs_by_div_id)
                .joins('INNER JOIN demographic_divisions AS div ON procs_by_div_id.div_id = div.id',
                       'INNER JOIN populations AS pop ON div.id = pop.demographic_division_id')
                .where(ActiveRecord::Base.sanitize_sql("(#{sum_of_population_columns}) <> 0"))
                .pluck(:latitude, :longitude, hospitalization_rates)
  end

  def census_sector_counts_by(attribute)
    filtered_procedures
      .group(:census_sector_id, attribute)
      .count
      .group_by { |cs_id_and_attribute, _count| cs_id_and_attribute.first }
      .transform_values do |array_and_count_pair|
      array_and_count_pair.map { |array| [array.first.last, array.last] }.to_h
    end
  end

  def census_sector_statistics
    statistics = census_sector_counts_by :race
    by_gender = census_sector_counts_by :gender
    ids = (statistics.keys + by_gender.keys).uniq

    statistics.each { |k, v| v.merge!(by_gender[k]) }
    statistics.merge({
                       total: filtered_procedures.group(demographic_division).count,
                       ids: ids
                     })
  end

  private

  attr_accessor :filters, :filtered_procedures, :demographic_division
end
