# frozen_string_literal: true

class DemographicDivisionsController < ApplicationController
  def geojson
    render json: Rails.cache.fetch("#{params[:area]}_geojson") { GeojsonProviderService.geojson_for(params[:area]) }
  end
end
