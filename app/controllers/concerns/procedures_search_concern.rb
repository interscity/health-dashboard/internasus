# frozen_string_literal: true

module ProceduresSearchConcern
  extend ActiveSupport::Concern

  def set_search_results
    q_params = query_params
    @active_filters = ProceduresSearchService.active_filters(q_params)
    @heatmap_weight_model = params[:heatmap_weight_model] || :census_sector
    @search,
      @health_facilities,
      @procedures_ids,
      @patients_data,
      @patients_latlong_array = ProceduresSearchService.search(q_params)
  end

  private

  def query_params
    params[:q] = session[:last_q_params] if params[:q].nil?
    params[:q]
  end
end
