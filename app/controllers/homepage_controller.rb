# frozen_string_literal: true

class HomepageController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @carousel_images = carousel_images
    @hospitalization_counter = HospitalizationDatum.count
    @health_facility_counter = HealthFacility.count
    @specialty_counter = HospitalizationDatum.select(:specialty).distinct.count
    @mean_distance = HospitalizationDatum.average(:distance)
    @dataset_years = HospitalizationDatum.dataset_years
  end

  private

  def carousel_images
    subdir = @public_instance ? 'public-rj' : 'sms-sp'
    paths = Dir.glob(Rails.root.join("app/assets/images/homepage-slides/#{subdir}/*"))
    paths.map! { |p| p.partition('images/').last }
    paths.sort
  end
end
