# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :set_privacy
  before_action :authenticate_user!, unless: :skip_auth?
  around_action :switch_locale

  def switch_locale(&action)
    session[:locale] = I18n.default_locale if session[:locale].nil?
    session[:locale] = params[:locale] unless params[:locale].nil?
    I18n.with_locale(session[:locale], &action)
  end

  def default_url_options
    { locale: params[:locale] }
  end

  def set_privacy
    @public_instance = Rails.application.config.public
  end

  private

  def skip_auth?
    @public_instance
  end
end
