# frozen_string_literal: true

class ProceduresController < ApplicationController
  include RansackMemory::Concern
  include ProceduresSearchConcern

  before_action :save_and_load_filters, only: %i[search export print]
  before_action :set_search_results, only: %i[index search export print]

  def index
    @sectors_counts = {}
  end

  def search
    sectors_procedures_counter = DemographicDivisionsProceduresCounterService.new(@procedures_ids,
                                                                                  @heatmap_weight_model)
    sectors_procedures_counter.build_filters(csw_heat_map_params.to_h)
    @divisions_rates = sectors_procedures_counter.demographic_divisions_rates
  end

  def show
    @patient_datum = PatientDatum
                     .includes(
                       :administrative_sector, :subprefecture, :technical_health_supervision,
                       :regional_health_coordination, :census_sector
                     )
                     .find_by!(procedure_id: params[:id])

    @hospitalization_datum = HospitalizationDatum
                             .includes(:icd_category)
                             .find_by!(procedure_id: params[:id])

    render layout: false
  end

  def export
    @patients_data = PatientDatum.includes(
      { hospitalization_datum: %i[health_facility icd_subcategory secondary_icd1 secondary_icd2] },
      :administrative_sector,
      :subprefecture,
      :technical_health_supervision,
      :regional_health_coordination
    ).where(procedure_id: @procedures_ids)
  end

  def print
    @base64_map = print_params[:map_image]
    @heatmap_type, @heatmap_max = print_params[:heatmap_max]&.split(':')
    @heatmap_gradient = print_params[:heatmap_gradient]

    render pdf: 'procedures', orientation: 'Landscape', page_size: 'A4', layout: 'wicked_pdf'
  end

  def map_popup_on_area
    @content = ProceduresMapAreaContentPopupService.new(params[:area], params[:id]).build_popup_content

    render layout: false
  end

  private

  def csw_heat_map_params
    params.fetch(:csw_heat_map).permit(DemographicDivisionsProceduresCounterService::ALL_FILTERS)
  end

  def print_params
    params.permit(:map_image, :heatmap_max, :heatmap_gradient)
  end
end
