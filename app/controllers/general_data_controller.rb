# frozen_string_literal: true

class GeneralDataController < ApplicationController
  include RansackMemory::Concern
  include ProceduresSearchConcern

  before_action :save_and_load_filters, only: %i[index]
  before_action :set_search_results, only: %i[index]

  def index
    @total_records = HospitalizationDatum.count
    general_data
    # census_sectors
  end

  def print
    @image = print_params[:image]
    @kind = print_params[:kind]

    render pdf: "general_data_#{@kind}", page_size: 'A4', layout: 'wicked_pdf'
  end

  private

  def general_data
    general_data_service = GeneralDataService.new(@health_facilities, @procedures_ids)
    @stats = general_data_service.stats
    @data = general_data_service.data
    @rankings = general_data_service.rankings
    @dynamic = general_data_service.dynamic
    @descriptive_statistics = general_data_service.descriptive_statistics
  end

  def census_sectors
    demographic_divisions_procedures_counter_service = DemographicDivisionsProceduresCounterService.new(@procedures_ids)
    @census_sector_counts = demographic_divisions_procedures_counter_service.census_sector_statistics
    @census_sectors = CensusSector.where(id: @census_sector_counts[:ids])
  end

  def print_params
    params.permit(:kind, :image)
  end
end
