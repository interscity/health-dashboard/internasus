#!/bin/sh
set -e

bundle exec rails db:prepare
bundle exec rails assets:precompile

cp -R public/* nginx_public/

bundle exec $@
