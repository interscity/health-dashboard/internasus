FROM ruby:2.7

RUN gem install bundler --no-document
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -y && apt-get install -y yarn
RUN apt-get update -y && apt-get install -y build-essential cmake wkhtmltopdf

WORKDIR /internasus
ADD . /internasus/

RUN gem install bundler --conservative --no-doc
RUN bundle install
RUN yarn

EXPOSE 3000
ENTRYPOINT ["./bin/entrypoint.sh"]
