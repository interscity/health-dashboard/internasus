# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/authenticated_controller'

RSpec.shared_examples 'successful search action' do |method, action|
  before do
    expect(subject).to receive(:set_search_results)

    send(method, action)
  end

  it { is_expected.to respond_with(:success) }

  it { is_expected.to use_before_action(:set_search_results) }

  it { is_expected.to use_before_action(:save_and_load_filters) }
end

RSpec.describe ProceduresController, type: :controller do
  describe 'action' do
    include_examples 'authenticated controller'

    describe 'GET #index' do
      it_behaves_like 'successful search action', :get, :index
    end

    describe 'POST #search' do
      let(:demographic_divisions_procedures_counter_service) do
        double('demographic_divisions_procedures_counter_service')
      end
      let(:params) do
        { 'csw_heat_map' => {
          'women' => 'true',
          'men' => 'true',
          'black' => 'false',
          'mulatto' => 'false',
          'native' => 'false',
          'white' => 'false',
          'yellow' => 'false'
        } }
      end
      let(:procedures_ids) { double('procedures_ids') }
      let(:heatmap_weight_model) { :census_sector }

      before do
        allow(DemographicDivisionsProceduresCounterService).to receive(:new)
          .and_return(demographic_divisions_procedures_counter_service)
        allow(demographic_divisions_procedures_counter_service).to receive(:demographic_divisions_rates)
        allow(demographic_divisions_procedures_counter_service).to receive(:build_filters)

        subject.instance_variable_set(:@procedures_ids, procedures_ids)
        subject.instance_variable_set(:@heatmap_weight_model, heatmap_weight_model)

        expect(subject).to receive(:set_search_results)

        post :search, params: params, xhr: true
      end

      it { is_expected.to respond_with(:success) }

      it { is_expected.to use_before_action(:set_search_results) }

      it { is_expected.to use_before_action(:save_and_load_filters) }

      it 'is expected to count the procedures for each census sector, its total population and rate' do
        expect(DemographicDivisionsProceduresCounterService).to have_received(:new)
          .with(procedures_ids, heatmap_weight_model)
        expect(demographic_divisions_procedures_counter_service).to have_received(:demographic_divisions_rates)
      end

      it 'is expected to set filters for DemographicDivisionsProceduresCounterService' do
        expect(demographic_divisions_procedures_counter_service).to have_received(:build_filters)
          .with(params['csw_heat_map'])
      end
    end

    describe 'GET #show' do
      let(:id) { 1234 }

      before do
        allow(PatientDatum).to receive_message_chain(:includes, :find_by!)
        allow(HospitalizationDatum).to receive_message_chain(:includes, :find_by!)

        get :show, params: { id: id }
      end

      it { is_expected.to respond_with(:success) }

      it 'is expected to find the PatientDatum' do
        expect(PatientDatum).to have_received(:includes).with(
          :administrative_sector, :subprefecture, :technical_health_supervision,
          :regional_health_coordination, :census_sector
        )
        expect(PatientDatum.includes).to have_received(:find_by!).with(procedure_id: id.to_s)
      end

      it 'is expected to find the HospitalizationDatum' do
        expect(HospitalizationDatum).to have_received(:includes).with(:icd_category)
        expect(HospitalizationDatum.includes).to have_received(:find_by!).with(procedure_id: id.to_s)
      end
    end

    describe 'GET #export' do
      let(:procedures_ids) { double('procedures_ids') }

      before do
        subject.instance_variable_set(:@procedures_ids, procedures_ids)

        allow(PatientDatum).to receive_message_chain(:includes, :where)
        expect(subject).to receive(:set_search_results)

        get :export, format: :csv
      end

      it { is_expected.to respond_with(:success) }

      it { is_expected.to use_before_action(:save_and_load_filters) }
      it { is_expected.to use_before_action(:set_search_results) }

      it 'is expected to query for patients data eager loading associations' do
        expect(PatientDatum).to have_received(:includes).with(
          { hospitalization_datum: %i[health_facility icd_subcategory secondary_icd1 secondary_icd2] },
          :administrative_sector,
          :subprefecture,
          :technical_health_supervision,
          :regional_health_coordination
        )
        expect(PatientDatum.includes).to have_received(:where).with(procedure_id: procedures_ids)
      end
    end

    describe 'POST #print' do
      before do
        expect(subject).to receive(:print_params).and_return({}).exactly(3).times
      end

      it_behaves_like 'successful search action', :post, :print
    end

    describe 'GET #map_popup_on_area' do
      let(:service) { double(ProceduresMapAreaContentPopupService) }
      let(:area) { 'subprefecture' }
      let(:id) { '1' }

      before do
        allow(ProceduresMapAreaContentPopupService).to receive(:new).and_return service
        allow(service).to receive(:build_popup_content)

        get :map_popup_on_area, params: { area: area, id: id }
      end

      it { is_expected.to respond_with(:success) }

      it 'is expected to get a new instance of the ProceduresMapAreaContentPopupService' do
        expect(ProceduresMapAreaContentPopupService).to have_received(:new).with(area, id)
      end

      it 'is expected to build popup content' do
        expect(service).to have_received(:build_popup_content)
      end
    end
  end

  describe 'private method' do
    describe 'print_params' do
      before do
        allow(subject).to receive_message_chain(:params, :permit)
        subject.send(:print_params)
      end

      it 'is expected to permit only some fields' do
        expect(subject.params).to have_received(:permit)
          .with(:map_image, :heatmap_max, :heatmap_gradient)
      end
    end
  end
end
