# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'render static page' do |page|
  describe "GET ##{page}" do
    before do
      get page
    end

    it { is_expected.to respond_with(200) }
  end
end

RSpec.describe StaticPagesController, type: :controller do
  it_behaves_like 'render static page', :about
  it_behaves_like 'render static page', :guide
end
