# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/authenticated_controller'

RSpec.describe GeneralDataController, type: :controller do
  describe 'action' do
    include_examples 'authenticated controller'

    describe 'GET #index' do
      let(:general_data_service) { double 'general_data_service' }
      let(:sectors_procedures_counter) { double 'census_sectors_procedures_couter' }

      before do
        expect(subject).to receive(:save_and_load_filters)
        expect(subject).to receive(:set_search_results)

        allow(HospitalizationDatum).to receive(:count)
        expect(GeneralDataService).to receive(:new).and_return(general_data_service)
        allow(general_data_service).to receive(:stats)
        allow(general_data_service).to receive(:data)
        allow(general_data_service).to receive(:rankings)
        allow(general_data_service).to receive(:dynamic)
        allow(general_data_service).to receive(:descriptive_statistics)
        allow(sectors_procedures_counter).to receive(:census_sector_statistics).and_return(ids: 1)
        allow(CensusSector).to receive(:where)

        get :index
      end

      it { is_expected.to respond_with(:success) }

      it { is_expected.to use_before_action(:set_search_results) }

      it { is_expected.to use_before_action(:save_and_load_filters) }

      it 'is expected to count HospitalizationDatum' do
        expect(HospitalizationDatum).to have_received(:count)
      end

      it 'is expected to get all stats from the service' do
        expect(general_data_service).to have_received(:stats)
      end

      it 'is expected to get all data from the service' do
        expect(general_data_service).to have_received(:data)
      end

      it 'is expected to get the general data rankings' do
        expect(general_data_service).to have_received(:rankings)
      end

      it 'is expected to get the general data for the dynamic charts' do
        expect(general_data_service).to have_received(:dynamic)
      end

      it 'is expected to get the general data descriptive_statistics' do
        expect(general_data_service).to have_received(:descriptive_statistics)
      end
    end

    describe 'POST #print' do
      let(:kind) { 'kind' }

      before do
        allow(subject).to receive(:save_and_load_filters)
        allow(subject).to receive(:set_search_results)

        allow(subject).to receive(:render)

        post :print, params: { image: 'image_data', kind: kind }
      end

      it 'is expected not to use the before actions' do
        expect(subject).not_to have_received(:save_and_load_filters)
        expect(subject).not_to have_received(:set_search_results)
      end

      it 'is expected to render a pdf file' do
        expect(subject).to have_received(:render).with(
          pdf: "general_data_#{kind}",
          page_size: 'A4',
          layout: 'wicked_pdf'
        )
      end
    end
  end

  describe 'private methods' do
    describe 'print_params' do
      before do
        allow(subject).to receive_message_chain(:params, :permit)

        subject.send(:print_params)
      end

      it 'is expected to permit a kind and an image field' do
        expect(subject.params).to have_received(:permit).with(:kind, :image)
      end
    end
  end
end
