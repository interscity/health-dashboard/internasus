# frozen_string_literal: true

require 'rails_helper'

class ApplicationControllerTest < ApplicationController
  def index; end
end

RSpec.describe ApplicationControllerTest, type: :controller do
  describe 'before_action' do
    before do
      subject.index
    end

    it { is_expected.to use_before_action(:authenticate_user!) }
  end

  describe 'around_action' do
    before do
      subject.index
    end

    it { is_expected.to use_around_action(:switch_locale) }
  end

  describe 'switch_locale' do
    let(:default) { 'pt-BR' }
    let(:locale) { 'en' }

    context 'if locale parameter is nil' do
      before do
        allow(subject).to receive(:params).and_return({ locale: nil })
        allow(I18n).to receive(:default_locale).and_return(default)
        allow(I18n).to receive(:with_locale).with(default)
        subject.send(:switch_locale)
      end

      it 'is expected to get the default locale' do
        expect(subject).to have_received(:params)
        expect(I18n).to have_received(:default_locale)
        expect(I18n).to have_received(:with_locale).with(default)
      end
    end

    context 'if locale parameter is defined' do
      before do
        allow(subject).to receive(:params).and_return({ locale: locale })
        allow(I18n).to receive(:with_locale).with(locale)
        subject.send(:switch_locale)
      end

      it 'is expected to get the defined locale' do
        expect(subject).to have_received(:params).exactly(2).times
        expect(I18n).to have_received(:with_locale).with(locale)
      end
    end
  end

  describe 'default_url_options' do
    let(:params) { { locale: 'pt-BR' } }

    before do
      allow(subject).to receive(:params).and_return(params)
    end

    it 'is expected to return a hash with the locale parameter' do
      response = subject.send(:default_url_options)
      expect(subject).to have_received(:params)
      expect(response).to eq(params)
    end
  end
end
