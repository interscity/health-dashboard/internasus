# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomepageController, type: :controller do
  describe 'GET #index' do
    let(:returned_paths) do
      %w[app/assets/images/homepage-slides/sms-sp/slide3.jpg app/assets/images/homepage-slides/sms-sp/slide4.jpg
         app/assets/images/homepage-slides/sms-sp/slide1.jpg app/assets/images/homepage-slides/sms-sp/slide2.jpg]
    end
    let(:image_paths) do
      %w[homepage-slides/sms-sp/slide1.jpg homepage-slides/sms-sp/slide2.jpg
         homepage-slides/sms-sp/slide3.jpg homepage-slides/sms-sp/slide4.jpg]
    end

    before do
      allow(HospitalizationDatum).to receive(:count)
      allow(HealthFacility).to receive(:count)
      allow(HospitalizationDatum).to receive_message_chain(:select, :distinct, :count)
      allow(HospitalizationDatum).to receive(:average)
      allow(HospitalizationDatum).to receive(:dataset_years)
      allow(Dir).to receive(:glob).and_return(returned_paths)

      get :index
    end

    it { is_expected.to respond_with(:success) }

    it { is_expected.to_not use_before_action(:authenticate_user!) }

    it 'is expected to count hospitalizations' do
      expect(HospitalizationDatum).to have_received(:count)
    end

    it 'is expected to count health facilities' do
      expect(HealthFacility).to have_received(:count)
    end

    it 'is expected to count specialties' do
      expect(HospitalizationDatum).to have_received(:select).with(:specialty)
      expect(HospitalizationDatum.select).to have_received(:distinct)
      expect(HospitalizationDatum.select.distinct).to have_received(:count)
    end

    it 'is expected to compute the distance average' do
      expect(HospitalizationDatum).to have_received(:average).with(:distance)
    end

    it 'is expected to get the dataset years' do
      expect(HospitalizationDatum).to have_received(:dataset_years).with(no_args)
    end

    it 'is expected to get the carousel images' do
      expect(assigns(:carousel_images)).to eq(image_paths)
    end
  end
end
