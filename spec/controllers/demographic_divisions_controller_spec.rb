# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/authenticated_controller'

RSpec.describe DemographicDivisionsController, type: :controller do
  describe 'action' do
    include_examples 'authenticated controller'

    describe 'GET #geojson' do
      let(:area) { 'subprefecture' }
      let(:geojson) { double('GeoJSON hash') }

      before do
        allow(Rails).to receive_message_chain(:cache, :fetch).and_yield
        allow(GeojsonProviderService).to receive(:geojson_for).and_return geojson
        allow(subject).to receive(:render)

        get :geojson, params: { area: area }, format: :json
      end

      it 'is expected to create a cache for the geojson data' do
        expect(Rails).to have_received(:cache)
        expect(Rails.cache).to have_received(:fetch).with('subprefecture_geojson')
      end

      it 'is expected to build the geojson for the area' do
        expect(GeojsonProviderService).to have_received(:geojson_for).with(area)
      end

      it 'is expected to render a JSON text' do
        expect(subject).to have_received(:render).with(json: geojson)
      end
    end
  end
end
