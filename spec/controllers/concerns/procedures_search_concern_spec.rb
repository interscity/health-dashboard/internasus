# frozen_string_literal: true

require 'rails_helper'

class ProceduresSearchConcernTest
  include ProceduresSearchConcern

  attr_accessor :params, :session
end

RSpec.describe ProceduresSearchConcern do
  subject { ProceduresSearchConcernTest.new }

  describe 'set_search_results' do
    let(:health_facilities) { double('health_facilities') }
    let(:patients_data) { double('patients_data') }
    let(:patients_latlong_array) { double('patients_latlong_array') }
    let(:search) { double('search') }
    let(:procedures_ids) { double('procedures_ids') }
    let(:heatmap_weight_model) { nil }
    let(:params) { {} }

    before do
      subject.params = { heatmap_weight_model: heatmap_weight_model }
      allow(subject).to receive(:query_params).and_return(params)

      allow(ProceduresSearchService).to receive(:search)
        .and_return([search, health_facilities, patients_data, procedures_ids,
                     patients_latlong_array])

      allow(ProceduresSearchService).to receive(:active_filters)

      subject.set_search_results
    end

    context 'when passing a demographic division to weight the heatmap' do
      let(:heatmap_weight_model) { :administrative_sector }

      it 'is expected to return the passed model' do
        expect(subject.instance_variable_get(:@heatmap_weight_model)).to eq(heatmap_weight_model)
      end
    end

    it 'is expected to default the heatmap weighting model to census_sector' do
      expect(subject.instance_variable_get(:@heatmap_weight_model)).to eq(:census_sector)
    end

    it 'is expected to invoke the ProceduresSearchService' do
      expect(ProceduresSearchService).to have_received(:search)
    end

    it 'is expected to get the active filters from ProceduresSearchService' do
      expect(ProceduresSearchService).to have_received(:active_filters)
    end
  end

  describe 'query_params' do
    let(:last_q_params) { double 'last_q_params' }
    let(:q_params) { double 'q_params' }

    before do
      subject.session = { last_q_params: last_q_params }
      subject.params = {}
    end

    context 'normal query' do
      before do
        subject.params = { q: q_params }
      end

      it 'is expected to return the query params' do
        response = subject.send(:query_params)
        expect(response).to eq(q_params)
      end
    end

    context 'empty query' do
      it 'is expected to return the query params from the last session search' do
        response = subject.send(:query_params)
        expect(response).to eq(last_q_params)
      end
    end
  end
end
