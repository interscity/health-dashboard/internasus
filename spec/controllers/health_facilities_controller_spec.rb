# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/authenticated_controller'

RSpec.describe HealthFacilitiesController, type: :controller do
  describe 'action' do
    include_examples 'authenticated controller'

    describe 'GET #show' do
      let(:id) { '1081' }

      before do
        allow(HealthFacility).to receive_message_chain(:find)

        get :show, params: { id: id }
      end

      it { is_expected.to respond_with(:success) }

      it 'is expected to find the HealthFacility' do
        expect(HealthFacility).to have_received(:find).with(id)
      end
    end
  end
end
