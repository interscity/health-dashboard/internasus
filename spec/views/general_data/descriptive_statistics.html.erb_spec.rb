# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_descriptive_statistics.html.erb', type: :view do
  let(:hash_1) { { 'a' => 42, 'b' => 314 } }
  let(:hash_2) { { 'a' => 314, 'b' => 42 } }
  let(:descriptive_statistics) do
    {
      'hash_1' => hash_1,
      'hash_2' => hash_2
    }
  end

  before do
    allow(view).to receive(:render).and_call_original
    descriptive_statistics.each do |hash|
      expect(view).to receive(:render).with(partial: 'general_data/descriptive_statistics_item',
                                            collection: hash.second)
    end
    expect(view).to receive(:descriptive_statistics_translations).and_return(descriptive_statistics)
    render
  end

  it 'is expected to have a table' do
    expect(rendered).to have_selector('table',
                                      class: 'table table-striped table-inverse')
  end

  it 'is expected to have headers for the table' do
    %w[variable count sum minimum maximum mean standard_deviation].each do |v|
      expect(rendered).to have_selector('th', text: I18n.t("general_data.descriptive_statistics.#{v}"))
    end
  end

  it 'is expected to find correct label on table rows' do
    expect(rendered).to match(/hash_1/)
    expect(rendered).to match(/hash_2/)
  end
end
