# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_ranking_table.html.erb', type: :view do
  let(:ranking_data) { { 'a' => 1, 'b' => 2 } }
  let(:ranking_table) { ['Estabelecimento', ranking_data] }
  let(:ranking_table_counter) { 2 }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: 'general_data/ranking_item',
                                         collection: ranking_table.second)

    render partial: 'general_data/ranking_table', locals: {
      ranking_table: ranking_table,
      ranking_table_counter: ranking_table_counter
    }
  end

  it 'is expected to have a table' do
    expect(rendered).to have_selector('div',
                                      id: "ranking-table-#{ranking_table_counter}",
                                      class: 'table-responsive')
  end

  it 'is expected to have headers for the table' do
    expect(rendered).to have_selector('th', text: '#')
    expect(rendered).to have_selector('th', text: ranking_table.first)
    expect(rendered).to have_selector('th', text: I18n.t('general_data.rankings.procedures_number'))
  end

  it 'is expected to render the partial for the table data' do
    expect(view).to have_received(:render).with(partial: 'general_data/ranking_item',
                                                collection: ranking_table.second)
  end
end
