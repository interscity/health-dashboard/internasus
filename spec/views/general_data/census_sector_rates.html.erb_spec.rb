# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_census_sector_rates.html.erb', type: :view do
  let(:census_sectors) { double('census_sectors') }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: 'general_data/census_sector_row', collection: census_sectors)

    assign(:census_sectors, census_sectors)

    render
  end

  it 'is expected to have a table' do
    expect(rendered).to have_selector('div', class: 'table-responsive')
  end

  it 'is expected to render the partial for the table rows' do
    expect(view).to have_received(:render).with(partial: 'general_data/census_sector_row', collection: census_sectors)
  end
end
