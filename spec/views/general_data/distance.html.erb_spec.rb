# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_distance.html.erb', type: :view do
  let(:charts_ids) { %i[distances_buckets specialties_means] }
  let(:parent_id) { '#collapseGeneralDatadistance' }

  let(:data) do
    { facility_type_means_per_specialty: { one: {}, two: {} } }
  end
  let(:stats) do
    {
      distances_buckets: {},
      specialties_means: {}
    }
  end

  before do
    allow(view).to receive(:render).and_call_original
    expect(view).to receive(:render).with(partial: 'general_data/charts',
                                          locals: { charts_ids: charts_ids, parent_id: parent_id })
    expect(view).to receive(:render).with(partial: 'general_data/distance_chart',
                                          collection: data[:facility_type_means_per_specialty], as: :chart)

    assign(:stats, stats)
    assign(:data, data)

    render
  end

  it 'is expected to have the selector for each id of the list' do
    %w[distances-buckets-chart specialties-means-chart].each do |id|
      expect(rendered).to have_selector('div', id: id)
    end
  end
end
