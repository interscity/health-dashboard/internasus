# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_rankings.html.erb', type: :view do
  let(:rankings) do
    {
      health_facilities: {},
      administrative_sectors: {},
      age_codes: {},
      genders: {},
      icd_categories: {}
    }
  end

  before do
    expect(view).to receive(:rankings_translations).and_return(rankings)

    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: 'general_data/ranking_table.html', collection: rankings)

    assign(:rankings, rankings)

    render
  end

  it 'is expected to have a select for the ranking category' do
    expect(rendered).to have_selector('select', id: 'ranking_category')
  end

  it 'is expected to render the partial for the tables' do
    expect(view).to have_received(:render).with(partial: 'general_data/ranking_table.html', collection: rankings)
  end
end
