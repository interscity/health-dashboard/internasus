# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

RSpec.describe 'general_data/_health_facilities_specialty_distances.html.erb', type: :view do
  context 'when there are health facilities selected' do
    before do
      allow(view).to receive(:render).and_call_original

      assign(:stats, { health_facilities_specialty_distances: { data: { health_facility: {} }, options: {} } })

      render
    end

    it 'is expected to have the selector for each id of the list' do
      expect(rendered).to have_selector('select', id: 'health_facility_specialty_distances_select')
    end
  end

  context 'when there are no health facilities selected' do
    before do
      allow(view).to receive(:render).and_call_original

      assign(:stats, { health_facilities_specialty_distances: { data: {}, options: {} } })

      render
    end

    it 'is expected to contain no health facilities warning' do
      expect(rendered).to have_content(
        I18n.t('general_data.health_facilities_specialty_distances.no_health_facilities')
      )
    end
  end
end
