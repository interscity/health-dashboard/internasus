# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_inside_element'

dynamic_charts_options = { health_facilities: {}, age_codes: {}, genders: {} }
RSpec.describe 'general_data/_dynamic.html.erb', type: :view do
  before do
    expect(view).to receive(:dynamic_section_titles).and_return(dynamic_charts_options)
  end

  it_behaves_like 'chart inside element', dynamic_charts_options, 'select', ['dynamic_chart_options']
end
