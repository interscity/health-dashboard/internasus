# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_section'

RSpec.describe 'general_data/_hospitalizations.html.erb', type: :view do
  it_behaves_like 'chart section',
                  {
                    genders: {},
                    ages_ordered_by_code: {},
                    hospitalization_days: {},
                    hospitalization_types: {}
                  },
                  %i[genders ages_ordered_by_code hospitalization_days hospitalization_types],
                  '#collapseGeneralDatahospitalizations'
end
