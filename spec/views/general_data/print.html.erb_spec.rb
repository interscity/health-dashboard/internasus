# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/print.html.erb', type: :view do
  let(:section_as_image) { 'base64_encoded_image' }
  let(:kind) { 'rankings' }

  before do
    assign(:image, section_as_image)
    assign(:kind, kind)

    render
  end

  it 'is expected to render the map as an image' do
    expect(rendered).to match(section_as_image)
  end
end
