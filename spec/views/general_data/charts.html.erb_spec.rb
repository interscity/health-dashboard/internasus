# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_charts.html.erb', type: :view do
  let(:parent_id) { 'parent_id' }
  let(:charts_ids) { %i[id_one id_two] }
  let(:divs_ids) { %w[id-one-chart id-two-chart] }

  let(:stats) do
    {
      id_one: { chart_type: 'map', chart_title: 'title', data: [], chart_area: 'area' },
      id_two: { chart_type: 'non-map', chart_title: 'title', data: [], chart_options: {} }
    }
  end

  before do
    assign(:stats, stats)
    render partial: 'general_data/charts', locals: { charts_ids: charts_ids, parent_id: parent_id }
  end

  it 'is expected to render all divs from chart_ids' do
    divs_ids.each { |id| expect(rendered).to have_selector('div', id: id) }
  end
end
