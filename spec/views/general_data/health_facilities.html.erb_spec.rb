# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_section'

RSpec.describe 'general_data/_health_facilities.html.erb', type: :view do
  it_behaves_like 'chart section',
                  {
                    administrations_pie: {},
                    facility_types_pie: {},
                    administrations_bar: {},
                    facility_types_bar: {},
                    beds: {}
                  },
                  %i[administrations_pie facility_types_pie administrations_bar facility_types_bar beds],
                  '#collapseGeneralDatahealth_facilities'
end
