# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_section'

RSpec.describe 'general_data/_specialties.html.erb', type: :view do
  it_behaves_like 'chart section',
                  {
                    specialties_bar: {},
                    specialties_pie: {}
                  },
                  %i[specialties_bar specialties_pie],
                  '#collapseGeneralDataspecialties'
end
