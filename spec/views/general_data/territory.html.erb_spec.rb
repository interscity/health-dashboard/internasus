# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_section'

RSpec.describe 'general_data/_territory.html.erb', type: :view do
  it_behaves_like 'chart section',
                  {
                    administrative_sectors_by_name: {},
                    regional_health_coordinations: {},
                    technical_health_supervisions: {},
                    administrative_sectors_by_name_map: { chart_type: 'map' },
                    regional_health_coordinations_map: { chart_type: 'map' },
                    technical_health_supervisions_map: { chart_type: 'map' },
                    subprefectures_map: { chart_type: 'map' }
                  },
                  %i[administrative_sectors_by_name regional_health_coordinations technical_health_supervisions
                     administrative_sectors_by_name_map regional_health_coordinations_map
                     technical_health_supervisions_map subprefectures_map],
                  '#collapseGeneralDataterritory'
end
