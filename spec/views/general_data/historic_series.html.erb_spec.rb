# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/chart_section'

RSpec.describe 'general_data/_historic_series.html.erb', type: :view do
  it_behaves_like 'chart section', { historic_series: {} }, %i[historic_series], '#collapseGeneralDatahistoric_series'
end
