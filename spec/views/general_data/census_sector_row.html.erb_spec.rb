# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'general_data/_census_sector_row.html.erb', type: :view do
  let(:census_sector_row) { FactoryBot.build(:census_sector, id: 1) }
  let(:census_sector_counts) do
    {
      1 => { '1' => 2, '99' => 3, 'F' => 4 }
    }
  end

  before do
    census_sector_row.population = FactoryBot.build(:population, demographic_division_id: census_sector_row.id)
    assign(:census_sector_counts, census_sector_counts)

    render partial: 'general_data/census_sector_row', locals: {
      census_sector_row: census_sector_row
    }
  end

  it 'is expected to show the code column as index' do
    expect(rendered).to have_selector('th', text: census_sector_row.code)
  end

  it 'is expected to show the race columns' do
    expect(rendered).to have_selector('td', text: census_sector_row.population['white'])
    expect(rendered).to have_selector('td', text: census_sector_row.population['black'])
    expect(rendered).to have_selector('td', text: census_sector_row.population['yellow'])
    expect(rendered).to have_selector('td', text: census_sector_row.population['mulatto'])
    expect(rendered).to have_selector('td', text: census_sector_row.population['native'])
  end

  it 'is expected to show the gender columns' do
    expect(rendered).to have_selector('td', text: census_sector_row.population['men'])
    expect(rendered).to have_selector('td', text: census_sector_row.population['women'])
  end

  it 'is expected to show the total column' do
    expect(rendered).to have_selector('td', text: census_sector_row.population['total'])
  end
end
