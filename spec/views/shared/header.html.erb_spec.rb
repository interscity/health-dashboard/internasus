# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/display_element'

RSpec.describe 'shared/_header.html.erb', type: :view do
  languages = { 'pt-BR': 'Português', en: 'English' }

  before do
    allow(view).to receive(:link_to).and_call_original
    I18n.available_locales.each do |locale|
      classes = 'dropdown-item'
      classes += ' disabled' if I18n.locale == locale
      allow(view).to receive(:link_to)
        .with(languages[locale], { locale: locale }, class: classes, method: :get)
        .and_return("#{languages[locale]} link")
    end
    assign(:public_instance, false)
    render
  end

  include_examples 'display element', 'partner logo', 'img', 'partner-logo', /partner-logo.*\.png/

  it 'is expected to display the application name' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.header.application_name'),
      href: root_path,
      class: 'navbar-brand'
    )
  end

  it 'is expected to not display a link to the InterSCity page' do
    expect(rendered).not_to have_selector(:link, href: 'https://interscity.org/')
  end

  context 'with a public instance' do
    before do
      assign(:public_instance, true)
      render
    end

    it 'is expected to display a link to the InterSCity page' do
      expect(rendered).to have_selector(:link, href: 'https://interscity.org/')
    end

    it 'is expected to display the public application name' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.public_application_name'),
        href: root_path,
        class: 'navbar-brand'
      )
    end
  end

  context 'menu links' do
    it 'is expected to display a link to map and filters' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.map_and_filters'),
        href: procedures_path,
        class: 'nav-link'
      )
    end

    xit 'is expected to display a link to health facilities' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.health_facilities'),
        href: '#',
        class: 'nav-link'
      )
    end

    it 'is expected to display a link to data and charts' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.data_and_charts'),
        href: general_data_path,
        class: 'nav-link'
      )
    end

    it 'is expected the link to data and charts to open in a new tab' do
      expect(rendered).to have_selector("a[href=\"#{general_data_path}\"][target='_blank']")
    end

    it 'is expected to display a link to the about page' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.about'),
        href: about_path,
        class: 'nav-link'
      )
    end

    it 'is expected to display a link to the user guide' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.guide'),
        href: guide_path,
        class: 'nav-link'
      )
    end

    it 'is expected to display the user menu toggle' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.user'),
        href: '#',
        class: 'nav-link dropdown-toggle'
      )
    end

    context 'without a signed in user' do
      it 'is expected to display a link to login' do
        expect(rendered).to have_selector(
          :link,
          I18n.t('views.shared.header.login'),
          href: new_user_session_path,
          class: 'dropdown-item'
        )
      end
    end

    context 'with a signed in user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user

        render
      end

      it 'is expected to display a link to logout' do
        expect(rendered).to have_selector(
          :link,
          I18n.t('views.shared.header.logout'),
          href: destroy_user_session_path,
          class: 'dropdown-item'
        )
      end

      it 'is expected to not display a link to admin' do
        expect(rendered).to_not have_selector(
          :link,
          I18n.t('views.shared.header.admin')
        )
      end

      context 'and this user is an admin' do
        before do
          user.update!(admin: true)
          sign_out user
          sign_in user

          render
        end

        it 'is expected to display a link to admin' do
          expect(rendered).to have_selector(
            :link,
            I18n.t('views.shared.header.admin'),
            href: trestle.root_path,
            class: 'dropdown-item'
          )
        end
      end
    end

    it 'is expected to display the language dropdown' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.language'),
        href: '#',
        class: 'nav-link dropdown-toggle'
      )
    end

    I18n.available_locales.each do |locale|
      classes = 'dropdown-item'
      classes += ' disabled' if I18n.locale == locale

      it "is expected to display the #{languages[locale]} link" do
        expect(view).to have_received(:link_to)
          .with(languages[locale], { locale: locale }, class: classes, method: :get)
        expect(rendered).to match(/#{languages[locale]} link/)
      end
    end
  end
end
