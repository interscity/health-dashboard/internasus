# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'footer img' do |name, file_name|
  it "is expected to display the #{name} logo in the footer" do
    expect(rendered).to have_selector('img')
    expect(rendered).to match("#{file_name}.\*")
  end
end

RSpec.describe 'shared/_footer.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to display a link to about' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.footer.about'),
      href: about_path,
      class: 'link'
    )
  end

  it 'is expected to display a link to FAQ' do
    expect(rendered).to have_selector(
      :link,
      I18n.t('views.shared.footer.guide'),
      href: guide_path,
      class: 'link'
    )
  end

  context 'sms version' do
    before do
      assign(:public_instance, false)
      render
    end

    it 'is expected to display the secretariat name' do
      expect(rendered).to have_content(
        I18n.t('views.shared.footer.sms')
      )
    end

    it 'is expected to display the contact' do
      expect(rendered).to have_content(
        I18n.t('views.shared.footer.contact')
      )
    end
  end

  context 'public version' do
    before do
      assign(:public_instance, true)
      render
    end

    include_examples 'footer img', 'ime-usp', 'ccsl_ime_usp'
    include_examples 'footer img', 'capes',   'capes'
    include_examples 'footer img', 'fapesp',  'fapesp'
    include_examples 'footer img', 'cnpq',    'cnpq'
  end
end
