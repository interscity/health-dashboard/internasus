# frozen_string_literal: true

require 'rails_helper'

def i18nt(key)
  I18n.t("general_data.characterization.#{key}")
end

RSpec.describe 'shared/services/active_filters', type: :view do
  context 'with active filters' do
    let(:admission_date_gteq) { '01/01/2015' }
    let(:leave_date_lteq) { '31/01/2015' }
    let(:health_facility_regional_health_coordination_id_in) { 'Sudeste' }
    let(:patient_datum_gender_eq_any) { 'Feminino' }
    let(:patient_datum_age_code_cont_any) { '44 a 50' }
    let(:hospitalization_type_in) { 'Urgência' }
    let(:active_filters) do
      {
        'admission_date_gteq' => admission_date_gteq,
        'leave_date_lteq' => leave_date_lteq,
        'health_facility_regional_health_coordination_id_in' => health_facility_regional_health_coordination_id_in,
        'patient_datum_gender_eq_any' => patient_datum_gender_eq_any,
        'patient_datum_age_code_cont_any' => patient_datum_age_code_cont_any,
        'hospitalization_type_in' => hospitalization_type_in
      }
    end

    before do
      render partial: 'shared/services/active_filters', locals: {
        active_filters: active_filters
      }
    end

    it 'is expected to display the starting date' do
      expect(rendered).to have_content("#{i18nt('admission_date_gteq')}: #{admission_date_gteq}")
    end

    it 'is expected to display the ending date' do
      expect(rendered).to have_content("#{i18nt('leave_date_lteq')}: #{leave_date_lteq}")
    end

    it 'is expected to display the regional health coordinations' do
      expect(rendered).to have_content(
        "#{i18nt('health_facility_regional_health_coordination_id_in')}: "\
        "#{health_facility_regional_health_coordination_id_in}"
      )
    end

    it 'is expected to display the patient gender' do
      expect(rendered).to have_content("#{i18nt('patient_datum_gender_eq_any')}: #{patient_datum_gender_eq_any}")
    end

    it 'is expected to display the patient age code' do
      expect(rendered).to have_content(
        "#{i18nt('patient_datum_age_code_cont_any')}: #{patient_datum_age_code_cont_any}"
      )
    end

    it 'is expected to display the hospitalization type' do
      expect(rendered).to have_content("#{i18nt('hospitalization_type_in')}: #{hospitalization_type_in}")
    end
  end

  context 'without active filters' do
    let(:active_filters) { {} }

    before do
      render partial: 'shared/services/active_filters', locals: {
        active_filters: active_filters
      }
    end

    it 'is expected to display a message about no filters' do
      expect(rendered).to have_content(i18nt('no_filters'))
    end
  end
end
