# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/show.html.erb', type: :view do
  let(:hospitalization_data) do
    FactoryBot.build(
      :hospitalization_datum,
      health_facility: health_facility,
      icd_category: icd_category
    )
  end
  let(:patient_data) do
    FactoryBot.create(
      :patient_datum,
      state: FactoryBot.build(:state),
      city: FactoryBot.build(:city),
      census_sector: FactoryBot.build(:census_sector),
      administrative_sector: FactoryBot.build(:administrative_sector),
      subprefecture: FactoryBot.build(:subprefecture),
      technical_health_supervision: FactoryBot.build(:technical_health_supervision),
      regional_health_coordination: FactoryBot.build(:regional_health_coordination),
      hospitalization_datum: hospitalization_data
    )
  end
  let(:health_facility) { FactoryBot.build(:health_facility) }
  let(:icd_category) { FactoryBot.build(:icd_category) }
  let(:translated_gender) { 'Feminino' }
  let(:translated_race) { 'Sem informação' }
  let(:translated_education_level) { 'Terceiro grau' }

  before do
    expect(view).to receive(:human_attribute_value).with(patient_data, 'gender').and_return(translated_gender)
    expect(view).to receive(:human_attribute_value).with(patient_data, 'race').and_return(translated_race)
    expect(view).to receive(:human_attribute_value)
      .with(patient_data, 'educational_level')
      .and_return(translated_education_level)

    assign(:patient_datum, patient_data)
    assign(:hospitalization_datum, hospitalization_data)

    render
  end

  [
    I18n.t('procedures.show.gender'),
    I18n.t('procedures.show.race'),
    I18n.t('procedures.show.age_year'),
    I18n.t('procedures.show.educational_level'),
    I18n.t('procedures.show.state'),
    I18n.t('procedures.show.city'),
    I18n.t('procedures.show.census_sector'),
    I18n.t('procedures.show.administrative_sector'),
    I18n.t('procedures.show.subprefecture'),
    I18n.t('procedures.show.technical_health_supervision'),
    I18n.t('procedures.show.regional_health_coordination'),
    I18n.t('procedures.show.icd_category'),
    I18n.t('procedures.show.admission_date'),
    I18n.t('procedures.show.distance'),
    I18n.t('procedures.show.health_facility')
  ].each do |content|
    it "is expected to have content \"#{content}\"" do
      expect(rendered).to have_content(content)
    end
  end

  it 'is expected to contain the patient data' do
    [
      translated_gender,
      translated_race,
      translated_education_level,
      patient_data.age_year,
      patient_data.educational_level,
      patient_data.census_sector.code,
      patient_data.administrative_sector.name,
      patient_data.subprefecture.name,
      patient_data.technical_health_supervision.name,
      patient_data.regional_health_coordination.name,
      icd_category.description,
      hospitalization_data.admission_date,
      hospitalization_data.distance,
      health_facility.name
    ].each do |content|
      expect(rendered).to have_content(content)
    end
  end
end
