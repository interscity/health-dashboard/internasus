# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/search.json.erb', type: :view do
  let(:patients_data) { [] }
  let(:health_facilities) { double 'health_facilities' }
  let(:grouped_facilities) { double 'grouped_facilities' }
  let(:health_facilities_ids) { [1, 2, 3] }
  let(:sectors_counts) { [] }
  let(:distance_percentiles) { [] }
  let(:divisions_rates) { [] }
  let(:facilities_data_array) { [[1, 2, 3, 4, 5, 6, 7, 8]] }

  before do
    allow(health_facilities).to receive(:ids).and_return(health_facilities_ids)
    allow(health_facilities).to receive(:map).and_return([])
    allow(health_facilities).to receive_message_chain(:select, :joins, :group).and_return(grouped_facilities)
    allow(HealthFacility).to receive_message_chain(:from, :order, :pluck).and_return(facilities_data_array)

    assign(:patients_data, patients_data)
    assign(:health_facilities, health_facilities)
    assign(:divisions_rates, divisions_rates)

    render
  end

  subject { JSON.parse(rendered) }

  it { is_expected.to have_key('patients_data') }
  it { is_expected.to have_key('health_facilities') }
  it { is_expected.to have_key('divisions_rates') }
end
