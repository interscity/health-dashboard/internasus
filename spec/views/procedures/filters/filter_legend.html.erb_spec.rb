# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/filters/_filter_legend.html.erb', type: :view do
  let(:name) { 'name' }
  let(:title) { 'title' }

  before do
    render partial: 'procedures/filters/filter_legend',
           locals: { name: name, title: title }
  end

  it 'is expected to create a div with the name as id' do
    expect(rendered).to have_selector(:id, name)
  end

  it 'is expected to create a js variable with the name as prefix' do
    expect(rendered).to match(/var #{name}Legend =/)
  end

  it 'is expected to call js with the title as argument' do
    expect(rendered).to match(/, "#{title}"\)/)
  end
end
