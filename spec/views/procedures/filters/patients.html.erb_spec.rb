# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'
require 'support/helpers'

RSpec.configure do |c|
  c.include Ransack::Helpers::FormHelper
  c.include TestHelpers
end

RSpec.describe 'procedures/filters/_patients.html.erb', type: :view do
  before do
    allow(PatientDatum).to receive_message_chain(:where, :not, :any?).and_return(true)
    allow(view).to receive(:max_distance).and_return(1.23)
    render_with_ransack_form 'procedures/filters/patients'
  end

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_age_code',
                   I18n.t('procedures.index.patient.age_code'),
                   'cont_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_race',
                   I18n.t('procedures.index.patient.race'),
                   'cont_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_educational_level',
                   I18n.t('procedures.index.patient.educational_level'),
                   'in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_gender',
                   I18n.t('procedures.index.patient.gender'),
                   'eq_any'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_state',
                   I18n.t('procedures.index.divisions.state'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_city',
                   I18n.t('procedures.index.divisions.city'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_administrative_sector',
                   I18n.t('procedures.index.divisions.administrative_sector'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_subprefecture',
                   I18n.t('procedures.index.divisions.subprefecture'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_technical_health_supervision',
                   I18n.t('procedures.index.divisions.technical_health_supervision'),
                   'id_in'

  include_examples 'contain field',
                   'select',
                   'q_patient_datum_regional_health_coordination',
                   I18n.t('procedures.index.divisions.regional_health_coordination'),
                   'id_in'

  include_examples 'contain field',
                   'input',
                   'q_distance',
                   I18n.t('procedures.index.procedure.distance'),
                   'between'
end
