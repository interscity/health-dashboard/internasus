# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/print.html.erb', type: :view do
  let(:encoded_map_content) { 'base64_encoded_map' }
  let(:active_filters) { {} }
  let(:heatmap_type) { '' }
  let(:heatmap_gradient) { '' }
  let(:heatmap_max) { '' }
  let(:filter_partial_params) do
    {
      partial: 'shared/services/active_filters',
      locals: { active_filters: active_filters }
    }
  end

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(**filter_partial_params)

    assign(:base64_map, encoded_map_content)
    assign(:active_filters, active_filters)
    assign(:heatmap_type, heatmap_type)
    assign(:heatmap_gradient, heatmap_gradient)
    assign(:heatmap_max, heatmap_max)

    render
  end

  it 'is expected to render the map as an image' do
    expect(rendered).to match(encoded_map_content)
  end

  it 'is expected to show the search active filters' do
    expect(view).to have_received(:render).with(**filter_partial_params)
  end

  describe 'heatmap legend' do
    context 'with no heatmap on the map' do
      it 'is expected not to show the heatmap legend' do
        expect(rendered).not_to have_selector('div#heatmap-legend')
      end
    end

    context 'with a heatmap on the map' do
      let(:heatmap_type) { 'procedures' }
      let(:heatmap_gradient) { 'some_gradient' }
      let(:heatmap_max) { 0.165 }

      it 'is expected not to show the heatmap legend' do
        expect(rendered).to have_selector('div#heatmap-legend')
      end

      it 'is expected to show the heatmap gradient' do
        expect(rendered).to match("background-image: #{heatmap_gradient}")
      end

      it 'is expected to show the maximum value represented by the heatmap' do
        expect(rendered).to have_content(heatmap_max)
      end
    end
  end
end
