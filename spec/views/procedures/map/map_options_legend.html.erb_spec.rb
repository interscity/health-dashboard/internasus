# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/map/_map_options_legend.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to contain the legend title' do
    expect(rendered).to have_selector('button', text: I18n.t('procedures.map.map_options_legend.title'), visible: true)
  end

  it 'is expected to contain the legend body' do
    expect(rendered).to have_selector('div', id: 'map-options-legend', visible: false)
  end

  it 'is expected to contain the cluster map options hidden' do
    expect(rendered).to have_selector('div', id: 'map-options-legend-cluster', visible: false)
    expect(rendered).to have_selector('span', id: 'map-options-legend-cluster-radius', visible: false)
  end

  it 'is expected to contain the heatmap map options hidden' do
    expect(rendered).to have_selector('div', id: 'map-options-legend-heatmap', visible: false)
    [%w[ul by], %w[li by-count], %w[li by-csw], %w[li high-contrast], %w[span radius], %w[span opacity]].each do |v|
      expect(rendered).to have_selector(v[0], id: "map-options-legend-heatmap-#{v[1]}", visible: false)
    end
  end

  it 'is expected to contain the administrative limits map options hidden' do
    expect(rendered).to have_selector('div', id: 'map-options-legend-limits', visible: false)
    %w[City RegionalHealthCoordination TechnicalHealthSupervision Subprefecture AdministrativeSector
       Ubs FamilyHealthStrategy CensusSector].each do |v|
      expect(rendered).to have_selector('li', id: "map-options-legend-limits-#{v}", visible: false)
    end
  end

  it 'is expected to contain the facilities map options hidden' do
    expect(rendered).to have_selector('div', id: 'map-options-legend-facilities', visible: false)
    expect(rendered).to have_selector('ul', id: 'map-options-legend-facilities-list', visible: false)
    expect(rendered).to have_selector('li', id: 'map-options-legend-facilities-icons', visible: false)
    expect(rendered).to have_selector('li', id: 'map-options-legend-facilities-percentiles', visible: false)
  end
end
