# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/contain_field'

RSpec.describe 'procedures/map/_options.html.erb', type: :view do
  before do
    [State, City, RegionalHealthCoordination, TechnicalHealthSupervision, Subprefecture,
     AdministrativeSector, Ubs, FamilyHealthStrategy, CensusSector].each do |klass|
      allow(klass).to receive_message_chain(:where, :not, :any?).and_return(true)
    end
    render
  end

  describe 'fields' do
    include_examples 'contain field',
                     'input[type=checkbox]',
                     'clustering_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=text]',
                     'clustering_radius',
                     I18n.t('procedures.map.options.radius')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'heat_map_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=text]',
                     'heat_map_radius',
                     I18n.t('procedures.map.options.radius')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'heat_map_high_contrast',
                     I18n.t('procedures.map.options.high_contrast')

    include_examples 'contain field',
                     'input[type=text]',
                     'heat_map_opacity',
                     I18n.t('procedures.map.options.opacity')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'csw_heat_map_enabled',
                     I18n.t('procedures.map.options.enabled')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_state_limits',
                     I18n.t('procedures.map.divisions.state')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_city_limits',
                     I18n.t('procedures.map.divisions.city')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_regional_health_coordination_limits',
                     I18n.t('procedures.map.divisions.regional_health_coordination')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_technical_health_supervision_limits',
                     I18n.t('procedures.map.divisions.technical_health_supervision')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_subprefecture_limits',
                     I18n.t('procedures.map.divisions.subprefecture')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_administrative_sector_limits',
                     I18n.t('procedures.map.divisions.administrative_sector')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_ubs_limits',
                     I18n.t('procedures.map.divisions.ubs')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_family_health_strategy_limits',
                     I18n.t('procedures.map.divisions.family_health_strategy')

    include_examples 'contain field',
                     'input[type=checkbox]',
                     'show_census_sector_limits',
                     I18n.t('procedures.map.divisions.census_sector')
  end
end
