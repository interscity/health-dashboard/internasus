# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/export.csv.erb', type: :view do
  let(:patients_data) { double('patients_data') }

  before do
    expect(patients_data).to receive(:find_in_batches).and_yield([])

    assign(:patients_data, patients_data)

    render
  end

  %w[COD LAT_SC LONG_SC P_SEXO P_IDADE P_RACA LV_INSTRU CNES GESTOR_ID CAR_INTEN CMPT DT_EMISSAO DT_INTERNA DT_SAIDA
     COMPLEXIDA PROC_RE DIAG_PR DIAG_SE1 DIAG_SE2 DIARIAS DIARIAS_UT DIARIAS_UI DIAS_PERM FINANC VAL_TOT DA SUB STS
     CRS DISTANCIA_KM].each do |field|
    it "is expected to have the header #{field}" do
      expect(rendered).to match(/#{field}/)
    end
  end
end
