# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'procedures/index.html.erb', type: :view do
  let(:search) { HospitalizationDatum.ransack([]) }
  let(:health_facilities) { double('health_facilities') }

  before do
    allow(view).to receive(:max_distance).and_return(1.23)
    allow(view).to receive(:max_service_value).and_return(1.23)

    assign(:search, search)
    assign(:health_facilities, health_facilities)

    allow(view).to receive(:prepare_health_facilities_data)

    render
  end

  context 'page element' do
    context 'map' do
      it 'is expected to have the div where the map will be loaded' do
        expect(rendered).to have_selector('div', id: 'map', class: 'col')
      end
    end

    context 'filters list' do
      it 'is expected to have a form' do
        expect(rendered).to have_selector("form[method=post][action=#{search_procedures_path.gsub('/', '\/')}]")
      end

      it 'is expected to display the search button' do
        expect(rendered).to have_button(I18n.t('procedures.index.search'))
      end
    end

    it 'is expected to have a link for CSV download' do
      expect(rendered).to have_link(I18n.t('procedures.index.download_csv'), href: export_procedures_path(format: :csv))
    end

    it 'is expected to prepare the health facilities data to map rendering' do
      expect(view).to have_received(:prepare_health_facilities_data).with(health_facilities, false)
    end
  end
end
