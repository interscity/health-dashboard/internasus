# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/guide.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to render the title' do
    expect(rendered).to have_selector('h1', text: 'Guia do Usuário')
  end
end
