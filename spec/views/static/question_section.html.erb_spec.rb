# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_question_section.html.erb', type: :view do
  let(:block_name) { 'block_name' }
  let(:name) { 'section_name' }
  let(:question) { { question: '?', answer: '!' } }
  let(:question_section) do
    [{ name: name }, question]
  end

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render)
      .with(partial: 'question', collection: anything)

    render partial: 'static_pages/question_section',
           locals: { question_section: question_section,
                     block_name: block_name }
  end

  it 'is expected to render an element with a certain id' do
    expect(rendered).to have_selector('span', id: "#{block_name}_#{name}")
  end

  it 'is expected to render a header' do
    expect(rendered).to have_selector('p',
                                      text: name,
                                      class: 'question-header')
  end

  it 'is expected to render the questions and answers' do
    expect(view).to have_received(:render).with(partial: 'question', collection: [question])
  end
end
