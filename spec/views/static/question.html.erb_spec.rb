# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_question.html.erb', type: :view do
  let(:question) { { question: 'question?', answer: 'answer!' } }

  before do
    render partial: 'static_pages/question', locals: { question: question }
  end

  %w[question answer].each do |field|
    it "is expected to render the #{field}" do
      expect(rendered).to have_selector('p', text: question[field.to_s], class: field)
    end
  end
end
