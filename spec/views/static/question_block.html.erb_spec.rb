# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_question_block.html.erb', type: :view do
  let(:question_block) do
    [:getting_started, { filter: [{ name: 'name' }] }]
  end

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render)
      .with(
        partial: 'question_section',
        collection: anything,
        locals: anything
      )

    render partial: 'static_pages/question_block',
           locals: { question_block: question_block }
  end

  it 'is expected to render the question block' do
    expect(rendered).to have_selector('div', class: 'question-block')
  end

  it 'is expected to render a header' do
    expect(rendered).to have_selector('p',
                                      text: 'name',
                                      class: 'category')
  end

  it 'is expected to add a listener to jump to category' do
    expect(rendered).to match(/onclick="userGuide\.jump\('getting_started_name'\)/)
  end

  it 'is expected to render the questions and answers' do
    expect(view).to have_received(:render).with(
      partial: 'question_section',
      collection: anything,
      locals: anything
    )
  end
end
