# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_member.html.erb', type: :view do
  let(:email) { 'email' }
  let(:linkedin) { 'linkedin' }
  let(:twitter) { 'twitter' }
  let(:gitlab_profile) { 'gitlab_profile' }
  let(:webpage) { 'webpage' }

  let(:member) do
    {
      name: 'Name',
      image: 'image',
      descrition: 'hey',
      email: email,
      linkedin: linkedin,
      twitter: twitter,
      gitlab_profile: gitlab_profile,
      webpage: webpage
    }
  end

  before do
    allow(view).to receive(:image_tag)

    render partial: 'static_pages/member', locals: { member: member }
  end

  it 'is expected to render an image' do
    expect(view).to have_received(:image_tag).with(member[:image], class: 'membro-img')
  end

  it 'is expected to show the email' do
    expect(rendered).to have_selector('p', text: email)
  end

  %w[linkedin twitter gitlab_profile webpage].each do |field|
    it "is expected to show a link to #{field}" do
      expect(rendered).to have_selector("a[href='#{field}']")
    end
  end

  context 'with missing fields' do
    let(:linkedin) { nil }
    let(:twitter) { nil }
    let(:gitlab_profile) { nil }
    let(:webpage) { nil }

    %w[linkedin twitter gitlab_profile webpage].each do |field|
      it "is expected not to show a link to #{field}" do
        expect(rendered).not_to have_selector("a[href='#{field}']")
      end
    end
  end
end
