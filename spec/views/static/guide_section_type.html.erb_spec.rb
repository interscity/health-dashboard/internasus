# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_guide_section_type.html.erb', type: :view do
  let(:section_type) { 'getting_started' }
  let(:section_type_counter) { 3 }

  before do
    render partial: 'static_pages/guide_section_type', locals: {
      guide_section_type: section_type,
      guide_section_type_counter: section_type_counter
    }
  end

  it 'is expected to render the type' do
    expect(rendered).to have_selector('p', text: t("static_pages.guide.#{section_type}"))
  end

  it 'is expected to apply style to the type by number' do
    expect(rendered).to have_selector('div', class: "type-#{section_type_counter + 1}")
  end

  it 'is expected to identify the type element by number' do
    expect(rendered).to have_selector("#type#{section_type_counter + 1}")
  end

  it 'is expected to add an event listener to change FAQ section' do
    expect(rendered).to match(/onclick="userGuide\.changeFAQ\(#{section_type_counter + 1}\)/)
  end
end
