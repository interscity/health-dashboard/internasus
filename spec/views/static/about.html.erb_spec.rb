# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/about.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to show information about the project' do
    expect(rendered).to have_selector('h2', text: I18n.t('static_pages.about.internasus'))
    expect(rendered).to have_selector('h2', text: I18n.t('static_pages.about.development'))
    expect(rendered).to have_selector('h2', text: I18n.t('static_pages.about.data'))
  end

  it 'is expected to show usage slides' do
    expect(rendered).to have_selector('h1', text: I18n.t('static_pages.about.usage'))
  end

  it 'is expected to show information about members' do
    expect(rendered).to have_selector('h1', text: I18n.t('static_pages.about.current_members'))
    expect(rendered).to have_selector('h1', text: I18n.t('static_pages.about.former_members'))
  end
end
