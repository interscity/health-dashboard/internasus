# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'static_pages/_about_section.html.erb', type: :view do
  let(:icon_class) { 'icon class' }
  let(:title) { 'title' }
  let(:content) { 'content' }

  let(:section) do
    {
      title: title,
      icon_class: icon_class,
      content: content
    }
  end

  before do
    render partial: 'static_pages/about_section', locals: { about_section: section }
  end

  it 'is expected to render the title' do
    expect(rendered).to have_selector('h2', text: title)
  end

  it 'is expected to render an icon' do
    expect(rendered).to have_selector('i', class: icon_class)
  end

  it 'is expected to render the content' do
    expect(rendered).to have_selector('p', text: content)
  end
end
