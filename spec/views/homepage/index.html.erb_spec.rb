# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/display_element'

RSpec.describe 'homepage/index.html.erb', type: :view do
  let(:hospitalization_counter) { 554.202 }
  let(:health_facility_counter) { 97 }
  let(:specialty_counter) { 9 }
  let(:mean_distance) { 10 }
  let(:dataset_years) { %w[2019 2020] }
  let(:carousel_images) do
    %w[homepage-slides/sms-sp/slide1.jpg homepage-slides/sms-sp/slide2.jpg
       homepage-slides/sms-sp/slide3.jpg homepage-slides/sms-sp/slide4.jpg]
  end

  before do
    assign(:hospitalization_counter, hospitalization_counter)
    assign(:health_facility_counter, health_facility_counter)
    assign(:specialty_counter, specialty_counter)
    assign(:mean_distance, mean_distance)
    assign(:dataset_years, dataset_years)
    assign(:carousel_images, carousel_images)
    render
  end

  context 'slider images' do
    include_examples 'display element', 'first slide', 'div', 'carousel-item', /slide1.*\.jpg/

    include_examples 'display element', 'second slide', 'div', 'carousel-item', /slide2.*\.jpg/

    include_examples 'display element', 'third slide', 'div', 'carousel-item', /slide3.*\.jpg/

    include_examples 'display element', 'fourth slide', 'div', 'carousel-item', /slide4.*\.jpg/
  end

  context 'counter' do
    include_examples 'display element', 'hospitalization counter', 'p', 'counter', /554/

    include_examples 'display element', 'health facility counter', 'p', 'counter', /97/

    include_examples 'display element', 'specialty counter', 'p', 'counter', /9/

    include_examples 'display element', 'mean distance counter', 'p', 'counter', /10 Km/

    include_examples 'display element', 'dataset_years', 'p', 'counter', /2019, 2020/
  end

  context 'services' do
    include_examples 'display element', 'advanced search image', 'img', 'card-img-top', /search.*\.png/

    include_examples 'display element', 'health facilities image', 'img', 'card-img-top', /facilities.*\.png/

    include_examples 'display element', 'general data image', 'img', 'card-img-top', /data.*\.png/

    include_examples 'display element', 'about page image', 'img', 'card-img-top', /about.*\.png/

    it 'is expected to display a link to map and filters' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.map_and_filters'),
        href: procedures_path
      )
    end

    xit 'is expected to display a link to health facilities' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.health_facilities'),
        href: '#'
      )
    end

    it 'is expected to display a link to data and charts' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.header.data_and_charts'),
        href: general_data_path
      )
    end

    xit 'is expected to display a link to about' do
      expect(rendered).to have_selector(
        :link,
        I18n.t('views.shared.footer.about'),
        href: '#'
      )
    end
  end
end
