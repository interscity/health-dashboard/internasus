# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DemographicDivision, type: :model do
  it { is_expected.to have_one(:population) }
end
