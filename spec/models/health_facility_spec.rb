# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HealthFacility, type: :model do
  it { is_expected.to belong_to(:state).optional }
  it { is_expected.to belong_to(:city).optional }
  it { is_expected.to belong_to(:administrative_sector).optional }
  it { is_expected.to belong_to(:subprefecture).optional }
  it { is_expected.to belong_to(:technical_health_supervision).optional }
  it { is_expected.to belong_to(:regional_health_coordination).optional }
  it { is_expected.to have_many(:hospitalization_data) }

  describe 'methods' do
    subject { build(:health_facility) }

    describe 'hospitalization_rate' do
      it 'is expected to return the hospitalization rate for the health facility' do
        expect(subject).to receive_message_chain(:hospitalization_data, :count).and_return 10
        expect(HospitalizationDatum).to receive(:count).and_return 100

        expect(subject.hospitalization_rate).to eq(0.1)
      end
    end

    describe 'self.administrations' do
      before do
        allow(described_class).to receive_message_chain(:select, :distinct, :pluck)

        described_class.administrations
      end

      it 'is expected to select the administrations' do
        expect(described_class).to have_received(:select).with(:administration)
      end

      it 'is expected to get only the distinct administrations' do
        expect(described_class.select).to have_received(:distinct).with(no_args)
      end

      it 'is expected to get the administrations in an array' do
        expect(described_class.select.distinct).to have_received(:pluck).with(:administration)
      end
    end
  end
end
