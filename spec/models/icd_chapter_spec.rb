# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IcdChapter, type: :model do
  it { is_expected.to have_many(:icd_categories) }
  it { is_expected.to have_many(:icd_groups) }
end
