# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CensusSector, type: :model do
  it { is_expected.to have_many(:patient_data) }
end
