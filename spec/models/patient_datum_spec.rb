# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/procedure_id_association'

RSpec.describe PatientDatum, type: :model do
  it { is_expected.to belong_to(:city).optional }
  it { is_expected.to belong_to(:state).optional }
  it { is_expected.to belong_to(:administrative_sector).optional }
  it { is_expected.to belong_to(:subprefecture).optional }
  it { is_expected.to belong_to(:technical_health_supervision).optional }
  it { is_expected.to belong_to(:regional_health_coordination).optional }
  it { is_expected.to belong_to(:census_sector).optional }
  it { is_expected.to belong_to(:hospitalization_datum) }
end
