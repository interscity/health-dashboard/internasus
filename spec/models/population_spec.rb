# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Population, type: :model do
  it { is_expected.to belong_to(:demographic_division) }
end
