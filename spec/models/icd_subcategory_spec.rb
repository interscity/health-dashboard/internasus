# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IcdSubcategory, type: :model do
  it { is_expected.to belong_to(:icd_category) }
  it { is_expected.to have_many(:hospitalization_data) }
end
