# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/procedure_id_association'

RSpec.describe HospitalizationDatum, type: :model do
  subject { FactoryBot.build(:hospitalization_datum) }

  it { is_expected.to belong_to(:health_facility) }
  it { is_expected.to belong_to(:icd_subcategory) }
  it { is_expected.to belong_to(:icd_category) }
  it { is_expected.to belong_to(:secondary_icd1).optional }
  it { is_expected.to belong_to(:secondary_icd2).optional }
  it { is_expected.to have_one(:patient_datum) }

  describe 'dataset_years' do
    before do
      allow(described_class).to receive_message_chain(:select, :distinct, :order, :map)

      described_class.dataset_years
    end

    it 'is expected to select the data year by competence' do
      expect(described_class).to have_received(:select)
        .with('substr(cast(competence as text), 1, 4) as year')
    end

    it 'is expected to select only those with distinct year' do
      expect(described_class.select).to have_received(:distinct).with(no_args)
    end

    it 'is expected to sort the years' do
      expect(described_class.select.distinct).to have_received(:order).with(:year)
    end

    it 'is expected to get the year from the results' do
      expect(described_class.select.distinct.order).to have_received(:map)
    end
  end
end
