# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TechnicalHealthSupervision, type: :model do
  it { is_expected.to have_many(:health_facilities) }
  it { is_expected.to have_many(:patient_data) }
end
