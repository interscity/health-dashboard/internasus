import * as jQuery from 'jquery';

import { ChartGenerator } from '../../../app/javascript/packs/general_data/ChartGenerator'

describe('ChartGenerator', () => {

  const ext = jasmine.createSpyObj('externalMocks', ['window']);
  const echarts = jasmine.createSpyObj('echarts', ['init', 'registerMap']);
  let subject: ChartGenerator;

  beforeEach(() => {
    subject = new ChartGenerator(echarts, jQuery, ext.window);
  });

  describe('createChart', () => {
    const foundElement = jasmine.createSpyObj('accordion', ['bind']);
    const windowElement = jasmine.createSpyObj('window', ['on']);
    const elementId = 'chart';
    const title = 'This is a Chart';
    const customizationOptions = {};
    const resizeTriggerElement = 'div id';
    const chart = jasmine.createSpyObj('chart', ['setOption']);
    const element = document.createElement('div');
    document.getElementById = jasmine.createSpy('element').and.returnValue(element);

    beforeEach(() => {
      echarts.init.withArgs(element).and.returnValue(chart);
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init')
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        .withArgs(resizeTriggerElement, undefined).and.returnValue(foundElement)
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        .withArgs(ext.window, undefined).and.returnValue(windowElement);
    });

    describe('create a line chart', () => {
      const chartType = 'line';
      const data = [{name: 'jan 2020', value: 0}, {name: 'fev 2020', value: 50}, {name: 'mar 2020', value: 100}];

      beforeEach(() => {
        subject.createChart(elementId, title, data, chartType, customizationOptions, resizeTriggerElement);
      });

      it('is expected to create a new echarts instance', () => {
        expect(document.getElementById).toHaveBeenCalledWith(elementId);
        expect(echarts.init).toHaveBeenCalledWith(element);
      });

      it('is expected to set the option to the chart instance', () => {
        expect(chart.setOption).toHaveBeenCalled();
      });

      it('is expected to retrieve the accordion element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(resizeTriggerElement, undefined);
      });

      it('is expected to add a bind event trigger to the accordion', () => {
        expect(foundElement.bind).toHaveBeenCalledWith('shown.bs.collapse', jasmine.any(Function));
      });

      it('is expected to retrieve the window element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(ext.window, undefined);
      });

      it('is expected to add a trigger upon window resizes', () => {
        expect(windowElement.on).toHaveBeenCalledWith('resize', jasmine.any(Function));
      });
    });

    describe('create a pie chart', () => {
      const chartType = 'pie';
      const data = [{name: 'jan 2020', value: 0}, {name: 'fev 2020', value: 50}];

      beforeEach(() => {
        subject.createChart(elementId, title, data, chartType, customizationOptions, resizeTriggerElement);
      });

      it('is expected to create a new echarts instance', () => {
        expect(document.getElementById).toHaveBeenCalledWith(elementId);
        expect(echarts.init).toHaveBeenCalledWith(element);
      });

      it('is expected to set the option to the chart instance', () => {
        expect(chart.setOption).toHaveBeenCalled();
      });

      it('is expected to retrieve the accordion element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(resizeTriggerElement, undefined);
      });

      it('is expected to add a bind event trigger to the accordion', () => {
        expect(foundElement.bind).toHaveBeenCalledWith('shown.bs.collapse', jasmine.any(Function));
      });

      it('is expected to retrieve the window element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(ext.window, undefined);
      });

      it('is expected to add a trigger upon window resizes', () => {
        expect(windowElement.on).toHaveBeenCalledWith('resize', jasmine.any(Function));
      });
    });

    describe('create a stacked bar chart', () => {
      const data = [['categories', 'a', 'b', 'c', 'd'], [['category', 1, 2, 3, 4]], [['category', 10.0, 20.0, 30.0, 40.0]]];

      beforeEach(() => {
        subject.createChart(elementId, title, data, 'stacked-bar', {}, resizeTriggerElement);
      });

      it('is expected to create a new echarts instance', () => {
        expect(document.getElementById).toHaveBeenCalledWith(elementId);
        expect(echarts.init).toHaveBeenCalledWith(element);
      });

      it('is expected to set the option to the chart instance', () => {
        expect(chart.setOption).toHaveBeenCalled();
      });

      it('is expected to retrieve the accordion element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(resizeTriggerElement, undefined);
      });

      it('is expected to add a bind event trigger to the accordion', () => {
        expect(foundElement.bind).toHaveBeenCalledWith('shown.bs.collapse', jasmine.any(Function));
      });

      it('is expected to retrieve the window element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(ext.window, undefined);
      });

      it('is expected to add a trigger upon window resizes', () => {
        expect(windowElement.on).toHaveBeenCalledWith('resize', jasmine.any(Function));
      });
    });

    describe('create a treemap chart', () => {
      const chartType = 'treemap';
      const data = [{name: 'node 1', children: [{name: 'node 1a', value: 20}, {name: 'node 1b', value: 30}]}]

      beforeEach(() => {
        subject.createChart(elementId, title, data, chartType, customizationOptions, resizeTriggerElement);
      });

      it('is expected to create a new echarts instance', () => {
        expect(document.getElementById).toHaveBeenCalledWith(elementId);
        expect(echarts.init).toHaveBeenCalledWith(element);
      });

      it('is expected to set the option to the chart instance', () => {
        expect(chart.setOption).toHaveBeenCalled();
      });

      it('is expected to retrieve the accoridon element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(resizeTriggerElement, undefined);
      });

      it('is expected to add a bind event trigger to the accordion', () => {
        expect(foundElement.bind).toHaveBeenCalledWith('shown.bs.collapse', jasmine.any(Function));
      });

      it('is expected to retrieve the window element', () => {
        // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
        // @ts-ignore: TS2345
        expect(jQuery.fn.init).toHaveBeenCalledWith(ext.window, undefined);
      });

      it('is expected to add a trigger upon window resizes', () => {
        expect(windowElement.on).toHaveBeenCalledWith('resize', jasmine.any(Function));
      });
    });
  });

  describe('createColorMap', () => {
    const elementId = 'chart';
    const title = 'This is a Chart';
    const resizeTriggerElement = 'div id';
    const data = {max_value: 10, data: [{name: 'Itaquera', value: 10}, {name: 'Lajeado', value: 5}]};
    const area = 'subprefectures';
    const ajax = jasmine.createSpyObj('ajax', ['done']);

    beforeEach(() => {
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery, 'get').and.returnValue(ajax);
      subject.createColorMap(elementId, title, data, resizeTriggerElement, area);
    });

    it('is expected to fetch the map via AJAX', () => {
      expect(jQuery.get).toHaveBeenCalledWith('demographic_divisions/geojson/subprefectures');
      expect(ajax.done).toHaveBeenCalled();
    });
  });
});
