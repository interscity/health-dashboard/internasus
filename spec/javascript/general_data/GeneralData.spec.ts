import { GeneralData } from '../../../app/javascript/packs/general_data/GeneralData';

describe('GeneralData', () => {

  const ext = jasmine.createSpyObj('externalMocks', ['jQuery']);
  const colorHashes = { dataType: { item: 'color'} };

  let subject: GeneralData;

  beforeEach(() => {
    subject = new GeneralData(ext.jQuery);
  });

  describe('contentSelectFor', () => {
    const select = { value: 1 };
    const category = 'ranking-table';
    const allTables = jasmine.createSpyObj('allTables', ['css']);
    const visibleTable = jasmine.createSpyObj('visibleTable', ['css']);

    beforeEach(() => {
      ext.jQuery.withArgs(`[id^="${category}"]`).and.returnValue(allTables);
      ext.jQuery.withArgs(`#${category}-1`).and.returnValue(visibleTable);

      subject.contentSelectFor(category, select);
    });

    it('is expected to hide all the tables', () => {
      expect(allTables.css).toHaveBeenCalledWith('display', 'none');
    });

    it('is expected to show the selected table', () => {
      expect(visibleTable.css).toHaveBeenCalledWith('display', 'block');
    });
  });

  describe('dynamicChartFor', () => {
    const echart = jasmine.createSpyObj('echarts', ['dispose']);
    const chart = { echarts: echart, createChart: jasmine.createSpy('chart'), createColorMap: jasmine.createSpy('map') };
    const selected = 'chart';
    const chartData = {};
    const element = document.createElement('div');
    document.getElementById = jasmine.createSpy('element').and.returnValue(element);
    const options = {};

    describe('map charts', () => {
      const area = 'area';
      const data = { chart: { chart_type: 'map', data: chartData, chart_area: area } };
      beforeEach(() => {
        subject.dynamicChartFor(selected, data, chart);
      });

      it('is expected to dispose previously created charts', () => {
        expect(document.getElementById).toHaveBeenCalledWith('dynamic-chart');
        expect(echart.dispose).toHaveBeenCalledWith(element);
      });

      it('is expected to create the desired chart with its data and customization options', () => {
        expect(chart.createColorMap).toHaveBeenCalledWith(
          'dynamic-chart', '',
          chartData,
          '#collapseGeneralDatadynamic',
          area
        );
      });
    });

    describe('non map charts', () => {
      const data = { chart: { chart_type: 'bar', data: chartData, chart_options: {} } };
      beforeEach(() => {
        subject.dynamicChartFor(selected, data, chart);
      });

      it('is expected to dispose previously created charts', () => {
        expect(document.getElementById).toHaveBeenCalledWith('dynamic-chart');
        expect(echart.dispose).toHaveBeenCalledWith(element);
      });

      it('is expected to create the desired chart with its data and customization options', () => {
        expect(chart.createChart).toHaveBeenCalledWith(
          'dynamic-chart', '',
          chartData,
          'bar',
          {},
          '#collapseGeneralDatadynamic'
        );
      });
    });
  });

  describe('healthFacilitiesChartFor', () => {
    const echart = jasmine.createSpyObj('echarts', ['dispose']);
    const selectedId = '0';
    const selectedName = 'healthFacility';
    const dataSet = {
      'data': { 'healthFacility': [] },
      'chart_title': 'title',
      'chart_type': 'stacked_bar',
      'chart_options': {}
    };
    const chart = { echarts: echart, createChart: jasmine.createSpy('chart') };
    const element = document.createElement('div');
    document.getElementById = jasmine.createSpy('element').and.returnValue(element);

    beforeEach(() => {
      subject.healthFacilitiesChartFor(selectedId, dataSet, chart);
    });

    it('is expected to dispose previously created charts', () => {
      expect(document.getElementById).toHaveBeenCalledWith('health-facility-specialty-distances-chart');
      expect(echart.dispose).toHaveBeenCalledWith(element);
    });

    it('is expected to get the name of health facility from selected id', () => {
      expect(Object.keys(dataSet.data)[selectedId]).toBe(selectedName);
    });

    it('is expected to create the desired chart with its data and customization options', () => {
      expect(chart.createChart).toHaveBeenCalledWith(
        'health-facility-specialty-distances-chart',
        dataSet.chart_title,
        dataSet.data[selectedName],
        dataSet.chart_type,
        dataSet.chart_options,
        '#collapseGeneralDatahealth_facilities_specialty_distances'
      );
    });
  });
});
