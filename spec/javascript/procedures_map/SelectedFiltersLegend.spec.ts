import { SelectedFiltersLegend } from '../../../app/javascript/packs/procedures_map/SelectedFiltersLegend';

describe('SelectedFiltersLegend', () => {
  const jQuery = jasmine.createSpy('jQuery');

  let subject: SelectedFiltersLegend;
  const destinationId = 'some-div-id';
  const title = 'legend-title';

  beforeEach(() => {
    subject = new SelectedFiltersLegend(jQuery, destinationId, title);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });
  });

  describe('updateLegend', () => {
    const options = [
      { selected: false, text: 'not selected' },
      { selected: true, text: 'selected' }
    ]
    const selectElement = jasmine.createSpyObj('selectElement', ['options']);
    selectElement.options = options;
    const destinationDiv = jasmine.createSpyObj('destinationDiv', ['children', 'append']);
    const children = jasmine.createSpyObj('remove', ['remove']);
    const data = jasmine.createSpyObj('data', ['append']);

    beforeEach(() => {
      jQuery.withArgs(`#${destinationId}`).and.returnValue(destinationDiv)
            .withArgs(document.createElement("ul")).and.returnValue(data)
      destinationDiv.children.and.returnValue(children);
      subject.update(selectElement);
    });

    it('is expected to get the expected output element', () => {
      expect(jQuery).toHaveBeenCalledWith(`#${destinationId}`);
    });

    it('is expected to get the newly created legend object', () => {
      expect(jQuery).toHaveBeenCalledWith(document.createElement("ul"));
    });

    it('is expected to delete previous legend data', () => {
      expect(children.remove).toHaveBeenCalled();
    });

    it('is expected to append the selected item', () => {
      expect(data.append).toHaveBeenCalledWith('<li>selected</li>');
    });

    it('is expected to append the legend to the right div', () => {
      expect(destinationDiv.append).toHaveBeenCalledWith(`<h6 class="pt-2">${title}</h6>`, data);
    });
  });
});
