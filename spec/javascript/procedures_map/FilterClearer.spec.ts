import { FilterClearer } from '../../../app/javascript/packs/procedures_map/FilterClearer';

describe('FilterClearer', () => {
  const jQuery = jasmine.createSpy('jQuery');
  const proceduresMap = jasmine.createSpyObj('proceduresMap', ['clearProcedures']);
  const clearFiltersButton = jasmine.createSpyObj('clearFiltersButton', ['click']);
  const filtersAccordion = jasmine.createSpyObj('filtersAccordion', ['find']);
  const element = jasmine.createSpyObj('element', ['val', 'change', 'each']);

  let subject: FilterClearer;

  beforeEach(() => {
    jQuery.withArgs('#clear-filters-button').and.returnValue(clearFiltersButton);
    jQuery.withArgs('#filters-accordion').and.returnValue(filtersAccordion);
    filtersAccordion.find.and.returnValue(element);

    subject = new FilterClearer(jQuery, proceduresMap);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });

    it('is expected to retrieve the clearFiltersButton element', () => {
      expect(jQuery).toHaveBeenCalledWith('#clear-filters-button');
    });

    it('is expected to bind the the click event to the clearFiltersButton element', () => {
      expect(clearFiltersButton.click).toHaveBeenCalledWith(jasmine.any(Function));
    });
  });

  describe('clearFilters', () => {
    beforeEach(() => {
      element.val.and.returnValue(element);
      subject.clearFilters();
    });

    it('is expected to retrieve the filtersAccordion element', () => {
      expect(jQuery).toHaveBeenCalledWith('#filters-accordion');
    });

    it('is expected to clear the select elements', () => {
      expect(filtersAccordion.find).toHaveBeenCalledWith('select[multiple = multiple]');
      expect(element.val).toHaveBeenCalledWith([]);
      expect(element.change).toHaveBeenCalled();
    });

    it('is expected to clear the datepicker elements', () => {
      expect(filtersAccordion.find).toHaveBeenCalledWith('input.datepicker');
      expect(element.each).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('is expected to clear the datepicker elements', () => {
      expect(filtersAccordion.find).toHaveBeenCalledWith('input[data-provide = slider]');
      expect(element.each).toHaveBeenCalledWith(jasmine.any(Function));
    });

    it('is expected to clear the procedures map', () => {
      expect(proceduresMap.clearProcedures).toHaveBeenCalled();
    });
  });
});
