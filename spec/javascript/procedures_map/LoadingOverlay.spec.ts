import { LoadingOverlay } from '../../../app/javascript/packs/procedures_map/LoadingOverlay';

describe('LoadingOverlay', () => {
  const jQuery = jasmine.createSpy('jQuery');
  const loadingOverlay = jasmine.createSpyObj('loadingOverlay', ['modal']);

  let subject: LoadingOverlay;

  beforeEach(() => {
    jQuery.withArgs('#loading-overlay').and.returnValue(loadingOverlay);

    subject = new LoadingOverlay(jQuery);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });
  });

  describe('showOverlay', () => {
    beforeEach(() => {
      subject.showOverlay();
    });

    it('is expected to retrieve the loadingOverlay element', () => {
      expect(jQuery).toHaveBeenCalledWith('#loading-overlay');
    });

    it('is expected to show the loadingOverlay element', () => {
      expect(loadingOverlay.modal).toHaveBeenCalledWith('show');
    });
  });

  describe('showOverlayWithCallback', () => {
    const callback = jasmine.createSpy('callback');
    let setTimeout;

    beforeEach(() => {
      spyOn(subject, 'showOverlay');
      setTimeout = spyOn(window, 'setTimeout');

      subject.showOverlayWithCallback(callback);
    });

    it('is expected to call function to show the overlay', () => {
      expect(subject.showOverlay).toHaveBeenCalled();
    });

    it('is expected to set a timer to call the callback', () => {
      expect(window.setTimeout).toHaveBeenCalledWith(callback, 100);
    });

    it('is expected to call the callback after the time set', () => {
      setTimeout.calls.argsFor(0)[0]();
      expect(callback).toHaveBeenCalled();
    });
  });


  describe('hideOverlay', () => {
    beforeEach(() => {
      subject.hideOverlay();
    });

    it('is expected to retrieve the loadingOverlay element', () => {
      expect(jQuery).toHaveBeenCalledWith('#loading-overlay');
    });

    it('is expected to hide the loadingOverlay element', () => {
      expect(loadingOverlay.modal).toHaveBeenCalledWith('hide');
    });
  });
});
