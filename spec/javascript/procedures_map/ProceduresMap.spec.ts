import * as jQuery from 'jquery';

import { ProceduresMap } from '../../../app/javascript/packs/procedures_map/ProceduresMap';

describe('ProceduresMap', () => {
  const divId = 'map';
  const map = jasmine.createSpyObj('map', ['invalidateSize', 'addLayer', 'hasLayer', 'removeLayer', 'getZoom', 'on']);
  const tileLayer = jasmine.createSpyObj('tileLayer', ['addTo']);
  const markerClusterGroup = jasmine.createSpyObj('markerClusterGroup',
    ['addLayer', 'getLayers', 'removeLayers']);
  const hfIcon = jasmine.createSpy('Icon');
  const marker = jasmine.createSpyObj('Marker', ['addTo', 'bindPopup']);
  const heatmapInternal = jasmine.createSpyObj('_heatmap',
                                               ['configure'],
                                               {_store: {_max: 1},
                                                 _config: {
                                                 gradient: {
                                                   0.25: "#2bffd3",
                                                   0.62: "#fffd57",
                                                    1.0: "#f93434"
                                                 }
                                               }})
  const heatmapLayer = jasmine.createSpyObj('heatmapLayer',
                                            ['setData', '_update'],
                                            {
                                              _heatmap: heatmapInternal,
                                              cfg: {}
                                            });
  const canvas = jasmine.createSpy('canvas');
  const geoJSONLayer = jasmine.createSpy('geoJSONLayer');
  const layerGroup = jasmine.createSpyObj('layerGroup', ['clearLayers', 'eachLayer']);
  const circle = jasmine.createSpyObj('Circle', ['addTo', 'bindTooltip']);
  const circleMarker = jasmine.createSpyObj('CircleMarker', ['addTo', 'bindPopup']);
  const leafletControl = jasmine.createSpyObj('control', {'minimap': tileLayer})
  const HeatmapOverlay = jasmine.createSpyObj('HeatmapOverlay', {'call_method': heatmapLayer});
  const Leaflet = jasmine.createSpyObj('L', {
    'map': map,
    'tileLayer': tileLayer,
    'markerClusterGroup': markerClusterGroup,
    'icon': hfIcon,
    'marker': marker,
    'heatLayer': heatmapLayer,
    'geoJSON': geoJSONLayer,
    'layerGroup': layerGroup,
    'circle': circle,
    'circleMarker': circleMarker,
    'canvas': canvas
  });
  const loadingOverlay = jasmine.createSpyObj('loadingOverlay', ['showOverlayWithCallback', 'hideOverlay']);
  Leaflet.control = leafletControl;
  const mapCenter = [-23.557296, -46.669211];
  const errorMessage = 'error';
  const percentileTranslation = 'percentile';
  const radiusTranslation = 'radius';

  const lat = -14.235004;
  const lng = -51.92528;

  let subject: ProceduresMap;

  beforeEach(() => {
    HeatmapOverlay.call_method.calls.reset();
    subject = new ProceduresMap(divId, Leaflet, jQuery, loadingOverlay, HeatmapOverlay.call_method, mapCenter, errorMessage, percentileTranslation, radiusTranslation);
  });

  describe('constructor', () => {
    it('is expected to create', () => {
      expect(subject).toBeTruthy();
    });

    it('is expected to create a Leaflet map', () => {
      expect(Leaflet.map).toHaveBeenCalledWith(
        divId,
        {
          center: [-23.557296, -46.669211],
          zoom: 11,
          maxZoom: 18
        }
      );
    });

    it('is expected to create the tileLayer', () => {
      expect(Leaflet.tileLayer).toHaveBeenCalledWith(
        'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
        { attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}
      );
    });

    it('is expected to add the tileLayer to the map', () => {
      expect(tileLayer.addTo).toHaveBeenCalledWith(map);
    });

    it('is expected to create the minimap', () => {
      expect(leafletControl.minimap).toHaveBeenCalledWith(tileLayer, { toggleDisplay: true });
    });

    it('is expected to set the patients_data_cluster with a markerClusterGroup', () => {
      expect(Leaflet.markerClusterGroup).toHaveBeenCalled();
    });

    it('is expected to create the heatmap layers', () => {
      expect(HeatmapOverlay.call_method).toHaveBeenCalledWith({
        "scaleRadius": true,
        "radius": 0.02,
        "maxOpacity": 0.8,
        "useLocalExtrema": true,
        gradient: subject.heatmapGradients.default,
        "valueField": 'value',
        onExtremaChange: jasmine.anything()
      });

      expect(HeatmapOverlay.call_method).toHaveBeenCalledTimes(2);
    });

    it('is expected to create layer groups for health facilities and its circles', () => {
      expect(Leaflet.layerGroup).toHaveBeenCalledWith([]);
    });
  });

  describe('heatmapListenerFor', () => {
    const element = jasmine.createSpyObj('element', ['text'])
    const idPrefix = 'id';

    beforeEach(() => {
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init').and.returnValues(element)
    })

    it('is expected to return a function', () => {
      expect(typeof subject.heatmapListenerFor(idPrefix)).toEqual('function');
    });

    it('is expected to return a method to update legend text', () => {
      const event = {max: 3};

      subject.heatmapListenerFor(idPrefix)(event);

      // @ts-ignore: TS2345
      expect(jQuery.fn.init).toHaveBeenCalledWith('#id-max', undefined);
      expect(element.text).toHaveBeenCalledWith(event.max.toFixed(2));
    })
  });

  describe('clear', ()=> {
    beforeEach(() => {
      layerGroup.clearLayers.calls.reset();
      spyOn(subject, 'clearProcedures');
      subject.clear();
    });

    it('is expected to clear the procedures and heatmap layers', () => {
      expect(subject.clearProcedures).toHaveBeenCalled();
    });

    it('is expected to clear the health facilities layer', () => {
      expect(layerGroup.clearLayers).toHaveBeenCalled();
    });
  });

  describe('clearProcedures', () => {
    const clusterLayers = {}
    beforeEach(() => {
      heatmapLayer.setData.calls.reset();
      layerGroup.clearLayers.calls.reset();
      markerClusterGroup.getLayers.and.returnValue(clusterLayers);
      subject.clearProcedures();
    });

    it('is expected to remove the patient cluster layers', () => {
      expect(markerClusterGroup.removeLayers).toHaveBeenCalledWith(clusterLayers);
    });

    it('is expected to reset the heatmap data', () => {
      expect(heatmapLayer.setData).toHaveBeenCalledWith({data: []})
      expect(heatmapLayer.setData).toHaveBeenCalledTimes(2)
    });

    it('is expected to clear the health facilities percentiles layer', () => {
      expect(layerGroup.clearLayers).toHaveBeenCalled();
    });
  });

  describe('currentHeatmapLayer', () => {
    describe('when the procedures heatmap is off', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
      });

      it('is expected to return an empty string', () => {
        expect(subject.currentHeatmapLayer()).toEqual('')
      });
    });

    describe('when the procedures heatmap is on', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);
      });

      it('is expected to return an empty string', () => {
        expect(subject.currentHeatmapLayer()).toEqual('procedures:1')
      });

    });

    describe('when the procedures heatmap is on', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValues(false, true);
      });

      it('is expected to return an empty string', () => {
        expect(subject.currentHeatmapLayer()).toEqual('csw:1.00')
      });
    });
  });

  describe('initializeHeatmapLayersBackend', () => {
    const layer = jasmine.createSpyObj('layer', ['bringToFront']);
    const layerArray = [ layer, layer, layer ];

    beforeEach(() => {
      layerGroup.eachLayer.and.callFake((fun) => {
        layerArray.forEach(fun);
      });
      subject.moveFacilitiesToFront();
    });

    it('is expected to move every facility to front', () => {
      expect(layer.bringToFront).toHaveBeenCalledTimes(3);
    });
  });

  describe('populateHealthFacilities', () => {
    const data = {
      facilities_data_array: [['image_url', lat, lng, 0.25, 10, [[75, 100]]]],
      facilities_procedures_range: [1, 99],
    };

    beforeEach(() => {
      spyOn(subject, 'addHealthFacility');
      spyOn(subject, 'addHealthFacilityPercentile');
      spyOn(subject, 'updateFacilitiesSizesLegend');
      spyOn(subject, 'moveFacilitiesToFront');

      subject.populateHealthFacilities(data);
    });

    it('is expected to call addHealthFacility for the data point', () => {
      expect(subject.addHealthFacility).toHaveBeenCalledWith('image_url', lat, lng, 0.25, 10)
    });

    it('is expected to call addHealthFacilityPercentile for the data point', () => {
      expect(subject.addHealthFacilityPercentile).toHaveBeenCalledWith(lat, lng, 75, 100)
    });

    it('is expected to call updateFacilitiesSizesLegend', () => {
      expect(subject.updateFacilitiesSizesLegend).toHaveBeenCalledWith(data);
    });

    it('is expected to move facilities to front', () => {
      expect(subject.moveFacilitiesToFront).toHaveBeenCalled();
    });
  });

  describe('populateDivisionsRates', () => {
    const data = [[lat, lng, 0.75]]

    beforeEach(() => {
      spyOn(subject, 'createCSWHMarker');

      subject.populateDivisionsRates(data);
    });

    it('is expected to call createCSWHMarker for the data point', () => {
      expect(subject.createCSWHMarker).toHaveBeenCalledWith(lat, lng, 0.75);
    });
  });

  describe('addHealthFacility', () => {
    const administration = 'ESTADUAL';
    const latitude = 1.0;
    const longitude = 2.0;
    const hospitalizationRate = 0.2;
    const facilityId = 10;
    const iconSize = 40;

    beforeEach(() => {
      subject.addHealthFacility(administration, latitude, longitude, hospitalizationRate, facilityId);
    });

    it('is expected to create a circle marker for the facility', () => {
      expect(Leaflet.circleMarker).toHaveBeenCalledWith(
        [
          latitude,
          longitude,
        ],
        {
          radius: Math.max(Math.sqrt(hospitalizationRate * 15000), 5),
          stroke: false,
          color: subject.healthFacilityColors[administration],
          fillOpacity: 0.6,
        }
      );
    });

    it('is expected to add the circle marker to the map', () => {
      expect(circleMarker.addTo).toHaveBeenCalledWith(layerGroup);
    });

    it('is expected to bind a popup to the circle marker', () => {
      expect(circleMarker.bindPopup).toHaveBeenCalled();
    });
  });

  describe('setPopupContent', () => {
    const element = jasmine.createSpyObj('element', ['html']);
    const content = 'Very interesting';

    describe('with elements that implement the update function', () => {
      const popup = jasmine.createSpyObj('popup', ['update']);

      beforeEach(() => {
        subject.setPopupContent(element, popup, content);
      });

      it('is expected to set the element html', () => {
        expect(element.html).toHaveBeenCalledWith(content);
      });

      it('is expected to update the popup', () => {
        expect(popup.update).toHaveBeenCalled();
      });
    });

    describe('with elements that does not implement the update function', () => {
      const layer = jasmine.createSpy('layer');

      beforeEach(() => {
        subject.setPopupContent(element, layer, content);
      });

      it('is expected to set the element html', () => {
        expect(element.html).toHaveBeenCalledWith(content);
      });
    });
  });

  describe('getPopupContent', () => {
    const url = 'localhost';
    const popup = jasmine.createSpyObj('popup', ['update']);

    const elementObj = jasmine.createSpyObj('element', ['css'])
    const element = jasmine.createSpy('element');
    const doneObj = jasmine.createSpyObj('doneObj', ['fail']);
    const ajax = jasmine.createSpyObj('ajax', { 'done': doneObj });

    let result: any;

    beforeEach(() => {
      elementObj[0] = element;
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init').and.returnValue(elementObj);
      // I'm aware that this does not matches the jqXHR interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery, 'ajax').and.returnValue(ajax);

      result = subject.getPopupContent(url, popup, errorMessage);
    });

    it('is expected to set the width to 300px', () => {
      expect(elementObj.css).toHaveBeenCalledWith('width', '300px');
    });

    it('is expected to perform a Ajax request', () => {
      expect(jQuery.ajax).toHaveBeenCalledWith({
        url,
        dataType: 'html',
        data: ''
      });

      expect(ajax.done).toHaveBeenCalled();

      expect(doneObj.fail).toHaveBeenCalled();
    });

    it('is expected to return the element', () => {
      expect(result).toEqual(element);
    });
  });

  describe('createPatientsDataMarkers', () => {
    const patientsData = [
      [2.0, 3.0, [10, 30]],
      [3.0, 1.0, [20, 40]],
    ]

    beforeEach(() => {
      spyOn(subject, 'addPointToHeatmapPreData');
      subject.createPatientsDataMarkers(patientsData);
    });

    it('is expected to create a Leaflet marker', () => {
      expect(Leaflet.marker).toHaveBeenCalledWith([patientsData[0][0], patientsData[0][1]]);
      expect(Leaflet.marker).toHaveBeenCalledWith([patientsData[1][0], patientsData[1][1]]);
    });

    it('is expected to bind a popup to the marker', () => {
      expect(marker.bindPopup).toHaveBeenCalled();
    });

    it('is expected to add the latlong to the heatmap pre data', () => {
      expect(subject.addPointToHeatmapPreData).toHaveBeenCalledTimes(4);
    });
  });

  describe('addPointToHeatmapPreData', () => {
    const latitude = 2.0;
    const longitude = 3.0;

    it('is expected to add the coordinates to the pre data struct', () => {
      expect(Object.keys(subject.heatmapPreData).length).toEqual(0);
      subject.addPointToHeatmapPreData(latitude, longitude);
      expect(Object.keys(subject.heatmapPreData).length).toEqual(1);
      expect(subject.heatmapPreData[latitude][longitude]).toEqual(1);
    })

    it('is expected to work as a counter to coordinate points', () => {
      subject.addPointToHeatmapPreData(latitude, longitude);
      subject.addPointToHeatmapPreData(latitude, longitude);
      expect(subject.heatmapPreData[latitude][longitude]).toEqual(2);
    })
  })

  describe('populateHeatmapData', () => {
    const preData = {}
    const lat1 = 0.23;
    const lng1 = 0.24;
    const lng2 = 0.25;
    const value1 = 5;
    const value2 = 7;

    beforeEach(() => {
      preData[lat1] = {};
      preData[lat1][lng1] = value1;
      preData[lat1][lng2] = value2;

      subject.heatmapPreData = preData;

      expect(subject.heatmapData.data.length).toEqual(0);
      subject.populateHeatmapData();
    });

    it('is expected to populate the heatmap data', () => {
      expect(subject.heatmapData.data.length).toEqual(2);
    });

    it('is expected to set the coordinates in the right structure' , () => {
      expect(subject.heatmapData.data[0].lat).toEqual(lat1);
      expect(subject.heatmapData.data[0].lng).toEqual(lng1);
      expect(subject.heatmapData.data[0].value).toEqual(value1);

      expect(subject.heatmapData.data[1].lat).toEqual(lat1);
      expect(subject.heatmapData.data[1].lng).toEqual(lng2);
      expect(subject.heatmapData.data[1].value).toEqual(value2);
    });
  });

  describe('populateSearchData', () => {
    const data = {
      health_facilities: [],
      distance_percentiles: [],
      patients_data: [],
      divisions_rates: [],
    };

    beforeEach(() => {
      spyOn(subject, 'populateHealthFacilities');
      spyOn(subject, 'createPatientsDataMarkers');
      spyOn(subject, 'populateDivisionsRates');

      subject.populateSearchData(data);
    });

    it('is expected to populate the health facilities', () => {
      expect(subject.populateHealthFacilities).toHaveBeenCalledWith(data.health_facilities);
    });

    it('is expected to populate the patient data', () => {
      expect(subject.createPatientsDataMarkers).toHaveBeenCalledWith(data.patients_data);
    });

    it('is expected to populate the divisions rates data', () => {
      expect(subject.populateDivisionsRates).toHaveBeenCalledWith(data.divisions_rates);
    });
  });

  describe('reloadWithData', () => {
    const data = {
      health_facilities: [],
      distance_percentiles: [],
      patients_data: [],
      divisions_rates: [],
    };

    beforeEach(() => {
      spyOn(subject, 'clear');
      spyOn(subject, 'populateSearchData');
      spyOn(subject, 'initializeHeatmapLayers');
      spyOn(subject, 'applyPatientCluster');

      subject.reloadWithData(data);
    });

    it('is expected to clear the map data', () => {
      expect(subject.clear).toHaveBeenCalled();
    });

    it('is expected to populate the new data', () => {
      expect(subject.populateSearchData).toHaveBeenCalledWith(data);
    });

    it('is expected to apply the heatmap data', () => {
      expect(subject.initializeHeatmapLayers).toHaveBeenCalled();
    });

    it('is expected to apply the patient data', () => {
      expect(subject.applyPatientCluster).toHaveBeenCalled();
    });
  });

  describe('initializeHeatmapLayers', () => {
    beforeEach(() => {
      heatmapLayer.setData.calls.reset();
      spyOn(subject, 'populateHeatmapData');
      spyOn(subject, 'initializeHeatmapLayersBackend');

      subject.initializeHeatmapLayers();
    });

    it('is expected to populate the heatmapData', () => {
      expect(subject.populateHeatmapData).toHaveBeenCalled();
    });

    it('is expected to set the data for the heatmaps', () => {
      expect(heatmapLayer.setData).toHaveBeenCalledTimes(2);
    });
  });

  describe('invalidateSize', () => {
    beforeEach(() => {
      subject.invalidateSize();
    });

    it('is expected to forward the call to the Leaflet map', () => {
      expect(map.invalidateSize).toHaveBeenCalled();
    });
  });

  describe('generateClusterLayer', () => {
    describe('dummy', () => {
      const markers = [1, 2, 3];
      const radius = 40;

      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
        Leaflet.markerClusterGroup.and.returnValue(markerClusterGroup);
        markerClusterGroup.addLayer.calls.reset();
      });

      it('is expected to set a radius for the new cluster', () => {
        subject.generateClusterLayer(markers, radius);
        expect(Leaflet.markerClusterGroup).toHaveBeenCalledWith({maxClusterRadius: radius});
      });

      it('is expected to populate the cluster with markers', () => {
        subject.generateClusterLayer(markers, radius);
        expect(markerClusterGroup.addLayer).toHaveBeenCalledTimes(markers.length);
      });

      it('is expected to return a cluster layer', () => {
        const result = subject.generateClusterLayer(markers, radius);
        expect(result).toEqual(markerClusterGroup);
      });
    });
  });

  describe('toggleCluster', () => {
    describe('when the cluster is not a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
        spyOn(subject, 'toggleLegendDiv');

        subject.toggleCluster();
      });

      it('is expected to add the marker cluster group as a map layer and update the map options legend', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('cluster');
        expect(map.hasLayer).toHaveBeenCalledWith(markerClusterGroup);
        expect(map.addLayer).toHaveBeenCalledWith(markerClusterGroup);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);
        spyOn(subject, 'toggleLegendDiv');

        subject.toggleCluster();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('cluster');
        expect(map.hasLayer).toHaveBeenCalledWith(markerClusterGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(markerClusterGroup);
      });
    });
  });

  describe('kilometresToPixels', () => {
    beforeEach(() => {
      map.getZoom.and.returnValue(11);
    });

    it('is expected to depend on the map zoom level', () => {
      subject.kilometresToPixels(1)
      expect(map.getZoom).toHaveBeenCalled();
    });

    it('is expected to return numbers', () => {
      expect(subject.kilometresToPixels(0)).toEqual(0);
      expect(subject.kilometresToPixels(10)).toEqual(143);
    });
  });

  describe('applyPatientCluster', () => {
    const defaultRadius = 80;
    const radius = 40;

    beforeEach(() => {
      spyOn(subject, 'generateClusterLayer').and.returnValue(markerClusterGroup);
      spyOn(subject, 'toggleCluster');
    });

    it('is expected to generate the data cluster', () => {
      subject.applyPatientCluster(radius);
      expect(subject.generateClusterLayer).toHaveBeenCalledWith([], radius);
    });

    it('is expected to have a default value for the radius', () => {
      subject.applyPatientCluster();
      expect(subject.generateClusterLayer).toHaveBeenCalledWith([], defaultRadius);
    });

    describe('when the cluster is visible', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);
        subject.applyPatientCluster();
      });

      it('is expected to toggle the cluster on and off', () => {
        expect(subject.toggleCluster).toHaveBeenCalledTimes(2);
      });
    });

    describe('when the cluster is not visible', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);
        subject.applyPatientCluster();
      });

      it('is expected to not not call toggleCluster', () => {
        expect(subject.toggleCluster).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('clusterRadiusEvent', () => {
    const input = { value: 10 };
    const finalRadius = 20;

    beforeEach(() => {
      spyOn(subject, 'setLegendValue');
      spyOn(subject, 'kilometresToPixels').and.returnValue(finalRadius);
      spyOn(subject, 'applyPatientCluster');
      subject.clusterRadiusEvent(input);
    });

    it('is expected to set the legend cluster radius value', () => {
      expect(subject.setLegendValue).toHaveBeenCalledWith('cluster-radius', input.value);
    });

    it('is expected to convert the input value to pixels', () => {
      expect(subject.kilometresToPixels).toHaveBeenCalledWith(input.value);
    });

    it('is expected to apply the new value to the cluster', () => {
      expect(subject.applyPatientCluster).toHaveBeenCalledWith(finalRadius);
    });
  });

  describe('toggleHeatmap', () => {
    describe('when the heatmap is not a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        spyOn(subject, 'toggleDiv');
        map.hasLayer.and.returnValue(false);

        subject.toggleHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-by-count');
        expect(subject.toggleDiv).toHaveBeenCalledWith('procedures-heatmap-legend');
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.addLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        spyOn(subject, 'toggleDiv');
        map.hasLayer.and.returnValue(true);

        subject.toggleHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-by-count');
        expect(subject.toggleDiv).toHaveBeenCalledWith('procedures-heatmap-legend');
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.removeLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });
  });

  describe('setDataSearchCallbacks', () => {
    const element = jasmine.createSpyObj('element', ['on']);
    const id = '#test';

    it('is expected to set callbacks for ajax events', () => {
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      subject.setDataSearchCallbacks(id);

      // @ts-ignore: TS2345
      expect(jQuery.fn.init).toHaveBeenCalledWith(id, undefined);
      expect(element.on).toHaveBeenCalledWith("ajax:success", jasmine.any(Function));
      expect(element.on).toHaveBeenCalledWith("ajax:error", jasmine.any(Function));
    });
  });

  describe('dataSearchSuccess', () => {
    const event = {
      detail: [null, 'OK', { responseText: `{"health_facilities": [],
                                             "patients_data": [],
                                             "divisions_rates": []}`}]
    };

    beforeEach(() => {
      spyOn(subject, 'reloadWithData');

      subject.dataSearchSuccess(event);
    });

    it('is expected to reload the map with response data', () => {
      expect(subject.reloadWithData).toHaveBeenCalledWith({health_facilities: [],
                                                           patients_data: [],
                                                           divisions_rates: []});
    });

    it('is expected to hide the overlay', () => {
      expect(loadingOverlay.hideOverlay).toHaveBeenCalled();
    });
  });

  describe('dataSearchError', () => {
    beforeEach(() => {
      spyOn(window, 'alert');

      subject.dataSearchError();
    });

    it('is expected to show an error alert', () => {
      expect(window.alert).toHaveBeenCalledWith(errorMessage);
    });

    it('is expected to hide the overlay', () => {
      expect(loadingOverlay.hideOverlay).toHaveBeenCalled();
    });
  });

  describe('heatmapRadiusEvent', () => {
    const value = '10';
    const slider = { value };

    beforeEach(() => {
      spyOn(subject, 'setLegendValue');
      spyOn(subject, 'kilometresToPixels').and.returnValue(Number(value));
      heatmapLayer._update.calls.reset();
    });

    it('is expected to set the radius to the slider value', () => {
      subject.heatmapRadiusEvent(slider);
      expect(subject.setLegendValue).toHaveBeenCalledWith('heatmap-radius', value);
      expect(heatmapLayer.cfg.radius).toEqual(0.01 * Number(value));
    });

    it('is expected to update the layer if needed', () => {
      map.hasLayer.and.returnValue(true);
      subject.heatmapRadiusEvent(slider);

      expect(heatmapLayer._update).toHaveBeenCalledTimes(2)
    });

    it('is expected not to update the layer if not needed', () => {
      map.hasLayer.and.returnValue(false);
      subject.heatmapRadiusEvent(slider);

      expect(heatmapLayer._update).not.toHaveBeenCalled();
    });
  });

  describe('toggleHeatmapHighContrast', () => {
    const element = jasmine.createSpyObj('element', ['css']);

    it('is expected to set the heatmap layer gradient', () => {
      spyOn(subject, 'toggleLegendDiv');
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);

      const gradients = subject.heatmapGradients;

      // Turns on
      subject.toggleHeatmapHighContrast();
      expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-high-contrast');
      expect(heatmapLayer._heatmap.configure).toHaveBeenCalledWith({
        gradient: gradients.highContrast
      });

      // Turns off
      heatmapInternal._config.gradient = {
        0.25: "#D3C9F8",
        0.55: "#7B5CEB",
        0.85: "#4E25E4",
         1.0: "#3816B3"
      }
      subject.toggleHeatmapHighContrast();
      expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-high-contrast');
      expect(heatmapLayer._heatmap.configure).toHaveBeenCalledWith({
        gradient: gradients.default
      });

      expect(heatmapInternal.configure).toHaveBeenCalledTimes(4);

      // @ts-ignore: TS2345
      expect(jQuery.fn.init).toHaveBeenCalledWith('.heatmap-legend', undefined);
      expect(element.css).toHaveBeenCalledWith('background-image', jasmine.anything());
    });
  });

  describe('heatmapGradientCSS', () => {
    beforeEach(() => {
      spyOn(subject, 'gradientToString').and.returnValue('gradient-string')
    });

    it('is expected to return a string', () => {
      expect(subject.heatmapGradientCSS()).toEqual(
        'linear-gradient(to right, gradient-string)'
      );
    });
  })

  describe('gradientToString', () => {
    const input = {0: "#aaa", 0.25: "#faa", 1: "#ffa"};
    const output = '#aaa 0%, #faa 25%, #ffa 100%';

    it('is expected to represent the gradient as css gradient string', () => {
      expect(subject.gradientToString(input)).toEqual(output);
    })
  });

  describe('heatmapOpacityEvent', () => {
    const slider = { value: "10" };
    const element = jasmine.createSpyObj('element', ['css']);

    beforeEach(() => {
      // I'm aware that this does not matches the jqXHR interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      spyOn(subject, 'setLegendValue');

      subject.heatmapOpacityEvent(slider);
    });

    it('is expected to set the minOpacity to the slider value', () => {
      expect(subject.setLegendValue).toHaveBeenCalledWith('heatmap-opacity', slider.value);
      // I'm ignoring this in order to be able to test the parameter passed to jQuery
      // @ts-ignore: TS2345
      expect(jQuery.fn.init).toHaveBeenCalledWith('.heatmap-canvas', undefined);
      expect(element.css).toHaveBeenCalledWith('opacity', slider.value);
    });
  });

  describe('buildLimitsLayer', () => {
    const geoJSON = jasmine.createSpy('geoJSON');

    describe('FamilyHealthStrategy identifier', () => {
      const identifier = 'FamilyHealthStrategy';
      const style = {
        'color': '#444444',
        'opacity': 0.6,
        'stroke': true,
        'fill': true,
        'fillOpacity': 0.2,
        'fillColor': '#9B1D03'
      };

      beforeEach(() => {
        subject.buildLimitsLayer(identifier, geoJSON);
      });

      it('is expected to build it using Leaflet', () => {
        expect(Leaflet.geoJSON).toHaveBeenCalledWith(
          geoJSON,
          { style, renderer: canvas, onEachFeature: jasmine.any(Function) }
        );
      });
    });

    describe('regular identifiers', () => {
      const identifier = 'identifier';
      const style = {
        'color': '#444444',
        'opacity': 0.6,
        'stroke': true,
        'fill': true,
        'fillOpacity': 0.05,
      };

      beforeEach(() => {
        subject.buildLimitsLayer(identifier, geoJSON);
      });

      it('is expected to build it using Leaflet', () => {
        expect(Leaflet.geoJSON).toHaveBeenCalledWith(
          geoJSON,
          { style, renderer: canvas, onEachFeature: jasmine.any(Function) }
        );
      });
    });
  });

  describe('addPopupToLayer', () => {
    const layer = jasmine.createSpyObj('layer', ['bindPopup']);

    beforeEach(() => {
      subject.addPopupToLayer(layer);
    });

    it('is expected to bind html content to the layer', () => {
      expect(layer.bindPopup).toHaveBeenCalledWith(jasmine.any(Function));
    });
  });

  describe('toggleLimits', () => {
    const identifier = 'identifier';

    describe('when there is no layer for the identifier', () => {
      beforeEach(() => {
        // @ts-ignore: TS2341
        subject.limitsLayers = {};

        spyOn(subject, 'toggleLimitsLayer');
        spyOn(subject, 'getGeoJsonData');

        subject.toggleLimits(identifier);
      });

      it('is expected to not try to toggle the limits layer', () => {
        expect(subject.toggleLimitsLayer).not.toHaveBeenCalled();
      });

      it('is expected to download the geoJSON data', () => {
        expect(subject.getGeoJsonData).toHaveBeenCalled();
      });
    });

    describe('when there is a layer', () => {
      const limitsLayer = jasmine.createSpy('limitsLayer');

      beforeEach(() => {
        // @ts-ignore: TS2341
        subject.limitsLayers[identifier] = limitsLayer;

        spyOn(subject, 'toggleLimitsLayer');
        spyOn(subject, 'getGeoJsonData');

        subject.toggleLimits(identifier);
      });

      it('is expected to toggle the limits layer', () => {
        expect(subject.toggleLimitsLayer).toHaveBeenCalled();
      });

      it('is expected to not try to download the geoJSON data', () => {
        expect(subject.getGeoJsonData).not.toHaveBeenCalled();
      });
    });
  });

  describe('toggleLimitsWithOverlay', () => {
    const identifier = 'identifier';

    beforeEach(() => {
      subject.toggleLimitsWithOverlay(identifier);
    });

    it('is expected to call the function to show the overlay with a callback', () => {
      expect(loadingOverlay.showOverlayWithCallback).toHaveBeenCalledWith(jasmine.any(Function));
    });
  });

  describe('getGeoJsonData', () => {
    const ajax = jasmine.createSpyObj('ajax', ['done']);

    beforeEach(() => {
      // I'm aware that this does not matches the jquery interface, but for test purposes I'm ignoring
      // @ts-ignore: TS2345
      spyOn(jQuery, 'get').and.returnValue(ajax);
      subject.getGeoJsonData('subprefectures');
    });

    it('is expected to fetch the map via AJAX', () => {
      expect(jQuery.get).toHaveBeenCalledWith('/demographic_divisions/geojson/subprefectures');
      expect(ajax.done).toHaveBeenCalled();
    });
  });

  describe('toggleLimitsLayer', () => {
    const identifier = 'identifier';
    const limitsLayer = jasmine.createSpy('limitsLayer');

    describe('and the map does not have the layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(false);

        spyOn(subject, 'toggleLegendDiv');

        subject.toggleLimitsLayer(identifier, limitsLayer);
      });

      it('is expected to add the layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith(`limits-${identifier}`);
        expect(map.hasLayer).toHaveBeenCalledWith(limitsLayer);
        expect(map.addLayer).toHaveBeenCalledWith(limitsLayer);
      });

      it('is expected to hide the loading overlay', () => {
        expect(loadingOverlay.hideOverlay).toHaveBeenCalled();
      });
    });

    describe('and the map already has the layer', () => {
      beforeEach(() => {
        map.hasLayer.and.returnValue(true);

        spyOn(subject, 'toggleLegendDiv');

        subject.toggleLimitsLayer(identifier, limitsLayer);
      });

      it('is expected to remove the layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith(`limits-${identifier}`);
        expect(map.hasLayer).toHaveBeenCalledWith(limitsLayer);
        expect(map.removeLayer).toHaveBeenCalledWith(limitsLayer);
      });

      it('is expected to hide the loading overlay', () => {
        expect(loadingOverlay.hideOverlay).toHaveBeenCalled();
      });
    });
  });

  describe('createCSWHMarker', () => {
    const latitude = 2.0;
    const longitude = 3.0;
    const rate = 0.42;

    beforeEach(() => {
      subject.createCSWHMarker(latitude, longitude, rate);
    });

    it('is expected to add the data to the heatmap data', () => {
      expect(subject.cSWHeatmapData.data[0].lat).toEqual(latitude);
      expect(subject.cSWHeatmapData.data[0].lng).toEqual(longitude);
      expect(subject.cSWHeatmapData.data[0].value).toEqual(rate);
    });
  });

  describe('toggleCSWHeatmap', () => {
    describe('when the heatmap is not a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        spyOn(subject, 'toggleDiv');
        map.hasLayer.and.returnValue(false);

        subject.toggleCSWHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-by-csw');
        expect(subject.toggleDiv).toHaveBeenCalledWith('csw-heatmap-legend');
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.addLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });

    describe('when the cluster already is a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        spyOn(subject, 'toggleDiv');
        map.hasLayer.and.returnValue(true);

        subject.toggleCSWHeatmap();
      });

      it('is expected to add the marker cluster group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('heatmap-by-csw');
        expect(subject.toggleDiv).toHaveBeenCalledWith('csw-heatmap-legend');
        expect(map.hasLayer).toHaveBeenCalledWith(heatmapLayer);
        expect(map.removeLayer).toHaveBeenCalledWith(heatmapLayer);
      });
    });
  });

  describe('toggleHealthFacilities', () => {
    describe('when the health facilities are not a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        map.hasLayer.and.returnValue(false);

        subject.toggleHealthFacilities();
      });

      it('is expected to add the health facilities group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('facilities-icons');
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.addLayer).toHaveBeenCalledWith(layerGroup);
      });
    });

    describe('when the health facilities already are a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        map.hasLayer.and.returnValue(true);

        subject.toggleHealthFacilities();
      });

      it('is expected to add the health facilities group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('facilities-icons');
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(layerGroup);
      });
    });
  });

  describe('addHealthFacilityPercentile', () => {
    const latitude = 12.0;
    const longitude = 13.0;
    const percentile = 25;
    const radius = 10.42;

    beforeEach(() => {
      subject.addHealthFacilityPercentile(latitude, longitude, percentile, radius);
    });

    it('is expected to create a circle', () => {
      expect(Leaflet.circle).toHaveBeenCalledWith(
        [latitude, longitude],
        {
          // @ts-ignore: TS2341
          color: subject.percentileColors[percentile],
          // @ts-ignore: TS2341
          fillColor: subject.percentileColors[percentile],
          fillOpacity: 0.2,
          radius: radius*1000,
        }
      );
    });

    it('is expected to bind a tooltip to the circle', () => {
      expect(circle.bindTooltip).toHaveBeenCalledWith(
        `<b>${percentileTranslation}</b>: ${percentile}%<br><b>${radiusTranslation}</b>: ${radius} Km`,
        {direction:'top'});
    });

    it('is expected to add the circle to the layer group', () => {
      expect(circle.addTo).toHaveBeenCalledWith(layerGroup);
    });
  });

  describe('toggleHealthFacilitiesPercentiles', () => {
    describe('when the health facilities are not a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        spyOn(subject, 'moveFacilitiesToFront');
        map.hasLayer.and.returnValue(false);

        subject.toggleHealthFacilitiesPercentiles();
      });

      it('is expected to add the health facilities percentiles group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('facilities-percentiles')
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.addLayer).toHaveBeenCalledWith(layerGroup);
        expect(subject.moveFacilitiesToFront).toHaveBeenCalled();
      });
    });

    describe('when the health facilities percentiles already are a map layer', () => {
      beforeEach(() => {
        spyOn(subject, 'toggleLegendDiv');
        map.hasLayer.and.returnValue(true);

        subject.toggleHealthFacilitiesPercentiles();
      });

      it('is expected to add the health facilities percentiles group as a map layer', () => {
        expect(subject.toggleLegendDiv).toHaveBeenCalledWith('facilities-percentiles')
        expect(map.hasLayer).toHaveBeenCalledWith(layerGroup);
        expect(map.removeLayer).toHaveBeenCalledWith(layerGroup);
      });
    });
  });

  describe('createInfoBox', () => {
    const content = 'html body';
    const box = jasmine.createSpyObj('box', ['addTo', 'onAdd']);

    beforeEach(() => {
      spyOn(Leaflet, 'control').and.returnValue(box);
      subject.createInfoBox(content);
    });

    it('is expected to create the legend and add it to the map', () => {
      expect(Leaflet.control).toHaveBeenCalled();
      expect(box.addTo).toHaveBeenCalledWith(map);
    });
  });

  describe('countLegendListVisibleItems', () => {
    const id = 'id';
    const element = jasmine.createSpyObj('element', ['filter']);
    const items = ['1', '2'];

    beforeEach(() => {
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      element.filter.and.returnValue(items);
    });

    it('is expected to gather all childs and filter the hidden ones', () => {
      const ret = subject.countLengendListVisibleItems(id);
      expect(element.filter).toHaveBeenCalled();
      expect(ret).toEqual(items.length);
    });
  });

  describe('getFacilitiesLegendDiameters', () => {
    describe('when there is no facility', () => {
      const hfDataArray = [];

      it('is expected to return an array with the correct radii', () => {
        const result = subject.getFacilitiesLegendDiameters(hfDataArray);

        const minDiameter = subject.minCircleMarkerRadius * 2;
        expect(result).toEqual([minDiameter, minDiameter, minDiameter]);
      });
    });

    describe('when there is one facility', () => {
      const hospRate = 0.5;
      const minHospRate = 0.1;
      const hfDataArray = [['adm', 'lat', 'long', hospRate, 1, []]];

      beforeEach(() => {
        spyOn(subject, 'getHospitalizationRateFromMarkerRadius')
          .withArgs(subject.minCircleMarkerRadius).and.returnValue(minHospRate);
        spyOn(subject, 'calculateFacilityDiameter').and.callFake((radius) => { return radius });
      });

      it('is expected to return an array with the correct radii', () => {
        const result = subject.getFacilitiesLegendDiameters(hfDataArray);

        const minDiameter = subject.minCircleMarkerRadius * 2;
        expect(result).toEqual([hospRate, (hospRate + minHospRate) / 2, minDiameter]);
        expect(subject.calculateFacilityDiameter).toHaveBeenCalledTimes(2);
      });
    });

    describe('when there are many facility', () => {
      const minHospRate = 0.1;
      const maxHospRate = 0.5;
      const hfDataArray = [['adm', 'lat', 'long', maxHospRate, 1, []], [], ['adm', 'lat', 'long', minHospRate, 2, []]];

      beforeEach(() => {
        spyOn(subject, 'calculateFacilityDiameter').and.callFake((radius) => { return radius });
      });

      it('is expected to return an array with the correct radii', () => {
        const result = subject.getFacilitiesLegendDiameters(hfDataArray);

        expect(result).toEqual([maxHospRate, (maxHospRate + minHospRate) / 2, minHospRate]);
        expect(subject.calculateFacilityDiameter).toHaveBeenCalledTimes(3);
      });
    });
  });

  describe('updateFacilitiesLegendMarkersSizes', () => {
    const hfDataArray = [];
    const facilitiesDiameters = [10, 20, 30];
    const element = jasmine.createSpyObj('element', ['css', 'each']);

    beforeEach(() => {
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      spyOn(subject, 'getFacilitiesLegendDiameters').and.returnValue(facilitiesDiameters);

      subject.updateFacilitiesLegendMarkersSizes(hfDataArray);
    });

    it('is expected to get the facilities diameters', () => {
      expect(subject.getFacilitiesLegendDiameters).toHaveBeenCalledWith(hfDataArray);
    });

    it('is expected to set the padded width of the facilities markers as 1.5x of the largest marker', () => {
      expect(element.css).toHaveBeenCalledWith('width', facilitiesDiameters[0] * 1.5);
    });

    it('is expected to set the size of each marker', () => {
      expect(element.each).toHaveBeenCalled();
    });
  });

  describe('getFacilitiesLegendProcedures', () => {
    describe('when there is no facility', () => {
      const hfData = {
        facilities_data_array: [],
        facilities_procedures_range: [],
      };

      it('is expected to return an array with the correct procedures count', () => {
        const result = subject.getFacilitiesLegendProcedures(hfData);
        expect(result).toEqual([0, 0, 0]);
      });
    });

    describe('when there is one facility', () => {
      const maxProcedures = 100;
      const hfData = {
        facilities_data_array: [[]],
        facilities_procedures_range: [maxProcedures, maxProcedures],
      };

      it('is expected to return an array with the correct procedures count', () => {
        const result = subject.getFacilitiesLegendProcedures(hfData);
        expect(result).toEqual([maxProcedures, maxProcedures / 2, 0]);
      });
    });

    describe('when there are many facility', () => {
      const minProcedures = 50;
      const maxProcedures = 100;
      const hfData = {
        facilities_data_array: [[], []],
        facilities_procedures_range: [minProcedures, maxProcedures],
      };

      it('is expected to return an array with the correct procedures count', () => {
        const result = subject.getFacilitiesLegendProcedures(hfData);
        expect(result).toEqual([maxProcedures, Math.floor((maxProcedures + minProcedures) / 2), minProcedures]);
      });
    });
  });

  describe('updateFacilitiesLegendMarkersLabels', () => {
    const hfData = [];
    const facilitiesProcedures = [];
    const element = jasmine.createSpyObj('element', ['each']);

    beforeEach(() => {
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      spyOn(subject, 'getFacilitiesLegendProcedures').and.returnValue(facilitiesProcedures);

      subject.updateFacilitiesLegendMarkersLabels(hfData);
    });

    it('is expected to set the label of each marker', () => {
      expect(element.each).toHaveBeenCalled();
    });
  });

  describe('updateFacilitiesSizesLegend', () => {
    const hfData = {
      facilities_data_array: [],
      facilities_procedures_range: []
    };

    beforeEach(() => {
      spyOn(subject, 'updateFacilitiesLegendMarkersSizes');
      spyOn(subject, 'updateFacilitiesLegendMarkersLabels');

      subject.updateFacilitiesSizesLegend(hfData);
    });

    it('is expected to call updateFacilitiesLegendMarkersSizes with the facilities data array', () => {
      expect(subject.updateFacilitiesLegendMarkersSizes).toHaveBeenCalledWith(hfData.facilities_data_array);
    });

    it('is expected to call updateFacilitiesLegendMarkersLabels with the facilities procedures range', () => {
      expect(subject.updateFacilitiesLegendMarkersLabels).toHaveBeenCalledWith(hfData);
    });
  });

  describe('updateLegend', () => {
    describe('when there aren\'t any items selected', () => {
      beforeEach(() => {
        spyOn(subject, 'countLengendListVisibleItems').and.returnValue(0);
        spyOn(jQuery.fn, 'hide');
      });

      it('is expected to hide the divs', () => {
        subject.updateLegend();
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('heatmap-by');
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('limits-list');
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('facilities-list');
        expect(jQuery().hide).toHaveBeenCalledTimes(3);
      })
    });

    describe('when there are items selected', () => {
      beforeEach(() => {
        spyOn(subject, 'countLengendListVisibleItems').and.returnValue(42);
        spyOn(jQuery.fn, 'show');
      });

      it('is expected to hide the divs', () => {
        subject.updateLegend();
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('heatmap-by');
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('limits-list');
        expect(subject.countLengendListVisibleItems).toHaveBeenCalledWith('facilities-list');
        expect(jQuery().show).toHaveBeenCalledTimes(3);
      })
    });
  });

  describe('toggleLegendDiv', () => {
    const id = 'id';

    beforeEach(() => {
      spyOn(jQuery.fn, 'toggle');
      spyOn(subject, 'updateLegend');
    });

    it('is expected to toggle the map options legend div', () => {
      subject.toggleLegendDiv(id);
      expect(jQuery(`#map-options-legend-${id}`).toggle).toHaveBeenCalled();
      expect(subject.updateLegend).toHaveBeenCalled();
    });
  });

  describe('toggleDiv', () => {
    const id = 'id';

    beforeEach(() => {
      spyOn(jQuery.fn, 'toggle');
    });

    it('is expected to toggle the map options legend div', () => {
      subject.toggleDiv(id);
      expect(jQuery(`#${id}`).toggle).toHaveBeenCalled();
    });
  });

  describe('setLegendValue', () => {
    const id = 'id';
    const value = 'value';
    const element = jasmine.createSpyObj('element', ['html']);

    beforeEach(() => {
      // @ts-ignore: TS2344
      spyOn(jQuery.fn, 'init').and.returnValue(element);
      subject.setLegendValue(id, value);
    });

    it('is expected to toggle the map options legend div', () => {
      expect(element.html).toHaveBeenCalledWith(value);
    });
  });
});
