import { AboutPage } from '../../../app/javascript/packs/static_pages/AboutPage'

describe('AboutPage', () => {

  let subject: AboutPage;

  beforeEach(() => {
    subject = new AboutPage();
  });

  describe('show_email', () => {
    describe('with an element with no neighbours', () => {
      it('is expected not to throw an error', () => {
        const element = document.createElement('div')
        subject.show_email(element);
      });
    });

    describe('with more than one neighbour', () => {
      let group;
      let element;
      let firstNeighbour;
      let secondNeighbour;

      beforeEach(() => {
        group = document.createElement('div')
        element = document.createElement('i')
        firstNeighbour = document.createElement('i')
        secondNeighbour = document.createElement('i')

        group.appendChild(element)
        group.appendChild(firstNeighbour)
        group.appendChild(secondNeighbour)
      });

      describe('with the next element invisible', () => {
        beforeEach(() => {
          firstNeighbour.style.display = 'none'
          secondNeighbour.style.display = 'block'

          subject.show_email(element)
        });

        it('is expected to flip the neighbours visibility', () => {
          expect(firstNeighbour.style.display).toEqual('block')
          expect(secondNeighbour.style.display).toEqual('none')
        });
      });

      describe('with the next element visible', () => {
        beforeEach(() => {
          firstNeighbour.style.display = 'block'
          secondNeighbour.style.display = 'none'

          subject.show_email(element)
        });

        it('is expected to flip the neighbours visibility', () => {
          expect(firstNeighbour.style.display).toEqual('none')
          expect(secondNeighbour.style.display).toEqual('block')
        });
      });
    });
  });
});
