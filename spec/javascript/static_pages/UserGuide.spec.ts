import { UserGuide } from '../../../app/javascript/packs/static_pages/UserGuide'

describe('UserGuide', () => {

  const ext = jasmine.createSpyObj('externalMocks', ['window', 'jQuery']);

  let subject: UserGuide;

  beforeEach(() => {
    subject = new UserGuide(ext.jQuery);
  });

  afterEach(() => {
    ext.jQuery.calls.reset();
  });

  describe('positionArrow', () => {
    const sectionElement = jasmine.createSpyObj('element', {'offset': {left: 1}, 'width': 3});
    const triangleElement = jasmine.createSpyObj('triangle', ['css']);

    beforeEach(() => {
      ext.jQuery.and.returnValues(sectionElement, sectionElement, triangleElement);

      subject.positionArrow();
    });

    it('is expected to get the section element', () => {
      expect(ext.jQuery).toHaveBeenCalledWith('#type1');
    });

    it('is expected to get the triangle element', () => {
      expect(ext.jQuery).toHaveBeenCalledWith('#triangle');
    });

    it('is expected to translate the triangle element', () => {
      expect(triangleElement.css).toHaveBeenCalledWith('transform', 'translate(-34.5px)')
    });
  });

  describe('changeFAQ', () => {
    const viewer = jasmine.createSpyObj('viewer', {width: 1});
    const questionContainer = jasmine.createSpyObj('questionContainer', ['css']);

    beforeEach(() => {
      spyOn(subject, 'positionArrow');
      ext.jQuery.and.returnValues(questionContainer, viewer, questionContainer);

      subject.changeFAQ(3)
    });

    it('is expected to reposition the arrow', () => {
      expect(subject.positionArrow).toHaveBeenCalled();
    });

    it('is expected to reset the question container position', () => {
      expect(questionContainer.css).toHaveBeenCalledWith('transform', 'translate(0px)');
    });

    it('is expected to get the width of the viewer element', () => {
      expect(viewer.width).toHaveBeenCalled();
    });

    it('is expected to reset the question container position', () => {
      expect(questionContainer.css).toHaveBeenCalledWith('transform', 'translate(-2px)');
    });
  });

  describe('jump', () => {
    const id = 'id';

    beforeEach(() => {
      spyOn(history, 'replaceState');
      location.href = 'about:blank';
      subject.jump(id);
    });

    it('is expected to set the href', () => {
      expect(location.href).toEqual(`about:blank#${id}`);
    });

    it('is expected to reset the history changes', () => {
      expect(history.replaceState).toHaveBeenCalledWith(null, null, 'about:blank');
    });
  });
});
