# frozen_string_literal: true

FactoryBot.define do
  factory :icd_group do
    first_category { 'MyString' }
    last_category { 'MyString' }
    description { 'MyText' }
  end
end
