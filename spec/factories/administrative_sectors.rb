# frozen_string_literal: true

FactoryBot.define do
  factory :administrative_sector, parent: :demographic_division, class: AdministrativeSector
end
