# frozen_string_literal: true

FactoryBot.define do
  factory :subprefecture, parent: :demographic_division, class: Subprefecture
end
