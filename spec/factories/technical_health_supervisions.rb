# frozen_string_literal: true

FactoryBot.define do
  factory :technical_health_supervision, parent: :demographic_division, class: TechnicalHealthSupervision
end
