# frozen_string_literal: true

FactoryBot.define do
  factory :census_sector, parent: :demographic_division, class: CensusSector
end
