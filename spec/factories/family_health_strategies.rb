# frozen_string_literal: true

FactoryBot.define do
  factory :family_health_strategy, parent: :demographic_division, class: FamilyHealthStrategy
end
