# frozen_string_literal: true

FactoryBot.define do
  factory :health_facility do
    cnes { 1 }
    name { 'MyString' }
    latitude { 1.5 }
    longitude { 1.5 }
    phone { 'MyString' }
    beds { 1 }
    administration { 'MUNICIPAL' }
  end
end
