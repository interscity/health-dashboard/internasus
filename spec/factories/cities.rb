# frozen_string_literal: true

FactoryBot.define do
  factory :city, parent: :demographic_division, class: City
end
