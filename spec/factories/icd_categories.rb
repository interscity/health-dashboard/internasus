# frozen_string_literal: true

FactoryBot.define do
  factory :icd_category do
    code { 'MyString' }
    description { 'MyText' }
  end
end
