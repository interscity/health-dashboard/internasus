# frozen_string_literal: true

FactoryBot.define do
  factory :hospitalization_datum do
    issue_date { '2019-09-20' }
    admission_date { '2019-09-20' }
    leave_date { '2019-09-20' }
    specialty { 1 }
    hospitalization_type { 1 }
    competence { 1 }
    complexity { 1 }
    procedure_id { 1 }
    hospitalization_days { 1 }
    hospitalization_rates { 1 }
    uti_rates { 1 }
    ui_rates { 1 }
    financing { 1 }
    service_value { 1.5 }
    distance { 0.0 }
  end
end
