# frozen_string_literal: true

FactoryBot.define do
  factory :regional_health_coordination, parent: :demographic_division, class: RegionalHealthCoordination
end
