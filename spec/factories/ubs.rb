# frozen_string_literal: true

FactoryBot.define do
  factory :ubs, parent: :demographic_division, class: Ubs
end
