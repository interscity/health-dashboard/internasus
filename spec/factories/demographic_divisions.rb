# frozen_string_literal: true

FactoryBot.define do
  factory :demographic_division do
    name { 'MyString' }
    shape { '{ "type": "Polygon", "coordinates": [[[-46.25,-23.85],[-46.25,-23.85],[-46.25,-23.85]]] }' }
    latitude { -23.0 }
    longitude { -46.0 }
  end
end
