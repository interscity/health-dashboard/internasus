# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DemographicDivisionsProceduresCounterService, type: :service do
  @patient_datum = FactoryBot.build(:patient_datum)
  @procedures_ids = @patient_datum.procedure_id

  let(:demographic_divisions) { %w[CensusSector AdministrativeSector] }

  let(:division_latitude) { 20.2 }
  let(:division_longitude) { 22.2 }
  let(:division_procedures_count) { 42 }
  let(:division_population) do
    { 'women' => 10, 'men' => 10 }
  end
  let(:procedures_by_demographic_division) do
    [division_latitude, division_longitude, division_population, division_procedures_count]
  end
  let(:procedures_by_demographic_division_array) { [procedures_by_demographic_division] }

  before do
    allow(DemographicDivision).to receive_message_chain(:distinct, :pluck).and_return demographic_divisions
  end

  describe 'initialize' do
    context 'with no additional parameters' do
      before do
        @subject = described_class.new(@procedures_ids)
      end

      it 'is expected to initialize filters as an empty array' do
        expect(@subject.send(:filters)).to eq []
      end
    end

    context 'with a demographic division parameter' do
      before do
        @subject = described_class.new(@procedures_ids, :administrative_sector)
      end

      it 'is expected to initialize filters as an empty array' do
        expect(@subject.send(:filters)).to eq []
      end
    end
  end

  describe 'instance method' do
    subject { described_class.new(@procedures_ids) }

    describe 'filters_from_params' do
      let(:params) do
        {
          'women' => 'true', 'men' => 'false', 'black' => 'false', 'mulatto' => 'true', 'native' => 'true',
          'white' => 'false', 'yellow' => 'false', '00a04' => 'false', '05a09' => 'false', '10a14' => 'false',
          '15a19' => 'false', '20a24' => 'false', '25a29' => 'false', '30a34' => 'true', '35a39' => 'false',
          '40a44' => 'false', '45a49' => 'false', '50a54' => 'false', '55a59' => 'false', '60a69' => 'false',
          '70' => 'false'
        }
      end

      it 'is expected to fetch params and build the list of filters uniting enabled filters' do
        expected = %w[women_30a34_mulatto women_30a34_native]

        expect(subject.filters_from_params(params)).to eq expected
      end
    end

    describe 'build_filters' do
      let(:params) { double('Rails params') }

      before do
        allow(subject).to receive(:filters_from_params).and_return filters
      end

      context 'when filters do not include empty string' do
        let(:filters) { ['women'] }

        it 'is expected to return a list of filters' do
          subject.build_filters(params)
          expect(subject.send(:filters)).to eq filters
        end
      end

      context 'when filters include empty string' do
        let(:filters) { [''] }

        it 'is expected to return only the total filter' do
          expect(subject.build_filters(params)).to eq ['total']
        end
      end
    end

    describe 'procedures_by_division_id' do
      let(:filtered_procedures) { double('filtered_procedures') }

      before do
        allow(subject).to receive(:demographic_division).and_return(:census_sector)
        allow(subject).to receive(:filtered_procedures).and_return(filtered_procedures)
        allow(filtered_procedures).to receive_message_chain(:select, :group)

        @result = subject.procedures_by_division_id
      end

      it 'is expected to group the procedures by demographic division id' do
        expect(filtered_procedures).to have_received(:select)
          .with('census_sector_id AS div_id', 'count(*) AS procs_count')

        expect(filtered_procedures.select).to have_received(:group)
          .with(ActiveRecord::Base.sanitize_sql('census_sector_id'))
      end
    end

    let(:procedures_by_division_id) { double('procedures_by_division_id') }

    describe 'demographic_divisions_rates' do
      let(:dbl) { double 'dbl' }
      let(:filters) { %w[men_10a14_native men_10a14_white] }
      let(:sum_of_cols_string) { 'pop.p_men_10a14_native + pop.p_men_10a14_white' }

      before do
        allow(subject).to receive(:procedures_by_division_id).and_return(procedures_by_division_id)
        allow(subject).to receive(:filters).and_return(filters)
        expect(PatientDatum).to receive_message_chain(:from, :joins, :where, :pluck).and_return(dbl)

        @result = subject.demographic_divisions_rates
      end

      it 'is expected to query facilities with hospitalization rates' do
        expect(@result).to equal(dbl)
      end
    end

    describe 'census_sector_counts_by' do
      let(:attribute) { :gender }
      let(:filtered_procedures) { double('filtered_procedures') }

      before do
        allow(subject).to receive(:filtered_procedures).and_return(filtered_procedures)
        allow(filtered_procedures).to receive_message_chain(:group, :count, :group_by, :transform_values)

        subject.census_sector_counts_by attribute
      end

      it 'is expected to return the count grouped by census_sector and gender' do
        expect(filtered_procedures).to have_received(:group).with(:census_sector_id, attribute)
        expect(filtered_procedures.group).to have_received(:count)
      end

      it 'is expected to mangle the data structure' do
        expect(filtered_procedures.group.count).to have_received(:group_by)
        expect(filtered_procedures.group.count.group_by).to have_received(:transform_values)
      end
    end

    describe 'census_sector_statistics' do
      let(:filtered_procedures) { double 'filtered_procedures' }
      let(:count_by_race) { { a: { c: 3 }, b: { c: 4 } } }
      let(:count_by_gender) { { a: { b: 3 }, b: {} } }
      let(:ids) { %i[a b] }

      before do
        allow(subject).to receive(:census_sector_counts_by).with(:race).and_return(count_by_race)
        allow(subject).to receive(:census_sector_counts_by).with(:gender).and_return(count_by_gender)
        allow(subject).to receive(:filtered_procedures).and_return(filtered_procedures)
        expect(filtered_procedures).to receive_message_chain(:group, :count).and_return([3, 4])

        @returned = subject.census_sector_statistics
      end

      it 'is is expected to get the total count' do
        expect(subject).to have_received(:filtered_procedures)
      end

      it 'is is expected to get the counts by race and gender' do
        expect(subject).to have_received(:census_sector_counts_by).with(:race)
        expect(subject).to have_received(:census_sector_counts_by).with(:gender)
      end

      it 'it is expected to return a list of ids' do
        expect(@returned[:ids]).to eq(ids)
      end
    end
  end
end
