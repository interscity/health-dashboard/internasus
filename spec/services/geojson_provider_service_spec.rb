# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GeojsonProviderService, type: :service do
  describe 'class method' do
    describe 'geojson_for' do
      let(:klass) { 'regional_health_coordination' }
      let(:rhc) do
        double(RegionalHealthCoordination, name: 'RHC', shape: '', id: '22', class: 'RegionalHealthCoordination')
      end

      before do
        allow(RegionalHealthCoordination).to receive(:find_each).and_yield rhc
        allow(JSON).to receive(:parse).and_return 'parsed'
      end

      it 'is expected to find and iterate over all klass records' do
        described_class.geojson_for(klass)
        expect(RegionalHealthCoordination).to have_received(:find_each)
      end

      it 'is expected to parse the record shape so we can turn eveything to JSON later' do
        described_class.geojson_for(klass)
        expect(JSON).to have_received(:parse).with('')
      end

      it 'is expected to return a hash in the GeoJSON format' do
        expected_hash = {
          'type' => 'FeatureCollection',
          'features' => [{
            'type' => 'Feature', 'geometry' => 'parsed',
            'properties' => {
              'name' => 'RHC',
              'dataUrl' => '/procedures/map_popup_on/regional_health_coordination/for/22',
              'errorMessage' => 'Houve um erro. Por favor tente novamente.'
            }
          }]
        }
        geojson_hash = described_class.geojson_for(klass)
        expect(geojson_hash).to eq(expected_hash)
      end
    end
  end
end
