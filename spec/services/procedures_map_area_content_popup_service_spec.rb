# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProceduresMapAreaContentPopupService, type: :service do
  let(:area_class) { 'subprefecture' }
  let(:id) { '1' }

  subject { described_class.new(area_class, id) }

  describe 'method' do
    describe 'build_popup_content' do
      context 'when there is patient information for that area' do
        let(:subprefecture) { double(Subprefecture, name: 'SP') }

        before do
          allow(subject).to receive(:area).and_return subprefecture
          allow(PatientDatum).to receive_message_chain(:where, :count)
          allow(PatientDatum).to receive_message_chain(:where, :not, :any?).and_return(true)

          subject.build_popup_content
        end

        it 'is expected to find the area' do
          expect(subject).to have_received(:area).exactly(2).times
        end

        it 'is expected to find patient data for that area' do
          expect(PatientDatum).to have_received(:where).with(area_class => subprefecture)
        end

        it 'is expected to count how many patient data we have found' do
          expect(PatientDatum.where).to have_received(:count)
        end
      end

      context 'when there is no patient information for that area' do
        let(:area_class) { 'city' }
        let(:city) { double(City, name: 'SP') }

        before do
          allow(subject).to receive(:area).and_return city
          allow(PatientDatum).to receive(:count)
          allow(PatientDatum).to receive_message_chain(:where, :not, :any?).and_return(false)

          subject.build_popup_content
        end

        it 'is expected to find the area' do
          expect(subject).to have_received(:area)
        end

        it 'is expected to not count patients for that area' do
          expect(PatientDatum).to_not have_received(:count)
        end
      end

      context 'when the area cannot have patient information' do
        let(:area_class) { 'ubs' }
        let(:city) { double(Ubs, name: 'Ubs Parque Anhanguera') }

        before do
          allow(subject).to receive(:area).and_return city
          allow(PatientDatum).to receive(:where)

          subject.build_popup_content
        end

        it 'is expected to find the area' do
          expect(subject).to have_received(:area)
        end

        it 'is expected to not try and find patient data for that area' do
          expect(PatientDatum).to_not have_received(:where)
        end
      end
    end
  end

  describe 'private method' do
    describe 'area' do
      let(:subprefecture) { double(Subprefecture, name: 'SP') }
      it 'is expected to find the area' do
        expect(Subprefecture).to receive(:find).with(id).and_return subprefecture

        expect(subject.send(:area)).to eq(subprefecture)
      end
    end
  end
end
