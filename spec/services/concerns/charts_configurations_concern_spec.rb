# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChartsConfigurationsConcern do
  class TestClass
    include ChartsConfigurationsConcern
  end
  subject { TestClass.new }
end
