# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'list data with params' do |category, data, column|
  let(:procedures_ids) { [42, 314] }
  let(:data_list) { [42, 314] }

  before do
    allow(subject).to receive(:procedures_ids).and_return(procedures_ids)
    allow(data).to receive_message_chain(:where, :pluck).and_return(data_list)
    expect(subject).to receive(:descriptive_statistics_hash).with(data_list)
    subject.send("#{category}_list")
  end

  it "is expected to get a hash with the descriptive statistics for #{column} from #{data}" do
    expect(data).to have_received(:where).with(procedure_id: procedures_ids)
    expect(data.where).to have_received(:pluck).with(column)
  end
end

RSpec.describe DescriptiveStatisticsConcern do
  class TestClass
    include DescriptiveStatisticsConcern
    def procedures_ids() end
  end

  subject { TestClass.new }

  let(:dummy_data) { { 'a' => 1, 'b' => 2, 'c' => 3 } }
  let(:data_hash) do
    {
      age_year: dummy_data,
      service_values: dummy_data,
      uti_rates: dummy_data,
      ui_rates: dummy_data,
      hospitalization_rates: dummy_data,
      hospitalization_days: dummy_data,
      hospitalization_types: { 'some_type' => nil }
    }
  end

  describe 'descriptive_statistics_for' do
    it 'is expected to list data when there is method definition and to skip when there is not' do
      %i[
        age_year_list hospitalization_days_list hospitalization_rates_list
        uti_rates_list ui_rates_list service_values_list
      ].each do |translation_method|
        expect(subject).to receive(translation_method)
      end

      subject.descriptive_statistics_for(data_hash)
    end
  end

  describe 'descriptive_statistics_hash' do
    let(:list) { double 'list' }

    before do
      expect(list).to receive(:extend).with(DescriptiveStatistics)
      %i[size min max].each do |method|
        expect(list).to receive(method)
      end
      %i[sum mean standard_deviation].each do |method|
        expect(list).to receive_message_chain(method, :to_d, :truncate)
      end
    end

    it 'is expected build a hash with the desired keys' do
      response = subject.descriptive_statistics_hash(list)

      %i[size min max sum mean standard_deviation].each do |key|
        expect(response).to have_key(key)
      end
    end
  end

  describe 'age_year_list' do
    it_behaves_like 'list data with params', 'age_year', PatientDatum, :age_year
  end

  describe 'hospitalization_days_list' do
    it_behaves_like 'list data with params', 'hospitalization_days', HospitalizationDatum, :hospitalization_days
  end

  describe 'hospitalization_rates_list' do
    it_behaves_like 'list data with params', 'hospitalization_rates', HospitalizationDatum, :hospitalization_rates
  end

  describe 'uti_rates_list' do
    it_behaves_like 'list data with params', 'uti_rates', HospitalizationDatum, :uti_rates
  end

  describe 'ui_rates_list' do
    it_behaves_like 'list data with params', 'ui_rates', HospitalizationDatum, :ui_rates
  end

  describe 'service_values_list' do
    it_behaves_like 'list data with params', 'service_values', HospitalizationDatum, :service_value
  end
end
