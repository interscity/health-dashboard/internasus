# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'two method query' do |test_method, first_method, first_argument, second_method, second_argument|
  let(:data) { double 'data' }
  let(:first_response) { double 'first_response' }
  let(:second_response) { double 'second_response' }

  before do
    allow(subject).to receive(:procedures_ids).and_return(procedures_ids)
    expect(data).to receive(first_method).with(first_argument).and_return(first_response)

    if second_argument
      expect(first_response).to receive(second_method).with(second_argument).and_return(second_response)
    else
      expect(first_response).to receive(second_method).and_return(second_response)
    end
  end

  it "is expected to call the methods #{first_method} and #{second_method} with correct arguments" do
    final_response = subject.send(test_method, data)
    expect(final_response).to eq(second_response)
  end
end

RSpec.describe DistancesConcern do
  class TestClass
    include DistancesConcern
    def count(data, group) end
  end

  subject { TestClass.new }

  let(:procedures_ids) { [42, 314] }

  describe 'distances_data' do
    it 'is expected to build a hash with all keys' do
      %i[distances_buckets specialties_means facility_type_means_per_specialty].each do |distances_method|
        expect(subject).to receive(distances_method)
      end

      result = subject.distances_data

      %i[distances_buckets specialties_means facility_type_means_per_specialty].each do |key|
        expect(result).to have_key(key)
      end
    end
  end

  describe 'private method' do
    describe 'distances_buckets' do
      let(:count) { double 'count' }
      let(:grouped_distances) { double 'grouped_distances' }
      let(:return_hash) { { 0 => 9, 1 => 6, 2 => 3, 3 => 0 } }

      before do
        expect(subject).to receive(:count).with(
          HospitalizationDatum,
          'CASE WHEN distance <= 1 THEN 0'\
            'WHEN distance <= 5 THEN 1'\
            'WHEN distance <= 10 THEN 2 ELSE 3 END'
        ).and_return(grouped_distances)
        expect(grouped_distances).to receive_message_chain(:sort, :to_h).and_return(return_hash)
        @distances_buckets = subject.send(:distances_buckets)
      end

      it 'is expected count the number of procedures within the distances ranges' do
        expect(@distances_buckets).to eq(return_hash)
      end
    end

    describe 'specialties_means' do
      let(:data) { double 'data' }
      let(:defaults_hash) { double 'defaults_hash' }
      let(:expected_return) { double 'expected_return' }

      before do
        expect(subject).to receive(:defaults_hash_from).with(
          HospitalizationDatum, { specialty: :desc }, :specialty, 0
        ).and_return(defaults_hash)

        expect(subject).to receive(:select_distinct_and_order).with(
          [:specialty, 'AVG(distance) AS average_distance'], { specialty: :desc }
        ).and_return(data)
        expect(subject).to receive(:group_by_specialties).with(data).and_return(data)
        expect(subject).to receive(:truncate_averages).with(data).and_return(data)

        expect(data).to receive(:reverse_merge).with(defaults_hash).and_return(expected_return)
      end

      it 'is expected to return a hash with the mean distances grouped by specialties' do
        result = subject.send(:specialties_means)
        expect(result).to eq(expected_return)
      end
    end

    describe 'facility_type_means_per_specialty' do
      let(:specialties_defaults) { double 'specialties_defaults' }
      let(:facility_type_defaults) { double 'facility_type_defaults' }
      let(:facility_type) { double 'facility_type' }
      let(:facility_type_array) { double 'facility_type_array' }
      let(:facility_type_distances_hash) do
        {
          specialty_one: facility_type,
          specialty_two: facility_type
        }
      end

      before do
        expect(subject).to receive(:defaults_hash_from).with(HospitalizationDatum, :specialty, :specialty, {})
                                                       .and_return(specialties_defaults)
        expect(subject).to receive(:defaults_hash_from).with(
          HealthFacility,
          { facility_type: :desc },
          :facility_type,
          0
        ).and_return(facility_type_defaults)

        expect(subject).to receive(:mean_distances_per_specialty_and_facility_type)
          .and_return(facility_type_distances_hash)
        expect(facility_type_distances_hash).to receive(:reverse_merge!).with(specialties_defaults)
        expect(facility_type).to receive(:reverse_merge!).with(facility_type_defaults).exactly(2)
        expect(facility_type).to receive(:to_a).at_least(2).and_return(facility_type_array)
        expect(facility_type_array).to receive(:map).at_least(2)
      end

      it 'is expected to return a hash with the mean distances grouped by specialties and facility type' do
        result = subject.send(:facility_type_means_per_specialty)
        expect(result).to eq(facility_type_distances_hash)
      end
    end

    describe 'select_distinct_and_order' do
      let(:columns) { double 'columns' }
      let(:order) { double 'order' }

      before do
        allow(subject).to receive(:procedures_ids).and_return(procedures_ids)
        allow(HospitalizationDatum).to receive_message_chain(:select, :where, :distinct, :order)
        subject.send(:select_distinct_and_order, columns, order)
      end

      it 'is expected to select distinct values and order them accordingly' do
        expect(HospitalizationDatum).to have_received(:select).with(columns)
        expect(HospitalizationDatum.select).to have_received(:where).with({ procedure_id: procedures_ids })
        expect(HospitalizationDatum.select.where).to have_received(:distinct)
        expect(HospitalizationDatum.select.where.distinct).to have_received(:order).with(order)
      end
    end

    describe 'group_by_specialties' do
      it_behaves_like 'two method query',
                      :group_by_specialties,
                      :group, :specialty,
                      :group_by, nil
    end

    describe 'join_and_group_health_facilities' do
      it_behaves_like 'two method query',
                      :join_and_group_health_facilities,
                      :joins, :health_facility,
                      :group, 'health_facilities.facility_type'
    end

    describe 'truncate_averages' do
      let(:value_1) { double 'value_1' }
      let(:value_2) { double 'value_2' }
      let(:data) do
        {
          value_1: value_1,
          value_2: value_2
        }
      end

      before do
        expect(value_1).to receive_message_chain(:first, :average_distance, :to_d, :truncate)
        expect(value_2).to receive_message_chain(:first, :average_distance, :to_d, :truncate)
      end

      it 'is expect to truncate the distances' do
        result = subject.send(:truncate_averages, data)
        expect(result).to equal(data)
      end
    end

    describe 'truncate_health_facilities_averages' do
      let(:value) { double 'value' }
      let(:data) do
        {
          value_1: value,
          value_2: value
        }
      end

      before do
        expect(value).to receive(:group_by).exactly(2).and_return(value)
        expect(subject).to receive(:truncate_averages).with(value).exactly(2)
      end

      it 'is expected to call truncate_averages for the hash values' do
        result = subject.send(:truncate_health_facilities_averages, data)
        expect(result).to equal(data)
      end
    end

    describe 'deafults_hash_from' do
      let(:data) { double 'data' }
      let(:order) { double 'order' }
      let(:column) { double 'column' }
      let(:value) { double 'value' }

      before do
        allow(data).to receive_message_chain(:distinct, :order, :pluck)
        expect(data).to receive(:distinct)
        expect(data.distinct).to receive(:order).with(order)
        expect(data.distinct.order).to receive(:pluck).with(column).and_return(%w[key_1 key_2])
      end

      it 'is expected to return a hash with all distinct values from column as keys' do
        result = subject.send(:defaults_hash_from, data, order, column, value)
        expect(result).to eq({ 'key_1' => value, 'key_2' => value })
      end
    end

    describe 'mean_distances_per_specialty_and_facility_type' do
      let(:data) { double 'data' }

      before do
        expect(subject).to receive(:select_distinct_and_order).with(
          [:specialty, 'health_facilities.facility_type AS health_facility_type', 'AVG(distance) AS average_distance'],
          :specialty
        ).and_return(data)
        expect(subject).to receive(:join_and_group_health_facilities).with(data).and_return(data)
        expect(subject).to receive(:group_by_specialties).with(data).and_return(data)
        expect(subject).to receive(:truncate_health_facilities_averages).with(data).and_return(data)
      end

      it 'is expected to return a hash with mean distances per specialty and facility type' do
        result = subject.send(:mean_distances_per_specialty_and_facility_type)
        expect(result).to eq(data)
      end
    end
  end
end
