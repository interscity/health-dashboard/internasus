# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ColoringConcern do
  class TestClass
    include ColoringConcern
  end

  subject { TestClass.new }

  describe 'process_chart_data' do
    let(:data_hash) do
      {
        no_coloring: { data: [] },
        bar_coloring: { data: [], chart_type: 'bar', chart_options: { colors: :test } },
        fallback_coloring: { data: [], chart_type: 'test', chart_options: { colors: :test } }
      }
    end
    let(:colors) { double 'colors' }
    let(:color_hash) { double 'color_hash' }
    let(:color_hashes) { { test: color_hash } }

    before do
      expect(subject).to receive(:fallback_coloring).with(data_hash[:fallback_coloring], color_hash).and_return(colors)
      expect(subject).to receive(:bar_coloring).with(data_hash[:bar_coloring], color_hash).and_return(colors)
    end

    it 'is expected to process data from all charts' do
      result = subject.color_charts(data_hash, color_hashes)
      %i[bar_coloring fallback_coloring].each do |i|
        expect(result[i][:chart_options][:colors]).to eq(colors)
      end
    end
  end

  describe 'private' do
    describe 'fallback_coloring' do
      let(:data_hash) { { data: [{ name: 'name_1' }, { name: 'name_3' }] } }
      let(:color_hash) { { 'name_1' => 'color_1', 'name_2' => 'color_2', 'name_3' => 'color_3' } }

      it 'is expected to return an array with the right colors' do
        response = subject.send(:fallback_coloring, data_hash, color_hash)
        expect(response).to eq(%w[color_1 color_3])
      end
    end

    describe 'bar_coloring' do
      let(:data_hash) { { data: [{ name: 'name_1' }, { name: 'name_3' }] } }
      let(:color_hash) { { 'name_1' => 'color_1', 'name_2' => 'color_2', 'name_3' => 'color_3' } }

      it 'is expected to return an array with the right colors' do
        response = subject.send(:bar_coloring, data_hash, color_hash)
        expect(data_hash[:data]).to eq(
          [
            { name: 'name_1', itemStyle: { color: 'color_1' } },
            { name: 'name_3', itemStyle: { color: 'color_3' } }
          ]
        )
        expect(response).to eq(%w[name_1 name_3])
      end
    end

    describe 'stacked_bar_coloring' do
      let(:data_hash) do
        { data: { 'facility' => [['Especialidade', '< 1 km', '> 1 km e < 5 km', '> 5 km e < 10 km', '> 10 km']] } }
      end
      let(:color_hash) do
        { '< 1 km' => 'color_1', '> 1 km e < 5 km' => 'color_2',
          '> 5 km e < 10 km' => 'color_3', '> 10 km' => 'color_4' }
      end

      it 'is expected to return an array with the right colors' do
        response = subject.send(:stacked_bar_coloring, data_hash, color_hash)
        expect(response).to eq(%w[color_1 color_2 color_3 color_4])
      end
    end
  end
end
