# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'data formatting' do |method, raw_data, formated_data|
  it 'is expected to format' do
    response = subject.send(method, raw_data)
    expect(response).to eq(formated_data)
  end
end

RSpec.describe DataProcessingConcern do
  class TestClass
    include DataProcessingConcern
  end

  subject { TestClass.new }

  let(:procedures_ids) { [42, 314] }

  describe 'process_chart_data' do
    let(:data) { double 'data' }
    let(:processed_data) { double 'processed_data' }
    let(:data_hash) do
      {
        bar: { chart_type: 'bar', data: data },
        pie: { chart_type: 'pie', data: data },
        treemap: { chart_type: 'treemap', data: data }
      }
    end

    before do
      expect(subject).to receive(:fallback_format).with(data_hash[:bar]).and_return(processed_data)
      expect(subject).to receive(:pie_format).with(data_hash[:pie]).and_return(processed_data)
      expect(subject).to receive(:treemap_format).with(data_hash[:treemap]).and_return(processed_data)
    end

    it 'is expected to process data from all charts' do
      result = subject.process_chart_data(data_hash)
      result.each_value do |v|
        expect(v[:data]).to eq(processed_data)
      end
    end
  end

  describe 'fallback_format' do
    context 'with percentages' do
      it_behaves_like 'data formatting', 'fallback_format',
                      { chart_type: 'bar', chart_options: { percentage: true },
                        data: { 'key_one' => 20, 'key_two' => 30 } },
                      [
                        { name: 'key_one', percentage: '40.00%', value: 20 },
                        { name: 'key_two', percentage: '60.00%', value: 30 }
                      ]
    end
    context 'without percentages' do
      it_behaves_like 'data formatting', 'fallback_format',
                      { chart_type: 'bar', data: { 'key_one' => 20, 'key_two' => 30 } },
                      [{ name: 'key_one', value: 20 }, { name: 'key_two', value: 30 }]
    end
  end

  describe 'pie_format' do
    it_behaves_like 'data formatting', 'pie_format',
                    { chart_type: 'pie', data: { 'key_one' => 20, 'key_two' => 30 } },
                    [
                      { name: 'key_one', percentage: '40.00%', value: 20 },
                      { name: 'key_two', percentage: '60.00%', value: 30 }
                    ]
  end

  describe 'map_format' do
    it_behaves_like 'data formatting', 'map_format',
                    { chart_type: 'map', data: { 'key_one' => 20, 'key_two' => 30 } },
                    {
                      max_value: 30,
                      data: [{ name: 'key_one', percentage: '40.00%', value: 20 },
                             { name: 'key_two', percentage: '60.00%', value: 30 }]
                    }
  end

  describe 'stacked_bar_format' do
    it_behaves_like 'data formatting', 'stacked_bar_format',
                    {
                      data: {
                        'facility' => [{ 'Total' => [45, 25, 20, 10],
                                         'TYPE_ONE' => [35, 15, 0, 0],
                                         'TYPE_TWO' => [10, 10, 20, 10] },
                                       { 'Total' => [45.0, 25.0, 20.0, 10.0],
                                         'TYPE_ONE' => [70.0, 30.0, 0.0, 0.0],
                                         'TYPE_TWO' => [20.0, 20.0, 40.0, 20.0] }]
                      }
                    },
                    {
                      'facility' => [['Especialidade', '< 1 km', '> 1 km e < 5 km', '> 5 km e < 10 km', '> 10 km'],
                                     [['Total', 45, 25, 20, 10],
                                      ['TYPE_ONE', 35, 15, 0, 0],
                                      ['TYPE_TWO', 10, 10, 20, 10]],
                                     [['Especialidade', '< 1 km', '> 1 km e < 5 km', '> 5 km e < 10 km', '> 10 km'],
                                      ['Total', 45.0, 25.0, 20.0, 10.0],
                                      ['TYPE_ONE', 70.0, 30.0, 0.0, 0.0],
                                      ['TYPE_TWO', 20.0, 20.0, 40.0, 20.0]],
                                     [100, 50, 50]]
                    }
  end

  describe 'treemap_format' do
    context 'invalid treemap type' do
      it 'is expected to return an empty list' do
        result = subject.treemap_format({})
        expect(result).to eq([])
      end
    end

    context 'valid treemap type' do
      it 'is expected to call all valid treemap format methods' do
        %w[icd hosp_group].each do |method|
          expect(subject).to receive("treemap_#{method}_format").with({ treemap_type: method }).and_return([method])
          result = subject.send("treemap_#{method}_format", { treemap_type: method })
          expect(result).to eq([method])
        end
      end
    end
  end

  describe 'treemap_icd_format' do
    let(:data) { double 'data' }
    let(:input_hash) { { data: data } }
    let(:chart_data) { double 'chart_data' }

    before do
      allow(subject).to receive(:create_icd_treemap_scaffold).and_return(chart_data)
      expect(subject).to receive(:populate_icd_data_treemap).with(chart_data, data)
    end

    it 'is expected to format the values' do
      result = subject.treemap_icd_format(input_hash)
      expect(result).to eq(chart_data)
    end
  end

  describe 'treemap_hosp_group_format' do
    context 'data with an empty key and an invalid key' do
      it_behaves_like 'data formatting',
                      'treemap_hosp_group_format',
                      { data: { '123123': 10, '123321': 10, '321123': 10, '12312': 5, '': 5 } },
                      [
                        {
                          name: '123',
                          children: [
                            { fullname: '123123', name: '123123', value: 10, percentage: '25.00%' },
                            { fullname: '123321', name: '123321', value: 10, percentage: '25.00%' }
                          ]
                        },
                        {
                          name: '321',
                          children: [{ fullname: '321123', name: '321123', value: 10, percentage: '25.00%' }]
                        }
                      ]
    end

    context 'data with only invalid keys' do
      it_behaves_like 'data formatting',
                      'treemap_hosp_group_format',
                      { data: { '12312': 10, '12332': 10, '32112': 10, '': 10 } },
                      []
    end

    context 'empty data' do
      it_behaves_like 'data formatting', 'treemap_hosp_group_format', { data: [] }, []
    end
  end

  describe 'private methods' do
    describe 'create_icd_treemap_scaffold' do
      it 'is expected to create a scaffold for icd_treemap' do
        result = subject.send('create_icd_treemap_scaffold')
        (0...26).each do |letter_index|
          (0...10).each do |number_index|
            expect(result[letter_index][:children]).to include(
              { name: (letter_index + 65).chr + (number_index + 48).chr, children: [] }
            )
          end
        end
      end
    end

    describe 'populate_icd_data_treemap' do
      let(:treemap) do
        [
          { name: 'A', children: [{ name: 'A0', children: [] }, { name: 'A1', children: [] }] },
          { name: 'B', children: [{ name: 'B0', children: [] }, { name: 'B1', children: [] }] }
        ]
      end

      context 'normal data with an invalid key' do
        let(:data) { { 'A00 - a_test' => 10, 'B10 - b_test' => 10, 'C00 - c_test' => 20 } }

        it 'is expected to populate scaffold with valid entries' do
          subject.send('populate_icd_data_treemap', treemap, data)
          expect(treemap).to eq(
            [
              {
                name: 'A',
                children: [
                  { name: 'A0', children: [{ fullname: 'A00 - a_test', name: 'A00',
                                             value: 10, percentage: '25.00%' }] },
                  { name: 'A1', children: [] }
                ]
              },
              {
                name: 'B',
                children: [
                  { name: 'B0', children: [] },
                  { name: 'B1', children: [{ fullname: 'B10 - b_test', name: 'B10', value: 10, percentage: '25.00%' }] }
                ]
              }
            ]
          )
        end
      end

      context 'data with only invalid insertions' do
        let(:data) { { '' => 10, 'B20 - b_test' => 10, '0aa - c_test' => 10, 'aaa - aaa_test' => 10 } }

        it 'is expected to reject all insertions' do
          initial_treemap = treemap.deep_dup
          subject.send('populate_icd_data_treemap', treemap, data)
          expect(treemap).to eq(initial_treemap)
        end
      end
    end
  end
end
