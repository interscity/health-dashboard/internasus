# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'returns health facilities and patients data relations' do
  it 'is expected to return the health facilities relation' do
    expect(@returned_health_facilities).to eq(health_facilities)
  end

  it 'is expected to return the patients data relation' do
    expect(@returned_patients_data).to eq(patients_data)
  end
end

RSpec.shared_examples 'returns procedures ids relation' do
  it 'is expected to return the procedures ids relation' do
    expect(@returned_patients_data).to eq(patients_data)
  end
end

RSpec.shared_examples 'returns search relation' do
  it 'is expected to return the search relation' do
    expect(@returned_search).to eq(search)
  end
end

RSpec.shared_examples 'ignores empty' do
  context 'and the value is empty' do
    let(:value_empty) { true }

    it 'is expected to not add the field to the result' do
      expect(@result[field]).to be_nil
    end
  end

  context 'and the value is not empty' do
    let(:value_empty) { false }

    it 'is expected to set a formatted value to the field in the result' do
      expect(described_class).to have_received(:format_active_filter).with(field, value)
      expect(@result[field]).to eq(formatted_value)
    end
  end
end

RSpec.shared_examples 'format active filter field in array' do |fields_array, method|
  let(:field) { fields_array.sample }
  let(:return_value) { double 'return_value' }

  before do
    allow(described_class).to receive(:send).with(method, field, value).and_return(return_value)
    @result = described_class.format_active_filter(field, value)
  end

  it 'is expected to call respective method and return correct value' do
    expect(@result).to eq(return_value)
  end
end

RSpec.shared_examples 'active filter format array' do |group, method|
  let(:field) { group.keys.sample }
  let(:input_value) { ['value 1', 'value 2'] }
  let(:return_value) { 'return_value' }

  before do
    allow(described_class).to receive(method).and_return(return_value)
    @result = described_class.send(:format_array_active_filter, field, input_value)
  end

  it "is expected to join the array, calling #{method} for each value" do
    input_value.each do |value|
      expect(described_class).to have_received(method)
        .with(*group[field], value)
    end
    expect(@result).to eq('return_value; return_value')
  end
end

RSpec.describe ProceduresSearchService, type: :service do
  describe 'class method' do
    let(:health_facilities) { double('health_facilities') }
    let(:patients_data) { double('patients_data') }
    let(:search) { double('search') }
    let(:procedures_ids) { double('procedures_ids') }

    describe 'empty' do
      before do
        allow(HealthFacility).to receive(:all).and_return(health_facilities)
        allow(PatientDatum).to receive(:none).and_return(patients_data)

        @returned_health_facilities, @returned_patients_data = described_class.empty
      end

      it 'is expected to get all HealthFacilities' do
        expect(HealthFacility).to have_received(:all)
      end

      it 'is expected to get none PatientDatum' do
        expect(PatientDatum).to have_received(:none)
      end

      it 'is expected to return the query values' do
        expect(@returned_health_facilities).to eq(health_facilities)
        expect(@returned_patients_data).to eq(patients_data)
      end
    end

    describe 'not_empty' do
      let(:hospitalization_data) { double('hospitalization_data') }
      let(:health_facilities_ids) { double('health_facilities_ids') }
      let(:hospitalization_data_count) { 10 }
      let(:patients_latlong_array) do
        [
          [10.1, 10.9, [1, 3, 5]],
          [10.9, 10.1, [2, 4, 6]]
        ]
      end

      before do
        allow(search).to receive_message_chain(:result, :includes).and_return(hospitalization_data)

        allow(hospitalization_data).to receive_message_chain(:distinct, :select).and_return(health_facilities_ids)
        allow(hospitalization_data).to receive(:select).and_return(procedures_ids)
        allow(hospitalization_data).to receive(:count).and_return(hospitalization_data_count)

        allow(HealthFacility).to receive(:where).and_return(health_facilities)

        allow(PatientDatum).to receive_message_chain(:select, :where).and_return(patients_data)
        allow(patients_data).to receive_message_chain(:group, :pluck).and_return(patients_latlong_array)

        @returned_health_facilities,
          @returned_procedures_ids,
          @returned_patients_data,
          @returned_patients_latlong_data = described_class.not_empty(search)
      end

      it 'is expected to build the search result relation' do
        expect(search).to have_received(:result)
        expect(search.result).to have_received(:includes).with(:health_facility, :patient_datum)
      end

      it 'is expected to get the relation of filtered HealthFacilities' do
        expect(hospitalization_data).to have_received(:distinct).with(:health_facility_id)
        expect(hospitalization_data.distinct).to have_received(:select).with(:health_facility_id)

        expect(HealthFacility).to have_received(:where).with(id: health_facilities_ids)
      end

      it 'is expected to get the relation procedures ids of the filtered data' do
        expect(hospitalization_data).to have_received(:select).with(:procedure_id)
      end

      it 'is expected to get the relation of filtered PatientDatum' do
        expect(PatientDatum).to have_received(:select).with(:latitude, :longitude, :procedure_id)
        expect(PatientDatum.select).to have_received(:where).with(procedure_id: procedures_ids)
      end

      it 'is expected to get the patient data grouped by latitude and longitude values' do
        expect(patients_data).to have_received(:group).with(:latitude, :longitude)
        expect(patients_data.group).to have_received(:pluck).with(:latitude, :longitude, 'array_agg(procedure_id)')
      end

      it_behaves_like 'returns health facilities and patients data relations'
      it_behaves_like 'returns procedures ids relation'
    end

    describe 'search' do
      before do
        expect(HospitalizationDatum).to receive(:ransack).with(query_params).and_return(search)
      end

      context 'when the search is empty' do
        let(:query_params) { nil }

        before do
          allow(described_class).to receive(:empty).and_return([health_facilities, patients_data])

          @returned_search,
            @returned_health_facilities,
            @returned_patients_data,
            @returned_procedures_ids,
            @returned_dataset_proportion = described_class.search(query_params)
        end

        it_behaves_like 'returns health facilities and patients data relations'
        it_behaves_like 'returns search relation'

        it 'is expected to return an empty array of procedures ids' do
          expect(@returned_procedures_ids).to be_empty
        end

        it 'is expected to call the method for empty searches' do
          expect(described_class).to have_received(:empty)
        end

        it 'is expected to return the dataset proportion as zero' do
          expect(@returned_dataset_proportion).to eq(0.0)
        end
      end

      context 'when the search is not empty' do
        let(:query_params) { { something_in: ['from nothing'] } }
        let(:dataset_proportion) { 0.42 }

        before do
          allow(described_class).to receive(:not_empty).and_return(
            [health_facilities, patients_data, procedures_ids, dataset_proportion]
          )

          @returned_search,
            @returned_health_facilities,
            @returned_patients_data,
            @returned_procedures_ids,
            @returned_dataset_proportion = described_class.search(query_params)
        end

        it_behaves_like 'returns health facilities and patients data relations'
        it_behaves_like 'returns search relation'
        it_behaves_like 'returns procedures ids relation'

        it 'is expected to call the method for not empty searches' do
          expect(described_class).to have_received(:not_empty).with(search)
        end

        it 'is expected to return the proportion of the dataset that the returned data represents' do
          expect(@returned_dataset_proportion).to eq(dataset_proportion)
        end
      end
    end

    describe 'format_active_filter' do
      let(:field) { 'some_field' }
      let(:value) { double 'value' }

      context 'when it matches none of the fields' do
        before do
          @result = described_class.format_active_filter(field, value)
        end

        it 'is expected to return the value unchanged' do
          expect(@result).to eq(value)
        end
      end

      context 'when it is in DATE_FIELDS' do
        it_behaves_like 'format active filter field in array', described_class::DATE_FIELDS, 'date_format'
      end

      context 'when it is in ARRAY_FIELDS' do
        it_behaves_like 'format active filter field in array', described_class::ARRAY_FIELDS, 'array_format'
      end

      context 'when it is in HUMAN_ATTR_FIELDS' do
        it_behaves_like 'format active filter field in array',
                        described_class::HUMAN_ATTR_FIELDS.keys,
                        'human_attr_format'
      end

      context 'when it is in NAME_FIELDS' do
        it_behaves_like 'format active filter field in array', described_class::NAME_FIELDS.keys, 'name_format'
      end

      context 'when it is in ICD_FIELDS' do
        it_behaves_like 'format active filter field in array', described_class::ICD_FIELDS.keys, 'icd_format'
      end

      context 'when it is in RANGES_FIELDS' do
        it_behaves_like 'format active filter field in array', described_class::RANGES_FIELDS.keys, 'ranges_format'
      end
    end

    describe 'active_filters' do
      let(:field) { double('field') }
      let(:value) { double('value') }
      let(:formatted_value) { double('formatted_value') }
      let(:query_params) { { field => value } }

      before do
        allow(described_class::ALL_FIELDS).to receive(:each).and_yield(field)
        allow(value).to receive(:is_a?).and_return(false)
        allow(value).to receive(:is_a?).with(Array).and_return(is_a_array)
        allow(value).to receive(:empty?).and_return(value_empty)

        allow(described_class).to receive(:format_active_filter).and_return(formatted_value)
        allow(value).to receive(:reject!)

        @result = described_class.active_filters(query_params)
      end

      context 'when the value is an Array' do
        let(:is_a_array) { true }
        let(:value_empty) { nil }

        it_behaves_like 'ignores empty'

        it 'is expected to reject empty array elements' do
          expect(described_class::ALL_FIELDS).to have_received(:each)
          expect(value).to have_received(:is_a?).with(Array)
          expect(value).to have_received(:empty?)
          expect(value).to have_received(:reject!)
        end
      end

      context 'when the value is not an Array' do
        let(:is_a_array) { false }

        it_behaves_like 'ignores empty'
      end

      context 'with empty query params' do
        let(:is_a_array) { false }
        let(:value_empty) { nil }
        let(:query_params) { nil }

        it 'is expected to return an empty hash' do
          expect(@result).to eq({})
        end
      end

      context 'when the value is a default range' do
        let(:is_a_array) { false }
        let(:value_empty) { nil }
        let(:field) { described_class::RANGES_FIELDS.keys.sample }
        let(:query_params) { { field => described_class::RANGES_FIELDS[field] } }

        it 'is expected to return an empty hash' do
          expect(@result).to eq({})
        end
      end
    end

    describe 'private method' do
      describe 'date_format' do
        let(:value) { '2015/01/31' }

        before do
          @result = described_class.send(:date_format, 'some_field', value)
        end

        it 'is expected to format to dd/mm/yyyy' do
          expect(@result).to eq(Date.parse(value).strftime('%d/%m/%Y'))
        end
      end

      describe 'array_format' do
        let(:value) { %w[some thing] }

        before do
          @result = described_class.send(:array_format, 'some_field', value)
        end

        it 'is expected to join array values' do
          expect(@result).to eq('some; thing')
        end
      end

      describe 'human_attribute_format' do
        let(:field) { described_class::HUMAN_ATTR_FIELDS.keys.sample }
        let(:value0) { double 'value0' }
        let(:value1) { double 'value1' }
        let(:input_value) { [value0, value1] }

        before do
          expect(described_class).to receive(:human_attribute_value).with(*described_class::HUMAN_ATTR_FIELDS[field],
                                                                          value0).and_return('value_0')
          expect(described_class).to receive(:human_attribute_value).with(*described_class::HUMAN_ATTR_FIELDS[field],
                                                                          value1).and_return('value_1')
          @result = described_class.send(:human_attr_format, field, input_value)
        end

        it 'is expected to format and join array values' do
          expect(@result).to eq('value_0; value_1')
        end
      end

      describe 'name_format' do
        let(:field) { described_class::NAME_FIELDS.keys.sample }
        let(:input_value) { [0, 1] }
        let(:value1) { double 'value1' }
        let(:value2) { double 'value2' }

        before do
          [value1, value2].each.each_with_index do |v, i|
            allow(described_class::NAME_FIELDS[field]).to receive(:find).with(i).and_return(v)
            allow(v).to receive(:name).and_return("value_#{i}")
          end
          @result = described_class.send(:name_format, field, input_value)
        end

        it 'is expected to format and join array values' do
          expect(@result).to eq('value_0; value_1')
        end
      end

      describe 'icd_format' do
        let(:field) { described_class::ICD_FIELDS.keys.sample }
        let(:value0) { double 'value0' }
        let(:value1) { double 'value1' }
        let(:input_value) { [value0, value1] }

        before do
          expect(described_class).to receive(:icd_value).with(described_class::ICD_FIELDS[field],
                                                              value0).and_return('value_0')
          expect(described_class).to receive(:icd_value).with(described_class::ICD_FIELDS[field],
                                                              value1).and_return('value_1')
          @result = described_class.send(:icd_format, field, input_value)
        end

        it 'is expected to format and join array values' do
          expect(@result).to eq('value_0; value_1')
        end
      end

      describe 'ranges_format' do
        let(:value) { '42,314' }

        before do
          @result = described_class.send(:ranges_format, 'some_field', value)
        end

        it 'is expected to format range' do
          expect(@result).to eq('42 - 314')
        end
      end

      describe 'human_attribute_value' do
        let(:model) { 'patient_datum' }
        let(:attribute) { 'gender' }
        let(:separator) { '_' }
        let(:value) { 'F' }
        let(:translated_value) { 'Feminino' }

        before do
          allow(I18n).to receive(:t).and_return(translated_value)

          @result = described_class.send(:human_attribute_value, model, attribute, separator, value)
        end

        it 'is expected to build the translation key and return the i18n value' do
          expect(I18n).to have_received(:t).with("#{model}.#{attribute}#{separator}#{value}")
          expect(@result).to eq(translated_value)
        end
      end

      describe 'icd_value' do
        let(:model) { double 'm' }
        let(:id) { double 'id' }
        let(:values) { %w[A00 ICD] }

        before do
          allow(model).to receive_message_chain(:where, :pluck)
          expect(model).to receive(:where).with(id: id)
          expect(model.where).to receive(:pluck).with(:code, :description).and_return(values)
          @result = described_class.send(:icd_value, model, id)
        end

        it 'is expected to build icd name from model id' do
          expect(@result).to eq('A00 - ICD')
        end
      end
    end
  end
end
