# frozen_string_literal: true

module TestHelpers
  def render_with_ransack_form(partial, klass = HospitalizationDatum)
    search_form_for klass.ransack([]), url: '', html: { method: :post } do |form|
      render partial: partial, locals: { form: form }
    end
  end
end
