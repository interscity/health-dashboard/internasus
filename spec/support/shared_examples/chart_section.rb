# frozen_string_literal: true

RSpec.shared_examples 'chart section' do |stats, charts_ids, parent_id|
  before do
    allow(view).to receive(:render).and_call_original
    expect(view).to receive(:render).with(partial: 'general_data/charts',
                                          locals: { charts_ids: charts_ids, parent_id: parent_id })

    stats.each do |key, chart|
      stats[key] = { chart_type: 'not map', chart_title: '', chart_area: '', chart_options: {}, data: {} }.merge(chart)
    end

    assign(:stats, stats)

    render
  end

  it 'is expected to have the div for each id of the list' do
    id_list = charts_ids.map { |id| "#{id.to_s.gsub('_', '-')}-chart" }
    id_list.each do |id|
      expect(rendered).to have_selector('div', id: id)
    end
  end
end
