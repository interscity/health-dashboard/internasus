# frozen_string_literal: true

RSpec.shared_examples 'authenticated controller' do
  before do
    allow(Rails.application.config).to receive(:public).and_return(false)
    expect(subject).to receive(:authenticate_user!)
  end
end
