# frozen_string_literal: true

RSpec.shared_examples 'contain field' do |type, select_id, label_text, search_type|
  context select_id do
    it 'is expected to have a label' do
      expect(rendered).to have_selector("label[for='#{select_id}']")
      expect(rendered).to have_content(label_text)
    end

    it "is expected to have a #{type} field" do
      id = search_type ? "#{select_id}_#{search_type}" : select_id

      expect(rendered).to have_selector(type, id: id)
    end
  end
end
