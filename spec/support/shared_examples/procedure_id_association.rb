# frozen_string_literal: true

RSpec.shared_examples 'has procedure_id association' do |subject, associated|
  it 'is expected to find by the procedure_id' do
    expect(associated.class).to receive(:find_by)
      .with(procedure_id: subject.procedure_id)
      .and_return(associated)

    expect(subject.send(associated.class.to_s.underscore)).to eq(associated)
  end
end
