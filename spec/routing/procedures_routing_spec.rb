# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/controller_with_index'

RSpec.describe ProceduresController do
  describe 'routing' do
    let(:id) { 1234 }

    it_behaves_like 'controller with index', 'procedures'

    it {
      is_expected.to route(:post, 'procedures/search')
        .to(controller: :procedures, action: :search)
    }

    it {
      is_expected.to route(:get, 'procedures/search')
        .to(controller: :procedures, action: :index)
    }

    it {
      is_expected.to route(:get, 'procedures/export')
        .to(controller: :procedures, action: :export)
    }

    it {
      is_expected.to route(:get, "procedures/#{id}")
        .to(controller: :procedures, action: :show, id: id)
    }

    it {
      is_expected.to route(:post, 'procedures/search')
        .to(controller: :procedures, action: :search)
    }

    it {
      is_expected.to route(:post, 'procedures/print')
        .to(controller: :procedures, action: :print)
    }

    it {
      is_expected.to route(:get, 'procedures/map_popup_on/ubs/for/1')
        .to(controller: :procedures, action: :map_popup_on_area, area: 'ubs', id: 1)
    }

    it {
      is_expected.to_not route(:get, 'procedures/map_popup_on/something/for/1')
        .to(controller: :procedures, action: :map_popup_on_area, area: 'something', id: 1)
    }
  end
end
