# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DemographicDivisionsController do
  it {
    is_expected.to route(:get, 'demographic_divisions/geojson/subprefecture')
      .to(controller: :demographic_divisions, action: :geojson, area: 'subprefecture')
  }

  it {
    is_expected.to_not route(:get, 'demographic_divisions/geojson/subprefectureadministrative_sector')
      .to(controller: :demographic_divisions, action: :geojson, area: 'subprefecture')
  }

  it {
    is_expected.to route(:get, 'demographic_divisions/geojson/technical_health_supervision')
      .to(controller: :demographic_divisions, action: :geojson, area: 'technical_health_supervision')
  }

  it {
    is_expected.to_not route(:get, 'demographic_divisions/geojson/anything')
      .to(controller: :demographic_divisions, action: :geojson, area: 'anything')
  }
end
