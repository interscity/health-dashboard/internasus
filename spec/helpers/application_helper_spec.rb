# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'returns bootstrap div class' do |expected_class, flash_level|
  it "is expected to return the class \"#{expected_class}\" for the flash level \"#{flash_level}\"" do
    expect(helper.flash_class(flash_level)).to eq(expected_class)
  end
end

RSpec.describe ApplicationHelper do
  describe 'flash_class' do
    include_examples 'returns bootstrap div class', 'alert alert-info', 'notice'
    include_examples 'returns bootstrap div class', 'alert alert-success', 'success'
    include_examples 'returns bootstrap div class', 'alert alert-danger', 'error'
    include_examples 'returns bootstrap div class', 'alert alert-warning', 'alert'
  end

  describe 'human_attribute_value' do
    let(:model_instance) { double('model_instance') }
    let(:attribute_name) { 'attribute_name' }
    let(:attribute_value) { 'attribute_value' }
    let(:klass) { 'ModelClass' }
    let(:translated) { double('translated') }

    it 'is expected to lookup the value translation with I18n' do
      expect(model_instance).to receive(attribute_name).and_return(attribute_value)
      expect(model_instance).to receive(:class).and_return(klass)

      expect(I18n).to receive(:t)
        .with("#{klass.underscore}.#{attribute_name}_#{attribute_value}")
        .and_return(translated)

      expect(helper.human_attribute_value(model_instance, attribute_name)).to eq(translated)
    end
  end
end
