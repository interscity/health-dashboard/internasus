# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HealthFacilityHelper do
  describe 'health_facilities_options' do
    let(:health_facilities_name_and_id) { [[201, 'hospital geral', 314]] }
    let(:health_facility_title) do
      "#{health_facilities_name_and_id[0][0]} - #{health_facilities_name_and_id[0][1].titleize}"
    end

    before do
      allow(HealthFacility).to receive_message_chain(:distinct, :where, :not, :pluck)
        .and_return(health_facilities_name_and_id)

      helper.health_facilities_options
    end

    it 'is expected to find HealthFacility records where the name is not null' do
      expect(HealthFacility.distinct.where).to have_received(:not).with(name: nil)
    end

    it 'is expected to pluck the name and id from records' do
      expect(HealthFacility.distinct.where.not).to have_received(:pluck).with(:cnes, :name, :id)
    end

    it 'is expected to titleize the results' do
      expect(helper.health_facilities_options[0][0]).to eq(health_facility_title)
    end
  end

  describe 'administration_options' do
    before do
      allow(HealthFacility).to receive_message_chain(:distinct, :where, :not, :pluck, :map)

      helper.administration_options
    end

    it 'is expected to find HealthFacility records where the administration is not null' do
      expect(HealthFacility.distinct.where).to have_received(:not).with(administration: nil)
    end

    it 'is expected to pluck the administration from records' do
      expect(HealthFacility.distinct.where.not).to have_received(:pluck).with(:administration)
    end

    it 'is expected to titleize the results' do
      expect(HealthFacility.distinct.where.not.pluck).to have_received(:map)
    end
  end

  describe 'max beds' do
    it 'is expected to return maximum among HealthFacility beds values' do
      maximum = 42

      expect(HealthFacility).to receive(:maximum).with(:beds).and_return(maximum)

      expect(helper.max_beds).to eq(maximum)
    end
  end

  describe 'health_facility_icons' do
    subject { helper.health_facility_icons }

    let(:administration) { 'ESTADUAL' }

    before do
      expect(HealthFacility).to receive(:administrations).and_return([administration])
      expect(helper).to receive(:image_path) { |x| x }
    end

    it { is_expected.to eq("{\"#{administration}\":\"#{administration}_hf_icon.png\"}") }
  end

  describe 'prepare_health_facilities_procedures_range' do
    let(:health_facilities) { double 'health_facilities' }
    let(:health_facilities_ids) { [1, 2, 3] }
    let(:procs_per_facility) { double 'procs_per_facility' }
    let(:procedures_range) { [10, 100] }

    before do
      allow(health_facilities).to receive(:ids).and_return(health_facilities_ids)
      allow(HospitalizationDatum).to receive_message_chain(:select, :where, :group).and_return(procs_per_facility)
      allow(HospitalizationDatum).to receive_message_chain(:from, :pick).and_return(procedures_range)
    end

    subject { helper.prepare_health_facilities_procedures_range(health_facilities) }

    it { is_expected.to eq(procedures_range) }
  end

  describe 'health_facilities_data_source' do
    let(:health_facilities) { double 'health_facilities' }
    let(:calculate_percentiles) { double 'calculate_percentiles' }
    let(:expected_result) { double 'expected_result' }

    before do
      allow(helper).to receive(:facilities_select_args).and_return(%w[arg1 arg2])
      expect(health_facilities).to receive_message_chain(:select, :joins, :group).and_return(expected_result)

      @result = helper.health_facilities_data_source(health_facilities, calculate_percentiles)
    end

    it 'is expected to select, join and group the health facilities data' do
      expect(@result).to equal(expected_result)
    end
  end

  describe 'facilities_select_args' do
    let(:args) { ['health_facilities.*', 'COUNT(*)::real / (SELECT COUNT(*) FROM hospitalization_data) AS hosp_rate'] }

    context 'should calculate distance percentiles' do
      let(:percentiles_args) do
        [
          Arel.sql('PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY distance) AS percentile_75'),
          Arel.sql('PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY distance) AS percentile_50'),
          Arel.sql('PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY distance) AS percentile_25')
        ]
      end

      before do
        @result = helper.facilities_select_args(true)
      end

      it 'is expected to return an array with the select args' do
        expect(@result).to eq(args + percentiles_args)
      end
    end

    context 'should not calculate distance percentiles' do
      before do
        @result = helper.facilities_select_args(false)
      end

      it 'is expected to return an array with the select args' do
        expect(@result).to eq(args)
      end
    end
  end

  describe 'prepare_health_facilities_data_array' do
    let(:data_source) { double 'data_source' }
    let(:health_facilities) { double 'health_facilities' }
    let(:query_result) { [[10, 20, 30, 40, 50, 60, 70, 80], [20, 40, 60, 80, 100, 120, 140, 160]] }

    before do
      expect(HealthFacility).to receive_message_chain(:from, :order, :pluck).and_return(query_result)
      expect(helper).to receive(:health_facilities_data_source).and_return(data_source)
    end

    context 'should calculate distance percentiles' do
      let(:expected_result) do
        [
          [10, 20, 30, 40, 50, [['75', 60], ['50', 70], ['25', 80]]],
          [20, 40, 60, 80, 100, [['75', 120], ['50', 140], ['25', 160]]]
        ]
      end

      before do
        @result = helper.prepare_health_facilities_data_array(health_facilities, true)
      end

      it 'is expected to query the health facilities data and parse including percentile distances' do
        expect(@result).to eq(expected_result)
      end
    end

    context 'should not calculate distance percentiles' do
      let(:expected_result) { [[10, 20, 30, 40, 50, []], [20, 40, 60, 80, 100, []]] }

      before do
        @result = helper.prepare_health_facilities_data_array(health_facilities, false)
      end

      it 'is expected to query the health facilities data and parse without the percentile distances' do
        expect(@result).to eq(expected_result)
      end
    end
  end

  describe 'prepare_health_facilities_data' do
    let(:health_facility) { build(:health_facility, id: 1) }
    let(:data) { [health_facility] }
    let(:facilities_data_array) { double 'facilities_data_array' }
    let(:facilities_procedures_range) { double 'facilities_procedures_range' }
    let(:expected_result) do
      {
        facilities_data_array: facilities_data_array,
        facilities_procedures_range: facilities_procedures_range
      }
    end

    before do
      allow(helper).to receive(:prepare_health_facilities_data_array).and_return(facilities_data_array)
      allow(helper).to receive(:prepare_health_facilities_procedures_range).and_return(facilities_procedures_range)
    end

    subject { helper.prepare_health_facilities_data(data, true) }

    it { is_expected.to eq(expected_result) }
  end
end
